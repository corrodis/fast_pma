-- Scrambler 
-- Simon Corrodi, 08.01.2014
-- WidthData between 64 and 80 are supported, for a width of e.g. 40bits, the scrambler state has to be adapted. Is currently too long

library IEEE;
package settings is
	use IEEE.std_logic_1164.all;
	constant WidthData 		:integer := 64;
end settings;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use WORK.settings.all;

entity Scrambler is
	Port ( CLK 			: in	std_logic;
			 RST			: in  std_logic;											-- active low
			 EN			: in	std_logic											:= '0';
			 GET_STATE	: in  std_logic;											:= '0'; -- if high, data is replaced by scrambler state with pre-code "001010"
			 DATA_IN		: in	std_logic_vector(WidthData-1 downto 0);
			 DATA_OUT	: out std_logic_vector(WidthData-1 downto 0)
	);
end Scrambler;

architecture RTL of Scrambler is

	constant initial_state : std_logic_vector(53 downto 0) := x"196AB87C96FDE" & "01";

	signal state 			: std_logic_vector(53 downto 0);
	signal next_state  	: std_logic_vector(WidthData-1 downto 0);

begin
	
	if(rising_edge(CLK)) then
		if(RST = '0') then
			-- RESET
			DATA_OUT		<= (others=>'0');
			-- reset state, needs to be sync first
			-- for Scrambling a different seed per lane is recomended
			-- currently there is only one lane in use
			state			<= 	initial_state																							-- arbitrary initial state
			next_state	<= 	initial_state & initial_state(57 downto 52) xor initial_state(38 downto  33);
		elsif(EN = '0') then
			-- BYPASS SCRAMBLER
			DATA_OUT 	<= DATA_IN;
		elsif(GET_STATE = '1') then
			DATA_OUT		<= "001010" & state;
		else
			-- use Scrambler, update state
			-- same Scrambler as used in Interlaken protocol
			next_state(63 downto 25) <= state(57 downto 19) xor state(38 downto  0);
			next_state(24 downto  6) <= state(57 downto 39) xor state(38 downto 20) xor state(18 downto  0);
			next_state( 5 downto  0) <= state(57 downto 52) xor state(19 downto 14);
			
			DATA_OUT 	<= DATA_IN xor (state & next_state(63 downto 58));
			
			state 		<= next_state;
		end if;
	end if;

end architecture Scrambler;
-- Parity control, ensured that the running disparity stays inside +/- pmaBitNo-1
-- Simon Corrodi, corrodi@physi.uni-heidelberg.de, 23/10/2013
-- 11/01/2014 added option for simple Scrambler

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use WORK.constants.all;

entity ParityControler is
	Port ( CLK 				: in	std_logic;
			 RST				: in  std_logic;											-- active low
			 EN				: in  std_logic										:= '1';
			 DATA_IN			: in	std_logic_vector(pmaBitNo-1 downto 0);
			 DATA_OUT		: out std_logic_vector(pmaBitNo-1 downto 0) := (others=>'0')
	);
end ParityControler;

architecture RTL of ParityControler is

	constant en_scrambler : std_logic := '0';
	constant en_half 		 : std_logic := '1';

	function count_ones(s : std_logic_vector) return integer is
		variable temp : natural := 0;
	begin
		for i in s'range loop
			if s(i) = '1' then 
				temp := temp + 1; 
			end if;
		end loop;
		return temp;
	end function count_ones;

	signal parity, parity1, parity2, parity_sum 													: integer range -80 to 80 := 0;
	signal parity1_0, parity1_1, parity1_2, parity2_0, parity2_1, parity2_2  	: integer range -20 to 20 := 0;
	signal parity_bit 																			: std_logic := '0';
	signal data0, data1 																			: std_logic_vector(pmaBitNo-1 downto 0) := (others=>'0');
	
	-- Scrambler 
	signal scr_state 																				: std_logic_vector(pmaBitNo-1 downto 0) := (others=>'0');
	signal scr_data 																				: std_logic_vector(pmaBitNo-1 downto 0) := (others=>'0');
	
begin
	Controler : process(CLK) begin
		if(rising_edge(CLK)) then
			if(RST = '0' ) then
				-- reset counter
				parity 			<= 0;
				parity1 			<= 0;
				parity2 			<= 0;
				parity1_0 		<= 0;
				parity1_1 		<= 0;
				parity1_2 		<= 0;
				parity2_0 		<= 0;
				parity2_1 		<= 0;
				parity2_2 		<= 0;
				parity_sum 	<= 0;
				-- outpur 0s
				DATA_OUT		<= (others=>'0');
			elsif(EN = '0' ) then
				-- Scrambler
				scr_data <= DATA_IN;
				data0		<= scr_data; 
				data1		<= data0;
				DATA_OUT <= data1;
			else
				-- buffer data for 2 cicle to calculate parity, maybe more cycles are needed
				-- calculate ones parallel in 4 rows
				
				-- Scrambler part
				if(en_scrambler = '1') then
					scr_data(pmaBitNo-2 downto 0) <= DATA_IN(pmaBitNo-2 downto 0) xor scr_state(pmaBitNo-2 downto 0);
					scr_data(pmaBitNo-1) 			<= DATA_IN(pmaBitNo-1);
					-- new scrambler state out of taps: 79, 70
					-- havent thought about that in depth, probably some more thinking is needed :-)
					scr_state <= scr_state(pmaBitNo-1 downto 1) & (DATA_IN(78) xor DATA_IN(70));
				else
					scr_data <= DATA_IN;
				end if;
				
				-- Parity part
				data0		<= scr_data; 
				data1		<= data0;
				parity1_0 	<= count_ones(scr_data(12 downto 0));
				parity1_1 	<= count_ones(scr_data(25 downto 13));
				parity1_2 	<= count_ones(scr_data(38 downto 26));
				parity2_0 	<= count_ones(scr_data(52 downto 40));
				parity2_1 	<= count_ones(scr_data(65 downto 53));
				parity2_2 	<= count_ones(scr_data(78 downto 66));
				parity1 		<= parity1_0 + parity1_1 + parity1_2 - pmaBitNo/4;
				parity2 		<= parity2_0 + parity2_1 + parity2_2 - pmaBitNo/4;
				parity		<= parity1_0 + parity1_1 + parity1_2 + parity2_0 + parity2_1 + parity2_2 - pmaBitNo/2;
				
				
				if(en_half = '1') then
					-- first part
					if(parity_sum > 0) then
						if(parity1 > 0) then
							-- INVERT first part
							-- second part
							if(parity_sum - parity1 > 0) then
								if(parity2 > 0) then
									-- INVERT second part
									parity_sum <= parity_sum - parity1 - parity2;
									DATA_OUT(pmaBitNo-1   downto pmaBitNo/2) <= '1' & not data1(pmaBitNo-2 downto 40);
									DATA_OUT(pmaBitNo/2-1 downto          0) <= '1' & not data1(pmaBitNo/2-2 downto 0);
								else 
									-- DONT INVERT second part
									parity_sum <= parity_sum - parity1 + parity2;
									DATA_OUT(pmaBitNo-1   downto pmaBitNo/2) <= '1' & not data1(pmaBitNo-2 downto 40);
									DATA_OUT(pmaBitNo/2-1 downto          0) <= '0' &     data1(pmaBitNo/2-2 downto 0);
								end if;
							else
								if(parity2 > 0) then
									-- DONT INVERT second part
									parity_sum <= parity_sum - parity1 + parity2;
									DATA_OUT(pmaBitNo-1   downto pmaBitNo/2) <= '1' & not data1(pmaBitNo-2 downto 40);
									DATA_OUT(pmaBitNo/2-1 downto          0) <= '0' &     data1(pmaBitNo/2-2 downto 0);
								else
									-- INVERT second part
									parity_sum <= parity_sum - parity1 - parity2;
									DATA_OUT(pmaBitNo-1   downto pmaBitNo/2) <= '1' & not data1(pmaBitNo-2 downto 40);
									DATA_OUT(pmaBitNo/2-1 downto          0) <= '1' & not data1(pmaBitNo/2-2 downto 0);
								end if;
							end if;
							-- end second part
						else
							-- DONT INVERT first part			
							-- second part
							if(parity_sum + parity1 > 0) then
								if(parity2 > 0) then
									-- INVERT second part
									parity_sum <= parity_sum + parity1 - parity2;
									DATA_OUT(pmaBitNo-1   downto pmaBitNo/2) <= '0' &     data1(pmaBitNo-2 downto 40);
									DATA_OUT(pmaBitNo/2-1 downto          0) <= '1' & not data1(pmaBitNo/2-2 downto 0);
								else 
									-- DONT INVERT second part
									parity_sum <= parity_sum + parity1 + parity2;
									DATA_OUT(pmaBitNo-1   downto pmaBitNo/2) <= '0' &     data1(pmaBitNo-2 downto 40);
									DATA_OUT(pmaBitNo/2-1 downto          0) <= '0' &     data1(pmaBitNo/2-2 downto 0);
								end if;
							else
								if(parity2 > 0) then
									-- DONT INVERT second part
									parity_sum <= parity_sum + parity1 + parity2;
									DATA_OUT(pmaBitNo-1   downto pmaBitNo/2) <= '0' &     data1(pmaBitNo-2 downto 40);
									DATA_OUT(pmaBitNo/2-1 downto          0) <= '0' &     data1(pmaBitNo/2-2 downto 0);
								else
									-- INVERT second part
									parity_sum <= parity_sum + parity1 - parity2;
									DATA_OUT(pmaBitNo-1   downto pmaBitNo/2) <= '0' &     data1(pmaBitNo-2 downto 40);
									DATA_OUT(pmaBitNo/2-1 downto          0) <= '1' & not data1(pmaBitNo/2-2 downto 0);
								end if;
							end if;
							-- end second part
						end if;
					else
						if(parity1 > 0) then
							-- DONT INVERT first part
							-- second part
							if(parity_sum + parity1 > 0) then
								if(parity2 > 0) then
									-- INVERT second part
									parity_sum <= parity_sum + parity1 - parity2;
									DATA_OUT(pmaBitNo-1   downto pmaBitNo/2) <= '0' &     data1(pmaBitNo-2 downto 40);
									DATA_OUT(pmaBitNo/2-1 downto          0) <= '1' & not data1(pmaBitNo/2-2 downto 0);
								else 
									-- DONT INVERT second part
									parity_sum <= parity_sum + parity1 + parity2;
									DATA_OUT(pmaBitNo-1   downto pmaBitNo/2) <= '0' &     data1(pmaBitNo-2 downto 40);
									DATA_OUT(pmaBitNo/2-1 downto          0) <= '0' &     data1(pmaBitNo/2-2 downto 0);
								end if;
							else
								if(parity2 > 0) then
									-- DONT INVERT second part
									parity_sum <= parity_sum + parity1 + parity2;
									DATA_OUT(pmaBitNo-1   downto pmaBitNo/2) <= '0' &     data1(pmaBitNo-2 downto 40);
									DATA_OUT(pmaBitNo/2-1 downto          0) <= '0' &     data1(pmaBitNo/2-2 downto 0);
								else
									-- INVERT second part
									parity_sum <= parity_sum + parity1 - parity2;
									DATA_OUT(pmaBitNo-1   downto pmaBitNo/2) <= '0' &     data1(pmaBitNo-2 downto 40);
									DATA_OUT(pmaBitNo/2-1 downto          0) <= '1' & not data1(pmaBitNo/2-2 downto 0);
								end if;
							end if;
							-- end second part
						else
							-- INVERT first part
							-- second part
							if(parity_sum - parity1 > 0) then
								if(parity2 > 0) then
									-- INVERT second part
									parity_sum <= parity_sum - parity1 - parity2;
									DATA_OUT(pmaBitNo-1   downto pmaBitNo/2) <= '1' & not data1(pmaBitNo-2 downto 40);
									DATA_OUT(pmaBitNo/2-1 downto          0) <= '1' & not data1(pmaBitNo/2-2 downto 0);
								else 
									-- DONT INVERT second part
									parity_sum <= parity_sum - parity1 + parity2;
									DATA_OUT(pmaBitNo-1   downto pmaBitNo/2) <= '1' & not data1(pmaBitNo-2 downto 40);
									DATA_OUT(pmaBitNo/2-1 downto          0) <= '0' &     data1(pmaBitNo/2-2 downto 0);
								end if;
							else
								if(parity2 > 0) then
									-- DONT INVERT second part
									parity_sum <= parity_sum - parity1 + parity2;
									DATA_OUT(pmaBitNo-1   downto pmaBitNo/2) <= '1' & not data1(pmaBitNo-2 downto 40);
									DATA_OUT(pmaBitNo/2-1 downto          0) <= '0' &     data1(pmaBitNo/2-2 downto 0);
								else
									-- INVERT second part
									parity_sum <= parity_sum - parity1 - parity2;
									DATA_OUT(pmaBitNo-1   downto pmaBitNo/2) <= '1' & not data1(pmaBitNo-2 downto 40);
									DATA_OUT(pmaBitNo/2-1 downto          0) <= '1' & not data1(pmaBitNo/2-2 downto 0);
								end if;
							end if;
							-- end second part		
						end if;
					end if;
				else
					-- parity control with 80bits
					if(parity_sum > 0) then
						if(parity > 0) then
							-- INVERT 
							parity_sum <= parity_sum - parity;
							DATA_OUT(pmaBitNo-1   downto 0) <= '1' & not data1(pmaBitNo-2 downto 0);
						else 
							-- DONT INVERT 
							parity_sum <= parity_sum  + parity;
							DATA_OUT(pmaBitNo-1   downto 0) <= '1' & not data1(pmaBitNo-2 downto 0);
						end if;
					else
						if(parity > 0) then
							-- DONT INVERT 
							parity_sum <= parity_sum + parity;
							DATA_OUT(pmaBitNo-1   downto 0) <= '1' & not data1(pmaBitNo-2 downto 0);
						else
							-- INVERT
							parity_sum <= parity_sum - parity;
							DATA_OUT(pmaBitNo-1   downto 0) <= '1' & not data1(pmaBitNo-2 downto 0);
						end if;
					end if;
					-- end second part
				end if;
				
			end if;
		end if;
	end process Controler;
end architecture RTL;
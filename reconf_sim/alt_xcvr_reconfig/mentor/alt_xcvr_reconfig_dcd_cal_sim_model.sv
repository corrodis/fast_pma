// Copyright (C) 1991-2013 Altera Corporation
// This simulation model contains highly confidential and
// proprietary information of Altera and is being provided
// in accordance with and subject to the protections of the
// applicable Altera Program License Subscription Agreement
// which governs its use and disclosure. Your use of Altera
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Altera Program License Subscription
// Agreement, Altera MegaCore Function License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs for
// use exclusively in logic devices manufactured by Altera and sold
// by Altera or its authorized distributors. Please refer to the
// applicable agreement for further details. Altera products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Altera assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 13.0sp1
// ALTERA_TIMESTAMP:Thu Jun 13 12:36:49 PDT 2013
// encrypted_file_type : mentor_tagged
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
Dw3bjOxd1f1pbuO5tX8N+u11ABNvjgZwMtmsaxk42OCFk1brCwocjgQq9g8Hte/v
x2oNHRJU1YI9VAId8cBmXJX+GEqlufYR/thDlI7tNhnEaTUyXmAFEwgVBk8jl237
D5HwgxLojwBY2oCbYU1wSLy796KVPvgfGnZuOh/n0x4=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 3072)
qqt+UH8TByp5Fwp6SmBBvxORVE1VORqaoc3bnT7DP+Y4c8pEzdlar5+vaRVPWMmk
B+JKvW4VxqF0xWhlkhjRh9xBx9+1XjBU6VlQVqgh2SrvGY9jvYzwk5LEYFJIh/dF
sRwDDrxrvajEwZUJfURywZMIyDWyePc/5aQxf92Y/8g6cFtt/K8vmpw27t2aaqHL
i7t2+7fTm+OY2pSIHMvKH9Gemd5ebWKdwNH0u29f+eN3tMXAjq+USCwU2OTl+feE
3GZw2Yd721leGuJwKlU6XR0hskkw0+MMJ9mEvTVtnJCSNi/ITOb1rHz9rh3niz2a
PyKDMQ7XdXpQ7UUUusmnvwJiCCc/8VxUY2Rc+uuI+7ecm+8bHyMr1BIHMnSY3vcf
QAhnCsRgx1uY0CiT3vCnFLmKVhfH3GNhb3hbodHSWnO+ZyPKGsiHpQTv6oLku7ib
Ha8pmy8iqTsfKHkd41FUbjxlakVUbKaq+oYUWfQP8mCoBVGjGagYoRW+a1CTTeAg
UUR2b2+FbY2A4kvmcbarkWPxbAi7GAcgjbTOelLf8H2aWoYFgEiQSft+F5IYohzR
aPVyM8K4muSrPVt/WzO0vxZOuKHEE7enHoA7kqiLJCx84LrrUA/a+cU7G85I5IMr
6rQCpZL6P4Nr9L3HLhKRum8txjJGK75EsxuS4dRtvR3Y7rPiNx4JGMUyAN7+bR8s
RsBHr2paHAYjHdkv44IXhGM18TbL04xRifHxJp22QGZc4fTvfgMChBw1hb/7SHW9
6vS2hOdCI62DYPARi1yeC17VOSyuL2kUQHeAT1ZnehuyhG6B4P2GvuYiQpUl+Jvv
hFartA9b5ywyhssoyqIOzAcv5WmNfnXh96iBMcw1rVcBIyzB+AF9hzSBlHZWwsnt
gpqIvFUzdq+TSO3SyuqxubrxlsubK9hJT/SFjy1h5jk4QcKrHM6AoR6IhUsWkHIq
+y8eLQXDCtweyBpZ9zyDV+yILY4oVoqpL1UNkZWL9o/eXitp4e7SYsRS9iqklud3
AcPiIfD2znXGo/Rm+tyo84V4Gafb49ve1Vl09n6VNCUycnifMbhjZGAbOpUQ6+9f
bw6nYSIaY0rE3pW154ZoR2PO/SVB+pkEMfZvNYgG4oARMK8+KLjVSwgaaVXwWLDL
+OWptN4JwkOrqc5wCU+f7tODjnyNUD9XPiqs2gPYmZpIRUvs+jr//u+PBK5VVawd
qkyQ1MdOrz7prPosEEpoHOjkrYt9as6FCd05pCwxkfuNnlZY9mW6atiH88vRQ0+E
Dx3NEiVD2vZN8/j06vZEMGyLxxVdNjMCBlQei/cf3LcDIaL2x2c3WcAEBy2/fj1S
68vRdUYhn5QbTe9tDrpXCCo2XxypPpd+WfW4MC4PYspRs6RfLFs2tFVVQDGKllr9
J1J4x5B3xKA/vq3x0BejL9GYGav9B2JwR2cAouHjpB6bVJB12lEj0cB5y2c17Rof
qwgShfgU4bVacEFHS7wDdY3wpy04xtzTjoRlWwGP06AFPBYGAAzEMqKzB1PvWdGH
Fo72Ht1tnbTb1r2TqMI/CtCkWhJD90z11NThRK1/53OVFCQE9NfyFDmnzRTcm+CR
WQAQaDn4X7/Yn1miOw9DynLi2CdJ8ELRmpzVvElcH1ijQAppcQ/RXSn1hQ7o882/
Cvj3VyQ0jXRFaEAWcInuVGKj7oX7uUG82hjm8yB47CFlznqYDW463fxXkBZK+uhM
NnrcXWS8qWlzrthhc+zR8s1O45tq0JOgc9anbWWCIkZWEoAXx9m/Oy1aMBsB851v
3HYS43r7ozlnef+HkR5sNC2QyzszhUM8ryAGkUYtYi7XURXxc/gMIEfbxu6lu4+r
/X9OzdpNLYsw3Ek/Req+twEGFfES6dgJijJrUdsqnvc47xcZnaREo9EM/v9nTiWa
epex/vD1u/2UdpAlGZ9mEpe8d5J2RJ2hBugB0FgEpsYgP/3L+g4ETuWUevJRpfSJ
xHROlXtXVjqm0wZygpIrDeCIlO4kXZJnEZFK4I9OLc0PCODQltWc6YSPU1TMf0HC
v9tTAdJcF+PExJKpyvFHJzjHt/2iwgOACH37rwcAy0MQwPEsvwl+ZjvxrYa8OdpG
deMHNGqDiv7lVhrezFyJKAsjLLPYmvrt0W1poyOOOwbqNCE8w+6ifLSpJsBkju2Q
Duyho8GScF0HQjanhfc1HEj4zP4QL/nIo6WfXd3rnD02j1bvE/wvo81TOg6y6811
GePvp/sGkweMlwHmA5lEgbIbIpVt0Z0LNbV2cBfom19RId/1tOBXZsHpQMB8D8L3
vanFo3omz0M/B+jkLoQiLzJC1GGSQccfq40tmpe7qZHIz/wlLvGnkp+uZpiNPV1c
C2wTLk7GnbIVFIizOdn1eTt+Zm95fKE20UMMX16P2tXrPCZl3NV23BX5Rivhq8AT
ZNJyz+858dmjX4u80HvOBW/wQ42pk97Hb9C1jFrOymF3zc0INb6+/HlOz7vNTDBb
zhnF4ByKnPsRI+FW8pAHJGAEmTs1Lbn9iO7dkisZY7ONgl2En/e985FYVt5bZRGz
L0XpXSHZvIxx/FLJpkxt332qbwaErd15PM7BvySEaa7A4jaC8f2lvd+T42IF5LWs
eiuGX62f9Yl4QpU0b7U1cbTi+XhIXZlR3hhndNQ/vkPreKP0CMmVJqcC9K3jHK3c
ee2OIYBBkARTm37GlhqDhsQ6r4WlvtCHmH+LZbVUxoJKkPNp67ev8o8LI/5VyfH0
7F2wnIyCBfjSwtBNhh2co6dak6NsceUyWF1i6256ksvOcBDp61H4UWXI/+jRiLa9
8ywRpGid+9XQ55Ig9m3wByQuqDYs05Pn5Ict2ttNsy51IBzuOnkGklgwkbuSTwtb
/XAZmuyrDtht10HhRwEci+MjHGaeqGA1B2EWQl5bCYkl0d8bfvdvIECZUWoqfbo7
8c8gKxXsXdxyozisMlinQf8CLoLB4/yk4U1LNQQf0s6hGdDOIFFCfdlYszJiU5Zq
YhPzlUXChN9jhGOYP0ivp7hxVYWe3I/55qePcYWlIZzXHhj0AWLHz8MTyckl/Tp7
qfRSObWTbIfsFnFWAAQqlUo7UuaWSS9m4vVCdwfVqABhw7a8A5I6jx0q+KvKNa4r
sj7uvGJ771KSFBUED0Bu4QHKpyAUL/Xay9/1E+bAkshzCLRpJ8iSR1aVAHUyAlfW
YBIf5BZOVzJ1Bq+/khS0ZqiGF0LrsieW2rv1rHxvTYILI9vg30B/9VDDT03Gt3rP
XBJmLh8BxjT06WAKw0fouWoFfSyskfziBRms8ufY350k104fu4jrjrOc4D+cHSeW
YM6eOU4ggM+r7OYC+pVpazt/cvK6zt3uIo718sXdMYc7VxSIwwkM5qBnFmYbEtRa
hwa3h9s+xPJdEVYU+6FfHpfeirmbU9dLuaUB6lDgdNFH8cviIrZPuuP3545+QF25
VOECppW7YMYgs47Cacj2mGWGnQEYE4scwS4s/U3ZGDnHLbsCnKi866HFRZ5Gzm06
OYyWTVaYjJbh3JnDTmkAOD7dzrXL2AL6w33hp4g3L81L1UDZ1azRLy37HYhSfA/6
JKlrllOce/qv9KtkCu70VMwcnCDkSvx1k3EhTmOdpHL7ruFMuYjv41EnD7jZmGPm
nSpikPIuY/xL+NNN60MDsuSVWcsIIdIrQHZU7PHHwGJQgVH9QUlDgWTuulvkzuCc
AgE1r0gcWC15vvd99KnKb/r+AY2ngZXqQvtgAKLSL6QKeogmCqWRa8Tyl1ozF4dG
avyVVxNxkKm0ahRMhjy+Th/U1kTDUld5JDrXOwjPb/InTfp3Khrw2kqnQJc6B4jx
/fAOcGKYd6Lbxu/g7+zafuQRSYPC2Pxng4UXUAEa8/WyJyB7ot9sKqxcSOIlrabh
r2pFvXwtFvvk+NMON4/vX6HDQNNyDvTJEAh8uQwMaxpArsFGa05Nl0G64Mh6hagS
30eqqYhIiytxF0OOyMIyJY6pQaf0BDkX8+AjLMDo2vhr19r3PzHX0ZPVEwqNGkQ8
IpwWnDb69WtRYjb63lTCYUFPtlWiNMTZjo8kbiMLShM+iq6cuT70Tfor0O+R2qSg
`pragma protect end_protected

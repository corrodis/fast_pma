// Copyright (C) 1991-2013 Altera Corporation
// This simulation model contains highly confidential and
// proprietary information of Altera and is being provided
// in accordance with and subject to the protections of the
// applicable Altera Program License Subscription Agreement
// which governs its use and disclosure. Your use of Altera
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Altera Program License Subscription
// Agreement, Altera MegaCore Function License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs for
// use exclusively in logic devices manufactured by Altera and sold
// by Altera or its authorized distributors. Please refer to the
// applicable agreement for further details. Altera products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Altera assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 13.0sp1
// ALTERA_TIMESTAMP:Thu Jun 13 12:36:52 PDT 2013
// encrypted_file_type : mentor_tagged
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
D412enhbwURgdXdqBq/Luunl4UxVJr2Y5nWER+4Qy/0Sn6TjUvIIfgIzl84aJNOk
guG3jEpQBs4ozjiystTVBxqkavMQncplDnukbSVlFdffMlGKg5+4SsPlnG2eHFEU
A6G9azCPMIscDCwDD2sab3GQtScjfL2beTTOJ6Ah70Q=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 3072)
PDGDnOCgivf3TmnRrqUJ18Z5mtq7z9tTd6xDjCmgIiGw+XDc5MercSaSJJXP+o3m
TjuEo9uiUYu7+pCshXusisvV/64cQ8AwHGzIeUXlOWA4X2rNpYEwkTn4JbIhEOuw
8xCwAAeTk4r7SsrS7uD6Lx7OMO4Gi0a4rEmZpFJpnRfVwlZafmpTSkMCfwkZCWeB
ONbNbZjWsTvGU/qf9X79Xv8pF+Kbg7yCQT288/ECJb/PSD6/+RmiSe3SfR+Jo5vP
wePjEsvTI4uEJeOulDAwmZUZyyM8CP0TE+OIZGNfjSIagywNOUspY7w183T8xIeM
5Um+V6Ydk2koVp4f5oB1QsAhZBP8OnI2/0i1ftfWR4I0z+g1rw4+INzVKEYgUAGP
Y+6Pr13MebecXNB9UhtP8qc+ucz7wgOaUqhjdkPk912t0W1mEN4qTMlUistDLDym
mN/PRhU9qlmlX78wwTi/chtudhV4nGtuJ2CCEivTwQ/9mQDEZOZ5Wzkv5YOEuJFc
4DvGkl7LchUBcXgszp/BZzsaoD06W5bjQH0ekgkFn7ILGoZEAt+IXXOzsqYhVkP6
6UwdQ6vVuHqwtlK/dnKpvI3kjotPaRt8xYz3f6WksD+3wXkLc4ZA5ek7Il/DJiMT
q+ElWn1t6ynb1rQPHRR3G+pV41h51kyPxpIcMHncSwGgNpysAbv3rHOCSbTZ9mWl
OPf5SB9Q0T1piCFWx6Drbqzl7TckSDgtmjFXgAdkGtRFW1gMF/aPGdiFIXAta1l6
MtUOjDtDQ0CFEYSPxmfSN3Bl23LPkXQwLcG6svpEwhpUlnw41X9OPwSxj4H/3gMF
1AwRcV93DgjKfi+jA4D707yyhyo4dOmD+k6gbNhrl9JCa7h3BNoebvmRtY9yJ9hr
JSgXRCZ3t/BfizddrzjIBhNK/Q6CZ4rcBuWbPXpOHT0Mh7RM29HCrznUoDRokvnz
Miy3LC7QQn3/Pt84zxyysmuWoFP8mGksSNRYTXVmL2ZEcvmvx2gHrpKTfyPROZjB
uuZXYQeZtPDDDNTlfcg3Gmgh2oZjAj/wM4H7wmNZjjvWhT6aNrwFR6c6lC4Klx71
VOXDQg2sGUlLMj+u5QiuYRgP4PY8Ksoywfkc/Y4ageeOHKgtudGOKD+lfzW38VsY
sstjCdNIPj6UwiHkghptX5XbiFpWHN4Fq+2ftOPT0SUYjeNsGEGQ4uYrUOQbRU/m
BY1Hg1poVeYnxdZ2cmJHHNOv4pjEM8XIwmsaOECyxZaCNHGGHPy7Cw8h4v/K6HVz
HLTkOsr7IezGm41eD2PwBdNn7FmR01QRGXC+Kw3tIeRmdune2lfdEwLo1u7cGGXu
1UxkZ5u+eRLDZHZ/jwCK9+KSq0Rqm334YxwC40ox+3Ck7iF/uC6fNDrw3GznkINL
PvwvlECjgcoccDeKe8slBQnlahVBzFgBX5bTFk+tJxQssPXdWdjbKcPo7t09Vd3E
uoQIzBkTKXTm2g1cWjMMzAhOCTj1sKgs5Ng74HxlOYXaWHcOvsUHh40QAAKlbfv7
zF+h2vfc/iifdfCgBvRl9HoCmcQPuWoGsrh9VMgdyToP2FrZ30gLcu/3DV426vjL
jtWnuNmb22Ae+tgj6Jl/vViKgljJ0+UhXnTHlLF6oCM7huZhV3T58sPY8GFx6Wm2
wCf3+YzN/26TzEOlCx1HJwHl7avmGNUY5QzRzUXL93sBElXa6ZMpDmr6RMV/PFkt
k9+TLb2QXa9q8YO9QRksC6FPVj8Kcc95xd6ZzwPn54Afcl/t3aKfWWkAbBhn7JqZ
EQFVJB4Y763QSt4qhqCqUrJBWmrg5pjO5op2nLOjnZ7J1NE7n4yYVitMofmj6ynV
IoUncR9n0FPdoautartg/h+7OdEnjDQz0eNfweqNpGy+JetvlzVujOh4xGdb216K
bqgGeLq4P6UcXeHeoDxT5zBaiOW7Dibg9qzVlKUegJ+d8aSMDittquOX7v/XyhmU
ZwUIT2gfhmRtGNsuRxOkGG3HGG2jO14FT6pBkn/381dxL+4AiZZH1k9Yy+J3Rcaj
9H0h/WsEusApYuz2Ar/aYqnLdsHTy4iiosDZ3If5hwKf29mAwj54Mc1+Y/B9ALwi
8tT11tbOn8FUZI/+mwAzNnxNcPSAnPJJ5UZYH3kfiZYz80HhI5fCvKMv+gU5/SIL
jCt0VCfoBOlJhvxyBlGDqaN+U1OAi/LDuMMSlx+e8fd2kzT+eNGrhVsF71Y9JFkH
vMK+rM2tn3nB8RlLXN8xZQLODXy+J8HANjyKhbX7YckEelE77HdHRtmw99wHnhPb
hmtSlspGLJGU/47vGhJTsP5gyFeiH2CWW0aZTfZiQ3apvFqBmp58y+luQoXIpQRt
uSHl6K+Mh7hUecBPVt1FLxjB9PVc/9Xgn9JOZ4MQuJxbQUsZkFbq0CCLkJosV2lV
8TeQbGC+ZHxYme3Lt4QCn0sg/dHLy8WHOoUEdEhdji7+7Fts1TCXPgM083/TtCan
fsXIQ41wwCfmOUnBA9hZV6zURiifhDBN2iumKbrw/rquO9p9hHwShd/GWIZFMWVE
/WOhfdVzKV2zkgqGwH/KzjRtYu6uHwGivB9n3VJ2HnHRM8mDTRkaaA5J00LLjK8i
1N4iCNcb2PRYpn/4K7cP9g0vbfb6LWchh+m1TSZm94aijbMGpj7pSc3kL7C/9sSX
g64/OChz+D6TwJjlnVE3vNkAjFWLe6yXOb3Hj/XwGXpQNARYZ3Y/Yw8kDYvSmmm6
hT5aVxa6JpfjY7u7RmE1o4CGRzGkwcAWlJQ+gp/VtSaOGlepF5SqLNgikr2+7Dh+
B6tK187EtX5FOd8O7zkYjrM1w/H1YL9UTIMrp9bpw6vfV7eAicoOkmPHWqX44og/
qI9OKAZd6RIjrMgH0H6/EbDDb087W/LLuH0fDarWKGI5LI9nkNYEhDUwbbNHqTzm
yrr6k5eHKDlUdbp7CU7m90v1ij499tyhOOx3of9RoGyXLmZrl0cEfGZNxQWKF63c
PnEzB5XJzo+xwlUJi2wn6DRhQQEOvIpw7+NtWrOSlkMnCVgcyNmD2oWsF0tqd7Jr
c3vsuMqjDADMNt9y+N2P1Li6HFB0dsXbj7oxjytHm9L9auCvfcaNIhvrCLO7qekj
PXD27/lyNp+6NZLw3VKLQvw45tFKeFr5u7nlPbOiFI3pf4fKRd0xWhQcNwauqojN
p0JxkIGoanleIQr1TjJ2jcS2WTM1Pmf6o9G1/mqLIXY0goCQ5/n+mMxIbdYzllWT
kyQphbO1YGELiJD0+PrrduQvazktFw02iv+4iULABr6ShYi56XGU7gY3cWWkX4HB
MuToBgSAUoyC3NCfzILo2ALhLqelcCb875/rV8vvihfhGujBdzLAFUvckWT0xOJW
aoBM+PdGNPywRIxmxpJ1CySoPmNeTzNFLSsMUdHcYVcDs95NU1V+sN59UOzDe7u1
MC8cbOpDuMN+AwMoMVymnJhzsctDtlOT7NLRCaR7htBWN9s7GryFMbM9FRkmO055
YaZEt8mOfGNfM5fZC8jAPFlR5EtZww8MorUz/vL5knyMr4+8NMh5LoVnfLjDXyzY
5Z1FqtQAUocmwUYT3eNWFp+19E1OvolM9tNIEAt1EuyRCrG4+JsyWv1WxZQARb46
1NuGGSUhk2suWBNtnj4mK+1MgWP5xiuI95kpi6BzpauzcaETofLHlg0VjyLbaYGO
WFIb67TNLoGeWH5vY5zQmEiTq2IWdsIm9OdKys95FHV7vuchr6FjTl82rpifC1uv
1TegAiBN3cLXoy3FSlSx12mFVRwfJAwDi2gInf21Rq4sGURkzbQfvVygn0v3ctTg
pYvlMYydxQW8hcfyF1ANPtHQiiwcHOOozTnt0AJqANYyOsQ/gHs/JhV9DpRr00A9
xZkp1bOiKXgMQ6QmfutAJJZtgOkWufq5Wug20KKFO/mIa8nmgn4CbdLkl8RwVxPV
jrzgsYezUCNRESNStinwswlzBV/aLua7/diddY8WQJPf2niBpyeGAGYoIcyjPiGM
l3/5j60DzuQfRGpu9KTOmIwdNr2z2mtLGnq7qUhPcuGXQxO5pFVOMpQi70qMht6f
`pragma protect end_protected

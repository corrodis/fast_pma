// Copyright (C) 1991-2013 Altera Corporation
// This simulation model contains highly confidential and
// proprietary information of Altera and is being provided
// in accordance with and subject to the protections of the
// applicable Altera Program License Subscription Agreement
// which governs its use and disclosure. Your use of Altera
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Altera Program License Subscription
// Agreement, Altera MegaCore Function License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs for
// use exclusively in logic devices manufactured by Altera and sold
// by Altera or its authorized distributors. Please refer to the
// applicable agreement for further details. Altera products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Altera assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 13.0sp1
// ALTERA_TIMESTAMP:Thu Jun 13 12:28:42 PDT 2013
// encrypted_file_type : mentor_tagged
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
cwvCwwj4V/VlQAdNUl9uV9luKpei/zdnoPAQs/bdNHQS9NOLGUi0BRo0Sl2Dg6v3
sIJ+fz3algE6S//+A07gFJEEombS0vVREvM5cniIKIAUKf8JCac/zq5vwj0Icteo
kWfeO6hNZPN/Ti4w93Ce6aipSyGnKuE+Cb8fuU7cfsY=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 2656)
eW633C6dPv94VRNCs9knVP5AgNZ5vB62LFVKdaN1dmvoDe/fU4POadgYryZSE+fF
Y4veeJOnztTqUia7EuMNp4bj1EPoWaoebzCQbPuLKBnSHcpD038yMpfrTGM2K+fk
/5JwN/7WNbabar8nS4EW3AnK7FNtcIivryIvgqea3dpqyfe8ciX8dJ0la0Vb2wcl
qQexRu0q6xHbtH5l17HXIiWWM5t8MIikFJFbp2P2ZNfTvs0X0W0HrBDiTbutTYLJ
pDkdGS4DFL/+Gjss3hbCahh1FAL4CbGdRsMBb1CELjHfEQka0Ak3z9Bz6lVTNwFC
9XwKHeIb5hu5Ocp4D+fLlp0plECza0uaHL/hPM9zgscYOYVxZ9pu1MFZfvYDKSUT
6VwK+umqc7nNUmT9Al/U/rfMA8QxZfNuwyNb+vhxoRATgCFkZKRSlplAkYfKAO10
RyK4n1IjHL14tFrMD7o+8kE6hLNz/L9bBT80NiWljwCocmdpE5QPezty3TtyJylp
HfAmehIvCyHUJVtd+54rqrYlRkrBkySSV73zhqBKT8fJibodkOaZhbBiAY21DSZf
1BKEDPrWqpY1B6nXCmbXJIRl6NCDHLmX6bhMqfdJwge+1FmS6vgzeOqsLxC+NNk3
QV2yItfz6qLJb0UrxIpY03M1le0l8sqL/FBMOxsjelmRiD9MOKKpTzxrfjIB+fnp
Hx6wfyqUVr+ihHBPUKhGKgQS++dvEoH5vFi8T4EVRhZIEk+/HpvHhkGgnmGALVkt
BCWvN7HB1ClaqWEKhDP51loIh32x0pjadh8NTujcCusg/IY4CmFnaXCnDP3w5+Oh
NnA3GOdgunGXZ6w2rXrzKFOxe8pMbIWXY6/AFeSGAtGLXm2gv2dK1l8gjoi4C7e9
JYhRCHSc+GFd4ZGHy8F7rmtnlbLqVbDvmv2mbugRXAYVrl1KfdIMFhUGaITWBU4m
2ZVvkNDhnxOnqI18JLqSkkDnHEXg0BpwUftJieJQppmpavTp/y+vmPRtPk0Ufq0z
WtXJkbBhTdc5bOjMp/nIFWiSYguqI0gpl3hJ+rzRWVOFz+04sf+tp01AWORdC8h5
pbvdygC0cJBfigF1CBMULMZS73amMBNFsR6Y/RymDTJpEdN7FGlmekBNHEBSd08i
E40+8R2Drl8ucdpvvVZWQ0HjBWlyUXPhooclVFHGnn2HzCdSewqSdqInXK03Cvm2
If/whJ2DVuAQd5MTFKtCojK+3z1plCUNV3Qfi4zYn/+RN7Y3wb3a/u+/QZg4b0iH
Yf6BoLZDrPqUX0VmoOD9vsqKHdlORxySpnl8Z31w2X09uMxzTCyCssbYwlqM7Biw
LmgBeGmcmNtJkwgvvrSO7D/vr85uv5PQ13CEznwQVbW0xtwBP0LEuXHJWWFKXpu1
WUNw2PQzlgQvtJPBgFPotHqHkhb32sBftkcw4GqrM7VV1jrPDZfSTl9P7SfZddkj
Lh40Ayg1M4xkC/o6OEFZmMhbdStkHR/akR45T06sjIhaXMZb6hgksE6JsDCXtsL+
XFlYSlu66DFyTwaGxx+fRTlB00lXHgaqAT5Cm0U4GnFOR4xgC8Ggvxw+wKc7jtOU
ASJadQf/VYQHtHE69kyEupkfUhIgozjgWJAQvyyo1mv38VI34T6UQa4+p29e+n2j
3lCY/vlGiQoPqfh28Qpmd2smfXIds2FftAsIgtMEG8msSHesBnrQdpHrnnJSfBg0
s1Uruaw//O1N5mFHuhk48REicTB7bhS9VTDB2kB5Wp1Rz7YHckfBSjFszNUijHlQ
7Uaop2UWQOdkSHuTVD25r9pN0GaxDpDnnDSDGDopB8J4J84fNR/wGGQxrVEdHoNc
20zMFOkrlbWYzSXTjRYCwMOvww+xkG2FKUp9NF4bxiyEpvXPsiTp9iQGImDScm43
5qaUNt84IWEHgFb/l/au+FeiYVOSl2NoxFkflC9pRJaNcu0SOmEP7mcw8cgKTDOC
1k1u01DygsiPaY80ZrtGwddhIhkCyb4/a7ejkicEfhWVm+Tnjqfz2n4QgRKDrH3l
4QCiJAo0vNFzS5OGHWddywwpanW5UmoRW93bY3AACCtCyjubhIPLSDAe5YgxoQLb
E8TK0g1VJx0Qbz6Tkgvp+8JU76uD6dC/37CGJp3YvwrVQNVdl0x86lby3OXLpn4p
Y3EJ4++55elM4YUCi71iwCJXLhKeRS+YCvUoHCd0IR5LjysjQY3BzNIuNWC9OUbQ
V5Z24HJvaLrqEWFX3NB/CmYYHUEAf1JtRqsxtw4Dgg8bBre7hVCFNs1jm+hESLeg
HLVJMksyIqVX4TSJcGnufdCMG8vaf50P8mB1TJ9sKS7erX+mlJoHvZrp3P+fg4pQ
llJqYZTEo6c3krZBjLNBjsH/N5ZV9RdBKYEisAVxOzDvO4gXSsM4rAuQNWLWxfUl
UfbYU46lFifw+pgYmk/JbAKKHzBEYLmS2pH1z/6TC8iePVpC3fuGwkrmHfkrx9yh
YYSC0Lg/fTJZJtIZ/CkWWcz/vHo8YxBNu5dC5wZ1iSfNMg2O9vTo6ujfH3olRR+R
nx+ek6oW5o9fW5Kn9aa9KvhCfuVLWXOkaG2iq45n0kM4D1njjtRpfkxEFWwtmTsS
dw8kgp/3MzqxqxpDgIQygpIcRoDahYko4ktAOtSpGt7NeVyn6Ker9bnurgE1PVj5
MsXxP6InvSlVPE2pcrPEV2sn6w8tFAs1U0aqg8DG3Wc3zPJRGX80pIO5FbzwMyD6
wQy/OCH/P6Jx3DlugSytPAXSiZo7+Kmt7bv8Md7Qe5UnNL9N5Z2+kScAwl9fOqjZ
YgKBTNslia7y09ODFkNwwwj1bYCjhw1iBlZctzBs0DMCNHG8ExktN6D6wQ5qfjI4
rFzyFvDhdpQmBBH883/158zAvQKlprh+SOcuPHIgxvdlrOThTuK+TtG0XK94qwkx
SlrAYW0/WWiYAdWlRs4Jlsqb3qhM7zpJSHdrKA+MY0NJg5O5IjAQiA/0fD9LakKv
nrSqfH2jCkVPqmdJfur9WTjoFY70KmikHwFi3oat4Neh+Yl3yOGUHAXfj5y74kIO
mauSTYBHtOsqWw4XtIqJhPJXBU2CT0wOTQuTDrQgSibvY2FM5SP2tT2jOgiDZXay
h6PgPfN+vrg9mElJCS/xVbHfJYseuQSQC5G6V2QR2ah3quN8L6GP268+H7iwRAMe
+Yi6ISvSMWl3ItOQETEGVraeAaS5B1dLmnRDi6bzkavm021CuVz6UV5cwkayP1pS
kXZQC1VH9uO5F0ZzVEuYe5T1ePnmzbDRdyfv4v+pXqwtmc4vZVZecajql1Oap6B+
DbntpluSoCoty1uRMi67JifdSJ9NJrL44V61eZr7ZQD6C/MiV7KglDZ0qvoUrVDx
RpJUr3mURq2K2h+FYpYcnGGyuw+XY904ECKTSH2ZsHyPAPQW2ToGFk/QlKk1C6k5
/23WzKzS+D8oUnG2r+bwaUw/6AIFZjJ5++hdoPQm+x9A0oSqJrsySz13J1J84Mwi
PFQpng116dF8+GarFKr7Sw==
`pragma protect end_protected

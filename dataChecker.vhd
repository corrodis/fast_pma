-- entety to perform alignment and BET for data
-- 18/12/2013

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use WORK.constants.all;

entity dataChecker is
	Port (CLK				: in    std_logic;
			RST				: in    std_logic													:= '0';		-- active low 
			RST_CNT			: in    std_logic													:= '0';		-- active high	
			DATA				: in    std_logic_vector(pmaBitNo-1 downto 0);
			MODE				: in 	  std_logic_vector(3 downto 0);
			ERROR_NO			: out   unsigned(63 downto 0);
			TESTED_NO		: out   unsigned(63 downto 0);
			LOSTSYNC_NO		: out	  unsigned(63 downto 0);
			SYNC				: out   std_logic;
			REC				: out   std_logic;
			BITSLIP			: inout std_logic													:= '0';
			
			deb_pattern1	: out   std_logic_vector(pmaBitNo-1 downto 0)			:= (others=>'0');
			deb_pattern2	: out   std_logic_vector(pmaBitNo-1 downto 0)			:= (others=>'0');
			deb_patternas	: in    std_logic													:= '0'
			);
end entity dataChecker;

architecture RTL of dataChecker is
	-- settings
	constant wapNo 	 		: integer 					:= 300;
	constant bitSlipNo 		: integer 					:= 400;
	constant erWordNo  		: integer 					:= 5000;
	constant wordslipNo		: integer 					:= 100;
	constant maxErrorNo		: integer 					:= 100000;
	constant errRatio			: integer					:= 4;
	constant en_bitslip 		: std_logic 				:= '1';
	constant en_autoreset	: std_logic 				:= '0';

	
	function count_ones(s : std_logic_vector) return integer is
		variable temp : natural := 0;
	begin
		for i in s'range loop
			if s(i) = '1' then 
				temp := temp + 1; 
			end if;
		end loop;
		return temp;
	end function count_ones;
	
	component dataGenerator 
		Port ( 
			CLK 									: in	std_logic;
			RST									: in  std_logic;																					-- active low
			EN										: in	std_logic;
			INJ_ERR								: in	std_logic											:= '0';
			MODE									: in  std_logic_vector(3 downto 0);
			DATA_IN								: in  std_logic_vector(pmaBitNo-1 downto 0);
			DATA_OUT								: out std_logic_vector(pmaBitNo-1 downto 0);
			set_datak							: in    std_logic											:= '0'
		);
	end component dataGenerator;
	
	signal sm_is_sync : std_logic;
	-- states
	type state_typ is ( ST_NotSync, ST_Sync, ST_Rec);
	signal state : state_typ;
	-- internal counter
	signal cnt_wap, cnt_bitSlip, cnt_wait, cnt_wordslips, cnt_bet_serie, cnt_bet_error_bit_serie 			: integer := 0;
	signal cnt_bet_error_bits, cnt_bet_error_bits_last 																	: integer := 0;
	signal cnt_bet_error_bits_1, cnt_bet_error_bits_2, cnt_bet_error_bits_3, cnt_bet_error_bits_4 			: integer := 0;
	signal cnt_bet_error_bits_5, cnt_bet_error_bits_6, cnt_bet_error_bits_7, cnt_bet_error_bits_8 			: integer := 0;
	-- error Counter
	signal cnt_bet_error, cnt_bet_error_bit_sum, cnt_lostSync, cnt_ok : unsigned(63 downto 0) 				:=(others=>'0');
	-- flipflop
	signal data_next_gen, data_next, data_diff : std_logic_vector(pmaBitNo-1 downto 0);
	signal data_shifted : std_logic_vector(pmaBitNo-1 downto 0)															:= (others=>'0');
	signal data_buffer  : std_logic_vector(pmaBitNo*2-1 downto 0);
	
	-- bitslip for nativ and avalon
	--signal bitflip : unsigned(4 downto 0) := (others=>'0');
	--signal cnt_bitflip : unsigned(31 downto 0)  := (others=>'0');
	signal bit_offset : integer range 0 to pmaBitNo := 0;
	
	signal deb_buffer_d0 : std_logic_vector(pmaBitNo-1 downto 0);
	signal deb_buffer_n0 : std_logic_vector(pmaBitNo-1 downto 0);
	signal deb_buffer_d1 : std_logic_vector(pmaBitNo-1 downto 0);
	signal deb_buffer_n1 : std_logic_vector(pmaBitNo-1 downto 0);
	signal deb_buffer_d2 : std_logic_vector(pmaBitNo-1 downto 0);
	signal deb_buffer_n2 : std_logic_vector(pmaBitNo-1 downto 0);
	signal deb_buffer_d3 : std_logic_vector(pmaBitNo-1 downto 0);
	signal deb_buffer_n3 : std_logic_vector(pmaBitNo-1 downto 0);
	signal deb_stoplcd : std_logic;
	
	
	
begin

	sm : process(CLK) begin
		data_next <= data_next_gen;
		data_buffer(pmaBitNo*2-1 downto 0) 	<= data_buffer(pmaBitNo-1 downto 0) & DATA(pmaBitNo-1 downto 0);
		-- 80bit version
		--data_buffer(159 downto 0) 	<= data_buffer(79 downto 0) & data(79 downto 0);
		if(en_bitslip = '0') then
			data_shifted(pmaBitNo-1 downto 0) 		<= data_buffer(pmaBitNo-1+bit_offset downto bit_offset);
		else
			data_shifted(pmaBitNo-1 downto 0) 		<= data_buffer(pmaBitNo-1 downto 0);
		end if;

		deb_buffer_d1		<= deb_buffer_d0;
		deb_buffer_d2		<= deb_buffer_d1;
		deb_buffer_d3		<= deb_buffer_d2;
		deb_buffer_n1		<= deb_buffer_n0;
		deb_buffer_n2		<= deb_buffer_n1;
		deb_buffer_n3		<= deb_buffer_n2;
		
		if ( RST = '0' ) then
			-- reset internal counters
			cnt_wap 		  				<= 0;
			cnt_bitSlip	  				<= 0;
			cnt_bet_serie 				<= 0;
			cnt_wait		  				<= 0;
			cnt_bet_error_bits_1		<= 0;
			cnt_bet_error_bits_2		<= 0;
			cnt_bet_error_bits_3		<= 0;
			cnt_bet_error_bits_4		<= 0;
			cnt_bet_error_bits 		<= 0;
			cnt_bet_error_bit_serie <= 0;
			cnt_bet_error_bits_last <= 0;
			cnt_wordslips				<= 0;
			-- reset error counters
			cnt_bet_error 				<= (others=>'0');
			cnt_bet_error_bit_sum	<= (others=>'0');
			cnt_lostSync 				<= (others=>'0');
			cnt_ok						<= (others=>'0');
			data_diff					<= (others=>'0');
			--cnt_bitflip					<= (others=>'0');
			-- reset other stuff
			--bitflip						<= (others=>'0');
			bit_offset					<= 0;
			deb_stoplcd					<= '0';
			--data_next	<= (others=>'0');
			state <= ST_NotSync;
		elsif( rising_edge(CLK)) then
				case state is
					--- --- --- --- --- STATE: NotSync -- --- --- --- ---
					-- to Sync: find WAP wapNo times
					when ST_NotSync =>
						--if(data = WAP) then
						if(data_shifted(pmaBitNo-1 downto 0) = WAP(pmaBitNo-1 downto 0)) then
							if(cnt_wap > wapNo) then
								state <= ST_Sync;
								cnt_wap 						<= 0;											-- reset
								cnt_bitSlip					<= 0;											-- reset
								cnt_bet_serie 				<= 0;
								cnt_wait						<= 0;											-- reset
								
								cnt_bet_error_bits_1		<= 0;
								cnt_bet_error_bits_2		<= 0;
								cnt_bet_error_bits_3		<= 0;
								cnt_bet_error_bits_4		<= 0;
								cnt_bet_error_bits_5		<= 0;
								cnt_bet_error_bits_6		<= 0;
								cnt_bet_error_bits_7		<= 0;
								cnt_bet_error_bits_8		<= 0;
								cnt_bet_error_bits 		<= 0;
								cnt_bet_error_bit_serie <= 0;
								cnt_bet_error_bits_last <= 0;
							else
								cnt_wap <= cnt_wap + 1;
							end if;
						else
							cnt_wap <= 0;
							-- perform every bitSlipNo time a bit slip
							-- bit slip only happens on rising edges 
							-- bit slip is asynchroneous (doesn't matter here)
							if(cnt_bitSlip > (bitSlipNo/2)) then
								--if(cnt_wordslips < 10) then
								--	set_wordslip <= not set_wordslip;
								--	cnt_wordslips <= cnt_wordslips + 1;
								--else
									if(bit_offset > pmaBitNo-1) then
										bit_offset <= 0;
									else
										if(en_bitslip = '1') then
											BITSLIP <= not BITSLIP;
										else
											bit_offset <= bit_offset + 1;
										end if;
									end if;
								--	cnt_wordslips <= 0;
								--end if;
								cnt_bitSlip <= 0;
							else
								cnt_bitSlip <= cnt_bitSlip + 1;
							end if;
						end if;

	
					--- --- --- --- --- STATE: Sync -- --- --- --- ---
					-- to Res (Resiving): if real word (no datak
					-- to NotSync:			 if 10bit pattern gives an error -> out of alignment
					when ST_Sync =>
						if(data_shifted(pmaBitNo-1 downto 0) = WAP(pmaBitNo-1 downto 0)) then
							-- stay in state
							state <= ST_Sync;
						
						else
							-- data or error													-- TODO: is there a way to recognise errors here?
							state <= ST_Rec;							
							cnt_bet_serie 				<= 0;									-- reset all error counters
							cnt_bet_error_bit_serie <= 0;
							cnt_bet_error_bits_last <= 0;
							cnt_bet_error_bits_1		<= 0;
							cnt_bet_error_bits_2		<= 0;
							cnt_bet_error_bits_3		<= 0;
							cnt_bet_error_bits_4		<= 0;
							cnt_bet_error_bits 		<= 0;
							data_diff					<= (others=>'0');
							-- data_next <= std_logic_vector(unsigned(data)+1);		-- gues next data, do exactly the same in ST_Rec
						end if;

					
					--- --- --- --- --- STATE: Res (Reseiving) -- --- --- --- ---
					-- to Sync: 	if datak occures, tx changed mode
					-- to NotSync:	if 10bit pattern gives an error -> out of alignment
					when ST_Rec =>
						--- --- --- --- --- BET part -- --- --- --- ---
						-- CAUTION: cnt_bet_error_bits are always the values from the last cycle, 3 cycles back right now
						
						-- loos sync VERSION 2: if more than num_bits/errRatio of one word are wrong
						if(cnt_bet_error_bits > pmaBitNo/errRatio or (cnt_bet_error_bit_sum > maxErrorNo and en_autoreset = '1')) then
							state <= ST_NotSync;
							cnt_lostSync <= cnt_lostSync + 1;
						-- loos sync VERSION 1: after erWordNo words with at least 1 wrong bit in a row
						elsif (cnt_bet_serie > erWordNo) then
							state <= ST_NotSync;
							cnt_bet_error <= cnt_bet_error - cnt_bet_serie;											-- correct for bet if lost sync
							--cnt_bet_error_bit_sum <= cnt_bet_error_bit_sum - cnt_bet_error_bit_serie;  	-- correct for bit count bet error of the abording serie
							cnt_lostSync <= cnt_lostSync + 1;
						-- sync not lost
						elsif(RST_cnt = '1') then
							-- reset of all global counters
							cnt_bet_error 				<= (others=>'0');
							cnt_bet_error_bit_sum	<= (others=>'0');
							cnt_lostSync 				<= (others=>'0');
							cnt_ok						<= (others=>'0');
						elsif (cnt_bet_error_bits = 0) then
							-- no bet error
							cnt_ok <= cnt_ok + 1;																			-- CAUTION: counts words not bits
							if(cnt_bet_serie > 0) then
								cnt_bet_error <= cnt_bet_error - 1;														-- correct for bet counter errror
								--cnt_bet_error_bit_sum <= cnt_bet_error_bit_sum - cnt_bet_error_bits_last;
							end if;
							cnt_bet_serie 				<= 0;
							cnt_bet_error_bit_serie <= 0;
						else
							-- bet error
							
							if(deb_stoplcd = '0') then
								deb_pattern1	<= deb_buffer_d0;
								--deb_pattern2	<= deb_buffer_n2;
								deb_pattern2	<= deb_buffer_d1;
								deb_stoplcd		<= '1';
							end if;
							
							cnt_ok <= cnt_ok + 1;																			-- CAUTION: counts words not bits
							cnt_bet_error_bit_sum 	<= cnt_bet_error_bit_sum   + cnt_bet_error_bits;		-- no of wrong bits of bet from last cycle
							cnt_bet_error_bit_serie <= cnt_bet_error_bit_serie + cnt_bet_error_bits;	   -- counts no if bet error bits for actual error serie, used for correction if aborded after erWordNo (loos sync VERSION 1)
							cnt_bet_error 			 	<= cnt_bet_error + 1;
							cnt_bet_serie 			 	<= cnt_bet_serie + 1;
								
							-- more investigations on what exactly went wrong, avaiable in the next cycle
							-- DEBUG
							-- cnt_bet_error_bits <= count_ones(data(pmaBitNo downto 0) xor data_next(pmaBitNo downto 0));
							--cnt_bet_error_bits <= cnt_bet_error_bits + 1;
						end if;
						
						cnt_bet_error_bits_last <= cnt_bet_error_bits;
						cnt_bet_error_bits <= cnt_bet_error_bits_1 + cnt_bet_error_bits_2+cnt_bet_error_bits_3+cnt_bet_error_bits_4+cnt_bet_error_bits_5 + cnt_bet_error_bits_6+cnt_bet_error_bits_7+cnt_bet_error_bits_8;
						cnt_bet_error_bits_1 <= count_ones(data_diff(9 downto  0));
						cnt_bet_error_bits_2 <= count_ones(data_diff(19 downto 10));
						cnt_bet_error_bits_3 <= count_ones(data_diff(29 downto 20));
						cnt_bet_error_bits_4 <= count_ones(data_diff(39 downto 30));
						cnt_bet_error_bits_5 <= count_ones(data_diff(49 downto 40));
						cnt_bet_error_bits_6 <= count_ones(data_diff(59 downto 50));
						--cnt_bet_error_bits_7 <= count_ones(data_diff(63 downto 60));
						cnt_bet_error_bits_7 <= count_ones(data_diff(69 downto 60));
						cnt_bet_error_bits_8 <= count_ones(data_diff(79 downto 70));
						data_diff(pmaBitNo-1 downto 0) <= data_shifted(pmaBitNo-1 downto 0) xor data_next(pmaBitNo-1 downto 0);
						--deb_buffer_d0(pmaBitNo-1 downto 0) <= data_shifted(pmaBitNo-1 downto 0);
						deb_buffer_n0(pmaBitNo-1 downto 0) <= data_next(pmaBitNo-1 downto 0);
						deb_buffer_d0(pmaBitNo-1 downto 0)	<= data_diff(pmaBitNo-1 downto 0);
				end case;
			if(deb_patternas = '0') then
				deb_stoplcd		<= '0';
			end if;
		end if;
	end process sm;

	-- always set data_next, is only used when it makes sense	
	dataGenerator0 : dataGenerator port map ( 
		CLK 						=> CLK,
		RST						=> RST,
		EN							=> '1',
		INJ_ERR					=> '0',
		set_datak				=> '0',
		MODE						=> MODE,
		DATA_IN					=> data_shifted(pmaBitNo-1 downto 0),
		DATA_OUT					=> data_next_gen
	);
	
	
	with state select
		SYNC <=  		'0' when ST_NotSync,
							'1' when ST_Sync,
							'1' when ST_Rec;
	with state select
		REC <=   		'0' when ST_NotSync,
							'0' when ST_Sync,
							'1' when ST_Rec;
	
	-- assign entity outputs 	
	-- bet_errorNo 	<= cnt_bet_error;
	ERROR_NO				<= cnt_bet_error_bit_sum;
	TESTED_NO			<= cnt_ok;
	LOSTSYNC_NO			<= cnt_lostSync;

end RTL;
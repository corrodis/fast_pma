// Copyright (C) 1991-2013 Altera Corporation
// This simulation model contains highly confidential and
// proprietary information of Altera and is being provided
// in accordance with and subject to the protections of the
// applicable Altera Program License Subscription Agreement
// which governs its use and disclosure. Your use of Altera
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Altera Program License Subscription
// Agreement, Altera MegaCore Function License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs for
// use exclusively in logic devices manufactured by Altera and sold
// by Altera or its authorized distributors. Please refer to the
// applicable agreement for further details. Altera products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Altera assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 13.0sp1
// ALTERA_TIMESTAMP:Thu Jun 13 12:36:53 PDT 2013
// encrypted_file_type : mentor_tagged
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
W9PK1L4DJSAwuzfHdNskOgszs6mSnUYaCZZCGzpQRaYNLDN2tDlZ5hjE7n4gd2Cc
WVWAfxujEExMOcSqVQjXjoiozXnkcou+aaxvt141IPBSVvz+wIaqFmt/GOVWUjUr
91rtgsxEQzUBMwc13r46ifh1QKCXT6Q5AZwsVqFar7A=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 1680)
ycPcuOqj345N/MxdMT2x7qhLCwO0ddivzml7M8FcDJG4Y8PSAJQXPk6l3NJUC/HT
yKi29zrxxEtikUTPgUeqQpEwibML9u2PMxf8IenLIwj4BbGC+1i3/cSTEwFTjm25
FxovN61jI7AmrT0TO28pvM7q9jT1H4RXk0ihA42cYFZv9vU1tUTtatdRsYOkLYFz
oWuadelLaMsFCiP81BmTfjhnTKkec9afPWrjYCIqLu4RSbeyNxi8qrM1uQKgqSjV
jNMVhO1VJWn8AiYFudMaCRiyZind2oBSlh858qNOv74JbIU+HnZo2Yr/iRQxuEo4
qggKXjJV1OKXv96cpDhbdwSR9CKKiA49LkzHH/p5rREt+4EBwpRtnNy1LI6F+Dhm
mGUZYk0sjAp35MlRfi71NQNIWMvBN6+bDUFLcTKUmxt4xhg6nKSrshTqQAlAGzAU
H+ktxfGM0cAxS5iRB/jtil/AEKtNcljBao+XWf1KPlq+VZuCtbFkzgVQfUM9e2ao
Hk8f2wL4F+UppQFV1yYmDzFiGWAqUcacAmWCrr3T1aRuN43SryRveFGwErTfWO42
kNL6ErI6ON6wKrJPzalibvPMxByvTodbX59JAkI9R28mkaITtu2SgjDgkQ/XLxef
2fjFlf9m7AOh8ZnbZtVYkBSjYAkyB3uBh7khHVfKq+fvg3CdehwtrXKJ5gVq8pBS
5zGbHoAqWSfvIRHr6pVerFrc5+Ic1Aug7jbCaof4OGs3NXdQvcAD/CYAPImuaf1K
tZW05s4GGZiIsGqlwTVPjTRd0ryVidYy/W5q9YvnxEZxj7WNhBKhAjpYPJ83Z1Ui
MaPJ/obHCK7zYOPaF8uffJvORzIBTsBxHOeugUku7oykPrtqy26ZRBJpyA2HiXOn
w64iMCyiqE50dWK+eXLfg9UgW/JN+kr0faPUx6QWG0NfMDGXQcSG8mAfUALsrJvs
+JuL60/lvsBxSqu+9RSP8AwXe1YIIYyt7RXY3oP7pfjeBGYGYoVOU5YQyE2cGlxI
lSFE8Om/ooxVvp2tSRgVmR2+0omgSteXtIfAyvoNgKhvgtlGRdyGDXuX2P3uvFxf
I4yOdOBbacv90oBCz2UY/yH6aiuFKnznnjWo1L3CIl5X5Xzr5YiLEuAjyBRn7B+h
mjj27pH3GgL+Up08TGoog6MboiKGlKFF9EJJF7siEMGtjeZ0fYt5G/XUEXvWEZM3
6nraoVLvu5zh5Okfe6Nka8Q+S75nq6Y5zlKEltFrgpf8yEU9hX1uw73FweBCKzwo
yUWjABJ26qGC38rr7Tm9xnpBaesbb8WbJpvNKh/+kAFkSQNCtcOCrcQEDMfxGd8T
9GxVcVLPqmrhZzEDPr4NJ+bENMlrDfYKdMHiPP9WLg8Mfjpi818Rt7qt1523R/pM
7Cc0WQE9CSNmJxnWrNB+EVT8GZBVZKP/QZIFXJjE9YvIyIs8IqjommasusMRhXsu
RqX8sKYE+6xoKTnrHtuxpz22NCbAqGQSnPIdrJAyIK6+Il7tgEN73wJN3fsQgmqk
2L3S8+jFOxfnv8BFlVodJFRfFlmdIR47S7oQukFgMWTMjaxt/qlpSm90JILCHv6m
NEcQDS89Zv5PZIDHbZWfFi2a62gSt8k4N9tvuAZAL+NhtPINv+ZJFCqm1P5t8wd5
EAC/C0VrlHSIIRGER0GnBEdWRRhzEHudvvrIfISuGiNS8QaSWEj4sUMqZjLNFnb7
nGu2LimsIR4eXRH8/UpCQOE6+BJKWD5ndXfi0eUnUkiQnyPQv6zqYHs/oYpTQ5Bw
k4+yniFWOX8l6LEptNhUJw4NDTUVTsxv9sUcjV/OEJj2MnYMoc/96ibyXkQLjhA6
RVNydfacTuvbcaoiGA+0RSFU7qxAGEqe48nAzPjPnbKAW2Znpdr3/wtWf9Xh46Sp
A0uJhKFwD7/lAAKl9pTa6aIb/7VIed4cJLGs03Ztg/uMARZs7aKol1157X0o3wzl
Wd5SNsEyFQy3nCWMzHnESkZ4Q6zJq3an1PYCv10Pi0nLs9PXZdsiK5DwZCdyWWIv
NqZOw93Tykd/nUy1O/krXlZ2W2z1N0nMmGUB/qeaVQ++vi/0zMrqEgYzpM77IcBp
28rb5U+0v+DjxbqXXUgzGKcn7ruLp736ywWexVuhIgkyDpAIcqZicHzggAC3P/9m
eJYVu6yEutXB1G0oE4xd0yuaHXtXnyV4Iw8v/BmZfGEhYnQMWj1+L8T4FdjVbA5w
`pragma protect end_protected

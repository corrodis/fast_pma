// Copyright (C) 1991-2013 Altera Corporation
// This simulation model contains highly confidential and
// proprietary information of Altera and is being provided
// in accordance with and subject to the protections of the
// applicable Altera Program License Subscription Agreement
// which governs its use and disclosure. Your use of Altera
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Altera Program License Subscription
// Agreement, Altera MegaCore Function License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs for
// use exclusively in logic devices manufactured by Altera and sold
// by Altera or its authorized distributors. Please refer to the
// applicable agreement for further details. Altera products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Altera assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 13.0sp1
// ALTERA_TIMESTAMP:Thu Jun 13 12:36:46 PDT 2013
// encrypted_file_type : mentor_tagged
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
Vf4e7GMrElReZANtVsQKCcMr9PWIqROdMwX0xEWeMbIztZOGS5yxmdHbPkUrgvjg
oobCZVqNf+XCFRFyKtOJSjOPsr2oLituFZtTjxtdFrFCwJ4qzmlfNtCnKU9HTH1r
vE2er/EscR2ShSoQ6LdCCcvj6eRjdf9sQ/2zkheJz7g=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 5312)
VdeuwSUtt8MKQ8ziS9YzXi3iKxIGg/qvLDJ21QUx3N1yFmmHfhY0ZAqlqCcCztkv
ueSszUG+9Uj/6qAGOn4fNlr/IoX1e5d6Ul+1dpL8ihnEeU9ZtXPcIqspAf511GOe
5AXuByTwmu2+wHPyhw9NtJKa5B2QhLXo1NhQNF0P/r/kdaHIk/6zz7gVy9pw37W/
bLEQLdgQK+9X/VEzWYjWV1HjAFIJmdnMZLnZazFUaG9JgkqNqrGBNYbQcACiC43y
wjRTkn70yYX6npZGluV5pHp4UvJvbz1Kk57/XeXvypylEREOlKEBj1iyJ4NUwpOV
X2XKl6+7nDZbmc1A5SaYC6s2C9Wo2r4aiixcUko56gTgzc5Uf2cIo5YQvrcXKMwv
DfEadhQWvZu6HBgNJq3xnfv2uk7pRbkaQ+HiYzM7kz68bEvIW4Z+4groufqcN9av
BSqc9156phq+QNv/F25pOKyZlDTQUXMdfP7guXe2CvMbFMASKp66vAMbE1R3G2+Q
o0W6sgnHgJd4+BzJZ8F6Aw+fadoJFsi+Zz1y6jd8mtK0H5mkRsiWC1U5+6Jl1iHK
zqhsj0JWuBgwzdXtRqFJyjFQZKwuTj/ANpVq0XG60YeyVKZ9eQlGYGQ6u1sYdAXs
/DCaphTaDQRXDQBublYTt4q61zu2EhG8OXFtzIPLf6Y4/eOZcxU4iBGa4og19iKk
sNCXpTyBI2Gp3Vk+O+aW7dvkzwdbUb/kBeybrB8TNa6YRlRUxCtdUqssx1+cmN7B
/Yx72yYhlhf8XG24p4cx8Vt+cUJ5FCAzNr6XxfnbWkXuMSFZLD+Zr2sn/qZ/HXqt
0acyU+0XRWIEHHgwi+DdnNHKVjWOQSSNzddt5TPz+8aLDzOG23VN51xVfS8FAtMZ
dTAxWPekkV6hVnjJH2iys+Z5QP45VMCvabPAgodpVmSxIfKSHQbv/w50K6ndYc8u
1+YtEm4b0ff+pS73jPiK1sWELlGfB/8cdNVNU+JHu9kfpnBJbgsJAice2Bx+57JL
wcyEJWGX3blZN9ABfP+Uf5x5YNqYRU0PG3aHNpb/3wTUvHRUsR4K8iaJfHc5qRkf
7w1MZzLfkq3McVBEXDhDpm1wuVi58Y3WuxHJqxgURP/u9kJkrUb42AQEz55GTCxf
lxBfywtSucqNqR7pGpP8JjJVECmjRWyrCZKqleieuD564LbQ21ePz2BF6TiFr0i8
EkpktvYXjF8nBdpyJfIizgduwl1kCPahLYHh//PRMrpH/UrnaucyA0O6nYhknUgD
FKDyI2Q8upifhhZcToeg0zQfApfLPcUVqWRnPTKVaHVCtVgzsT/0aWUzJhiHp6m2
souOGaL7CVIj15LgvsFGtcaPAct9e4trbiC0bo9oE34Nlw7m7I4UfKLYIgctOutl
14LsL0zx7eFg+1ZZI8qCTlZtWwLY//YQLhaHsq5dsK6ALylR1JQciJOb5XZOuFuW
40EbRdHn3g7gbrAe6s5VKiyhJaUhXYJLz1AUg/YvakgrwHCfApKrC0CQxwqylEWW
FCPrpQMN3gZpYMoyPB81isJ2+WT2b8BMz1k5hcEEcRtYmm9askTRKO0i/Ijm0adY
mGDi1prIeXeIr8VXuChbfqNiAlcJefTDku32/iYzGr2BrNAXFW0plAnyE281Ul6O
6LZRTOuZmC4qO9/1iRGkqJyMp7N4X+xb5mhjsHSVlC4YAXqA9NyWtm0TkX4VxU+7
cUbBIP1zhpKHKC9ygCUQZLVgfw/jK461qJmrMbgWah8I5QLanBPm+CJHLz/msjoV
tWt8IR0Jfr6CqgNaMYVYKZBZ5Nbl1MWhsU0AN2Eya/iQxfbaTek6jnialT8DWFIA
ojfsIpDqkw2TMi9naz+asjPTG6iTvjMVFuepiF6HsFQENCZX/MRRghkCTUkmIabH
lYJm6LNvOdrHSoKuQ64bBUwA12QkHTWg+BJCwaDKKeh42n2IynZeFz+FSQbBDFDq
54rinHXxMEUn1xakSZMgVmUQLChYMSrmBzOo1lfbSWOKGt1kaNEbNNYoQRdDfNAN
83xmV/XLVA6YXGsvYOs7C4BCRHzqvASxNteauZDvXtN1OjoOA4zjfoT3bDbBf0r7
hkU9DQZv0XBYoDGugUVTUJjaxo0U5wjdA0VWSQ6GpLblOwNbh+CCmowfctZSMexy
9gXTXQgNLJT66LRT+WQl0+rj+FoxaHCqvgcyD/0anId4L/aafv/eRpeESfXlZj9t
S9PxEEdZsC4uedUuJ6mqZZC7JnH0bfnFQaUrvMPNDnbpjBQw7RugRDEfrm1N6hb8
Flm0k6lmqXo/Yjm8kihZsGj///HDFlMaBsVI0I8ojVIz/58KQ10E0FOEp4Dbk0mj
GHsCm8nT1N9ALaiJEXz6WR1IOMPJncg/TBkZ15SUBRxw78fJ3B3FljPJQuF1q7LK
EcB78PrJ94tOuZKHtfx+C+D1zft78uyDQvkXUyd68yghh0I6yWzgoLRJYzdi9O8J
AB7gumqkOSOnwZyoi73fIvkHdiuodK0CCqdIKRm1lgIMp0W2fwtISvazOhblJYPG
WDyP4bYgmmKZ5gfQ3vgZ9tCJRZo+iiqRrYtnkugSEGGxiEif3jNk6ZoDdfTs8z6T
zboTvUIu1+2jdPJmy/W+J0CxuTzeF51/0m0EpSIzLnUMdVam6As3g79kRkuytkYo
z6m1RyW69gHFeDMP4z5YzXH/a24MTRnlP7AI+H1ZEwXdy9P7nTQQtBWQRafc5asu
FFY3TvRYG5D3Xg/Q7x8prEGgVW4Z4OSGQxQ+1RNpYTfF5cMLzuA3ZuGZCoGjnLCX
DW6ZSEJRz5z2F/wHib/5UKvxZM4dyH3W1SkZi5nvxw5sYNyBgHkjLimw6MqN/HBz
dBE36X0qnOqAGtVz5uCeFV/sOHXdEQd3WEQe82z8/wBlAe3Ygl6outcHF2yff3g1
wa5XhHdO7t5UVz0W2aTLddasPO0pL1Ii8YH1zXPOJ6EVpwX26wFdSiWcE+/mIzXK
j0+1QgAtJi0D1DOWomSOcHewmGu2+CoLVhx2zajuDhh9bCI7qCsNVbfuBXP1HrTZ
GpbOE3Y4c2MlopXhXDTTFj4d43415kKhNiRm3D/sr+BAm8IKY6ewdW7dCCFzPDvx
SiFdfwOdbiJT5LghyC0VCP9/PipM7CLPUstNRMMM3rDgD81dCj0JQVE7pl7p14pq
/nsiTc0IFqyaZKXsIETduXITYD7XDqnOFIgJHze/FHAZwXdI6KQAf+hGvRNwj3mp
ZK2i6QPkPifHgdsnk6YRc8HkgpI0k6jdB4bAmjqsCb8JUaLV1whfhpVp9HYjO15v
Gzcd7nvfazVoyCSalWLuLdbUtWQ5p4JnQ8H6WM2mTcqYxdjW3zxbiOaj0Lc6wpjc
5xMfnPxKMucBsb2o3xcrufhjZ8/P3ipFhRbBdlz5pSd40hcQoGlJt5Fsx1W84L1R
C/VIFBSqrO9CErJB+Z0dQ3ZOP81Xy3yI42O6XR50aal1aLoUMWaxyBha1EbNoIu8
Zp2st9Df37Lnr8fnGFuX8TlSddAwPQFgff9HsGcFhtMMK6N4uNkBQrh92OT8g48F
JaYoigcw2lWUnMR7tmCzk2wMFDm+uun3aQY1c7eBBloyu6bv5ag22iBOjmov+WoZ
fWOoqTHdEtGSuvpjV0HMrKWwB/FuYuJE6UR6sh3V3s5xBDjFGns/OsbO+uYOAtsY
Qvu0dlWjw6Z6XTRHeA8tzo9M3hWEA0mSoqhwWYBEzBjBCflcB9e2iEKdo5mov3LZ
NGS0vN3Ow+tDLMrkXARuKpOReu4CYyWWlHlpmTKo2RGcoyHyyynz4u1Bs3xWvU0l
eiiGeqlWFy0VpYXlEYvUPO1sqao0bUd0zNehqUa7elIH17KKmEmmshtBl7DTtd4X
SYgymlQkov7ZW9vZzcQZgoytgK30WoPp8QXdk3WSq2vEajyA3EvDutpJJL3Qou/r
9gBIkIMgT/mDw4IB39yjizKuDUrbyLVzgKDCLDU2YHplvsKOpW81qYCB1JinzhZZ
pzGW/KoImFtEQhPRhiLdCJmg3/3i1UbLHjoEKPXDTELY3Dpp6MvHhZ5CWMl08Ap0
WmO//kclcv2dRNeSl7s2ICwSEFIaF29NPq3+Cux8fiH6+I1PYmwczRpyRMCUrNOc
4HtYDqZ7TqhxNkm9LtkiJg/+YN3AS6xK3/omgNtw4fTv8CMcgjOQISZTRTOV8Y0W
Bvdqlmy3XYzKvQQgzAGarNsDum2vXqenoOyFs85BGE6jTW+tZog8KJZC4BFOFtcM
rcSYECeIYoMnbTFhWhtt51GS4+I5mqWTK4WQu4DkKeBfOuaJIweqKl/S5OO3AqVV
KGM82N3Jq2Tyx2x+5Ak8mWqFmJtIasedOwBsMtttdhI7881to9vkSqFOoPxH8C5v
aMLGjmWC4zaDrl9RuR6Zb7cXAnr54Sjkx6T3RdJzZuT9DKCc7ZNAZaxc8SGy3gl7
zRwKrg6owmcto3PCMoCY1bG3aTuX3WRqEaPp4abjLEMh/LwgyhS9A9SRKAt/b583
t6aB97TS7vRUPIWOoMmnYk/PGiDUKLwDpeFeQWQyy8dHcdvXy6zoa0PPYDjQXKr/
hzNoVA5QfigcRtZ3vtlKJj+o395RkxOnziHwiImlW/qLYIk11Pm5R6jyKrg2B7K8
SnZ35ZQ+hkD5+4WDAWUN9TV98QjxG/BIAABA0cF2ltKbuaxlRaabNgUWxkjcAWuc
vdTULd8LUz1Z+SWstV741WfmgYgYg4d7/XWABJSk3LbwO0hSneDCbufhSd6DOkb9
7DFy7APTvHAvfXNmVZlJ9ycaso9ty5fU2L9U44/sjU3qRCZsrp0Da3sAXdPF/SiP
SABnGcKqGivGEcwbsCu/tChPRe1aTrOWfR+/XyINzBMocVvfZ/euGchz/YvVPdJR
XZrWEinJvLA6GKgsZCIxYOZtc0gertRMy3PgfEqISCM/0CN+Yg6FULs66w0Q1vLF
mijj5IXu9Ct9FLltkL5H6YJDC4LFQCt2QRCaafGapkf47Tf98Rs9cKDb17YXOrjD
XhXyp+hLFVy3KZQh7BcKybiJla8q9V6yBlEvWAEQnxmJ35z5Q55A+VXNnhIR+mLL
xxnT3Z6fU+mnENi7Su7Pvg+hyqROH70iatRRS9d2FXaLsfuFZLtXGm8BGKqk2z/G
Npw2lF8olz/mKn2wxs2wUns5c17VFNwWDmokwLakPiop1KONSFaCt3aVEYKrXYQy
Ap3F3nA4rGrcsK8gCAxg1+aSgq5hP5WOTnvtEhGDRLbppn2VoP1j9+El6uE6phkd
IJnIEvVijqyCzTk6m0JiC6zHt32Igm54KVpfqPuB4eikzn84a/5f66L+oiqdbXhD
mRMYGZrtWdaklxtosaC66zjb0m4ok/d7IWT1FTV1aqURWrMGVqKxnjXej5Kkw2ry
8aejqTezCx/rdeLRxUevBIFRTQj2ib5XQSYWASJ7x+sxSkxjo5FYlwyHXH1qshIu
MxsUKK/vFG4pTTnKCSqzyFWO6zKjlS90alViZg3ZJXr/3wWz3S1hBk/qhizbSthp
Dwz9BGseNT2UP3yT9h9f2OybVMWplraG9zbujo8OESkVK+sZbD/tfm+4BSon/3o/
8TRBH3ZKN1pwG8XGq6mbJ0VhtpwDrBzV1r1ygudHYBpgWWog1xeomYhn+8Iw1KHM
j0L8l0NsbpxBXSe/03NO6grk4PwDldjsgpPjIVtoBuWa70qluJhhekjK3CLpDk9O
vnkocuuBVAyOidH+sIXOJ/L0NIktgQjUQiPq7dTCkFbyOBP/M2AvE5zXn1CEvYy5
riXhIrNs78EDcOpCEHDXWlWM9t921i0GgtZtvHMH3wiycQngI/j6trHiPUqsdXc9
gCHTVsmj+hW2/S/4VByQYeD9tqyMaqlOm9Cv1FhMon27nxg6145rtetdwEaW8Fx1
ByLsMleU4pVjM8hg5SuEfyWG81yHwQdPRxgodQoxG0rMhnqNvC5tF+B7ljYqZudr
vuy1oKCAmMQVZ9edWvyzptwmCm3hQv8fJtbTlbACV1bW9Qg3PPpWPPe7rPVmSE6a
ILS+fbTTRUC/fdNJ6QK+65J9hRGAdvmRC14WPKlrT8xuvctK4Tq71bZ5f4qLwnnK
7TbW39kmGqVWZCx7IUOkyE/tk2cN0WCV7Cnog7hqj/hsGs5T58Vc8KLv+7dan94U
TqZweJhnjAzeRIZXnfwIw4ukxlNHc6BLlzFSo8PAM+v3NwOyJRUlH8RpKIfOLanD
wuurQq1TXroiU8DDb+00NrlRyv8JFTlfF8smXGVxdBUT9cv/2nT50naXykSSsQyy
5zXPVss+Wtw+OKCAw8TH3dunUU80N2OMmONHr95D6+KWjdgcWwTLvG7F6rUEDxiN
RcETbIuPIGqf2lh51x1hyegCiLGjzwMwUgUezUowoYMXfGbYGlPoj6xhxCWBSd2c
tLsG1GMIMtwU7jQYHkfvy5WrJnbIkeQ58VWvBjeEi8o+vO12epY3sfRCz95aJBF6
K2OgKQHi9mutg3gAmVpsfShKOjhjL27qEqJVGYO0VAkVQ+GeTyxHLQfYefQxvP9x
nL1kAxpMUdAIfQO+Pi7jYSYkOvMp83wltDuV98BZ3tLguCjClK1PP8TQYEYoFEWZ
7nh0lvEz9URcl+Hv1FXpG5B6wPUzgmWIVbbpxWyl3M+cgcjVqhK3k0wiE2o6VTFw
4axRK1976mJF9si9596r3RhH8d/sh9IAj5VxIxsKvusCL2XQcPM1WaH2XwM1mnTV
Pp86AWa1EiupHOGK8yS5wGSgrUVJsx8tzAgsRbqR0EW5YLsb/AP5WZLqDu1/Ivaj
JRwnyAmzDGSufKud3ii7bfuFHRYuH8N4ZTfeLHEh6VjDvpbxvtBDdzMg5Bx0e+O1
UR56wVGRuYaMXLGwDTJ+vRmf7WlN6vcOy62nadUzObHrZNxqyuuexH8jQd9U7Rgp
EituNIYHq47SMSAVHYMsuU8DSJ2LHsjbJRdu+s3SoYzThgueT44dk34rJDFc7oUM
IgElv85te4pUYzhh5L+qrDiIKjkYiPCPsGhbVNsHEtI=
`pragma protect end_protected

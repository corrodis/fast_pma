-- PMA for HMSA und HSMB cage testing
-- based on the native PHY IP
-- handels transmitting and resceiving of data
-- to change nyumber of channels, interface width or rate, adjust constants in WORK.constants and adjust native PI with MegaWizzard
-- 18/12/2013 Simon Corrodi and Carsten 

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use WORK.constants.all;

entity pma is
	port(		
		CLK100								: in  std_logic													:= '0';						-- clk used for transceivers, for 8Gbps a CLK125 could be used
		CLK50									: in  std_logic													:= '0';
		CLK_TX								: out std_logic_vector(chNo-1 downto 0);													-- clk which should drive input data
		CLK_RX								: out std_logic_vector(chNo-1 downto 0);													-- clk which drives output data
		RST									: in  std_logic													:= '0';						-- active low
		DATA_TX								: in  std_logic_vector (chNo*pmaBitNo-1 downto 0)		:= (others=>'0');			-- chNo*pmaBitNo
		DATA_RX								: out std_logic_vector (chNo*pmaBitNo-1 downto 0);										-- chNo*pmaBitNo
		SERIAL_TX							: out std_logic_vector(chNo-1 downto 0);
		SERIAL_RX							: in  std_logic_vector(chNo-1 downto 0);
		BITSLIP								: in  std_logic_vector(chNo-1 downto 0)					:= (others=>'0');
		READY									: out std_logic_vector(chNo-1 downto 0)					:= (others=>'0');
		LOOPBACK								: in  std_logic													:= '0';
		deb_cnt_ready						: out unsigned(63 downto 0)
	);
end entity pma;

architecture RTL of pma is

	component native
		port (
			pll_powerdown        		: in  std_logic_vector(chNo-1 downto 0)    			:= (others => '0'); 		--	pll_powerdown.pll_powerdown
			tx_analogreset       		: in  std_logic_vector(chNo-1 downto 0)    			:= (others => '0'); 		-- tx_analogreset.tx_analogreset
			tx_digitalreset      		: in  std_logic_vector(chNo-1 downto 0)     			:= (others => '0'); 		-- tx_digitalreset.tx_digitalreset
			tx_pll_refclk        		: in  std_logic_vector(0 downto 0)    		 			:= (others => '0'); 		-- tx_pll_refclk.tx_pll_refclk
			tx_pma_clkout        		: out std_logic_vector(chNo-1 downto 0);                       				-- tx_pma_clkout.tx_pma_clkout
			tx_serial_data       		: out std_logic_vector(chNo-1 downto 0);                       				-- tx_serial_data.tx_serial_data
			tx_pma_parallel_data 		: in  std_logic_vector(chNo*pmaBitNo-1 downto 0)	:= (others => '0'); 		-- tx_pma_parallel_data.tx_pma_parallel_data
			pll_locked           		: out std_logic_vector(chNo-1 downto 0);                       				-- pll_locked.pll_locked
			rx_analogreset       		: in  std_logic_vector(chNo-1 downto 0)    			:= (others => '0'); 		-- rx_analogreset.rx_analogreset
			rx_digitalreset      		: in  std_logic_vector(chNo-1 downto 0)    			:= (others => '0'); 		-- rx_digitalreset.rx_digitalreset
			rx_cdr_refclk        		: in  std_logic_vector(0 downto 0)    					:= (others => '0'); 		-- rx_cdr_refclk.rx_cdr_refclk
			rx_pma_clkout        		: out std_logic_vector(chNo-1 downto 0);                       				-- rx_pma_clkout.rx_pma_clkout
			rx_serial_data       		: in  std_logic_vector(chNo-1 downto 0)    			:= (others => '0'); 		-- rx_serial_data.rx_serial_data
			rx_pma_parallel_data 		: out std_logic_vector(chNo*pmaBitNo-1 downto 0);                    		-- rx_pma_parallel_data.rx_pma_parallel_data
			rx_clkslip           		: in  std_logic_vector(chNo-1 downto 0)    			:= (others => '0'); 		-- rx_clkslip.rx_clkslip
			rx_is_lockedtoref    		: out std_logic_vector(chNo-1 downto 0);                       				-- rx_is_lockedtoref.rx_is_lockedtoref
			rx_is_lockedtodata   		: out std_logic_vector(chNo-1 downto 0);                       				-- rx_is_lockedtodata.rx_is_lockedtodata
			rx_seriallpbken      		: in  std_logic_vector(chNo-1 downto 0)    			:= (others => '0');		-- rx_seriallpbken.rx_seriallpbken
			tx_cal_busy          		: out std_logic_vector(chNo-1 downto 0);                       				-- tx_cal_busy.tx_cal_busy
			rx_cal_busy          		: out std_logic_vector(chNo-1 downto 0);                       				-- rx_cal_busy.rx_cal_busy
			reconfig_to_xcvr     		: in  std_logic_vector(70*2*chNo-1 downto 0) 		:= (others => '0'); 		-- reconfig_to_xcvr.reconfig_to_xcvr
			reconfig_from_xcvr   		: out std_logic_vector(46*2*chNo-1 downto 0)                      			-- reconfig_from_xcvr.reconfig_from_xcvr
		);
	end component native;

	component reset
		port (
			clock              			: in  std_logic                    						:= '0';             		-- clock.clk
			reset              			: in  std_logic                    						:= '0';             		-- reset.reset
			pll_powerdown      			: out std_logic_vector(0 downto 0);                    							-- pll_powerdown.pll_powerdown
			tx_analogreset     			: out std_logic_vector(chNo-1 downto 0);                    					-- tx_analogreset.tx_analogreset
			tx_digitalreset    			: out std_logic_vector(chNo-1 downto 0);                    					-- tx_digitalreset.tx_digitalreset
			tx_ready           			: out std_logic_vector(chNo-1 downto 0);                    					-- tx_ready.tx_ready
			pll_locked         			: in  std_logic_vector(0 downto 0) 						:= (others => '0'); 		-- pll_locked.pll_locked
			pll_select         			: in  std_logic_vector(0 downto 0) 						:= (others => '0'); 		-- pll_select.pll_select
			tx_cal_busy        			: in  std_logic_vector(chNo-1 downto 0) 				:= (others => '0'); 		-- tx_cal_busy.tx_cal_busy
			rx_analogreset     			: out std_logic_vector(chNo-1 downto 0);                    					-- rx_analogreset.rx_analogreset
			rx_digitalreset    			: out std_logic_vector(chNo-1 downto 0);                    					-- rx_digitalreset.rx_digitalreset
			rx_ready           			: out std_logic_vector(chNo-1 downto 0);                    					-- rx_ready.rx_ready
			rx_is_lockedtodata 			: in  std_logic_vector(chNo-1 downto 0) 				:= (others => '0'); 		-- rx_is_lockedtodata.rx_is_lockedtodata
			rx_cal_busy        			: in  std_logic_vector(chNo-1 downto 0) 				:= (others => '0')  		-- rx_cal_busy.rx_cal_busy
		);
	end component reset;

	component reconf
		port (
			reconfig_busy             : out std_logic;                                          						-- reconfig_busy.reconfig_busy
			mgmt_clk_clk              : in  std_logic                       					:= '0';             		-- mgmt_clk_clk.clk
			mgmt_rst_reset            : in  std_logic                       					:= '0';             		-- mgmt_rst_reset.reset
			reconfig_mgmt_address     : in  std_logic_vector(6 downto 0)    					:= (others => '0'); 		-- reconfig_mgmt.address
			reconfig_mgmt_read        : in  std_logic                       					:= '0';             		-- 	          .read
			reconfig_mgmt_readdata    : out std_logic_vector(31 downto 0);                      						--              .readdata
			reconfig_mgmt_waitrequest : out std_logic;                                          						--              .waitrequest
			reconfig_mgmt_write       : in  std_logic                       					:= '0';             		--              .write
			reconfig_mgmt_writedata   : in  std_logic_vector(31 downto 0)   					:= (others => '0'); 		--              .writedata
			reconfig_to_xcvr          : out std_logic_vector(70*2*chNo-1 downto 0);                    				-- reconfig_to_xcvr.reconfig_to_xcvr
			reconfig_from_xcvr        : in  std_logic_vector(46*2*chNo-1 downto 0)  		:= (others => '0')  		-- reconfig_from_xcvr.reconfig_from_xcvr
		);
	end component reconf;

-- -- -- -- -- Signals -- -- -- -- --
	-- Native PHY including reconf and reset
	signal pll_locked																							: std_logic_vector(chNo-1 downto 0);
	signal tx_analogreset, tx_digitalreset 															: std_logic_vector(chNo-1 downto 0);
	signal rx_analogreset, rx_digitalreset       													: std_logic_vector(chNo-1 downto 0);
	signal rx_is_lockedtoref, rx_is_lockedtodata, tx_cal_busy, rx_cal_busy					: std_logic_vector(chNo-1 downto 0);
	signal tx_ready, rx_ready  																			: std_logic_vector(chNo-1 downto 0);
	signal pll_powerdown																						: std_logic;

	-- reconf
	signal reconfig_busy																						: std_logic;
	signal reconfig_to_xcvr       																		: std_logic_vector(70*2*chNo-1 downto 0);
	signal reconfig_from_xcvr       																		: std_logic_vector(46*2*chNo-1 downto 0);
	
	-- clocks
	signal clkrx																								: std_logic_vector(chNo-1 downto 0);
	
	-- ready
	signal st_ready, ready_buf																				: std_logic_vector(chNo-1 downto 0);
	signal cnt_ready0, cnt_ready1, cnt_ready2, cnt_ready3											: unsigned(63 downto 0)						:=(others=>'0');
	signal cnt_ready4, cnt_ready5, cnt_ready6, cnt_ready7											: unsigned(63 downto 0)						:=(others=>'0');
	


begin
-- -- -- trasciever -- -- --
	transciever: native	port map(
		pll_powerdown        => (others=> pll_powerdown),
		tx_analogreset       => tx_analogreset,
		tx_digitalreset      => tx_digitalreset,
		tx_pll_refclk(0)     => CLK100,
		tx_pma_clkout        => CLK_TX,
		tx_serial_data       => SERIAL_TX,
		tx_pma_parallel_data => DATA_TX,
		pll_locked           => pll_locked,
		rx_analogreset       => rx_analogreset,
		rx_digitalreset      => rx_digitalreset,
		rx_cdr_refclk(0)     => CLK100,
		rx_pma_clkout        => clkrx,
		rx_serial_data       => SERIAL_RX,
		rx_pma_parallel_data => DATA_RX,
		rx_clkslip           => BITSLIP,
		rx_is_lockedtoref    => rx_is_lockedtoref,
		rx_is_lockedtodata   => rx_is_lockedtodata,
		rx_seriallpbken      => (others=> LOOPBACK),
		tx_cal_busy          => tx_cal_busy,
		rx_cal_busy          => rx_cal_busy,
		reconfig_to_xcvr     => reconfig_to_xcvr,
		reconfig_from_xcvr   => reconfig_from_xcvr
	);

-- -- -- reset -- -- --
	res: reset	port map(
		clock              => CLK100,
		reset              => not RST,
		pll_powerdown(0)   => pll_powerdown,
		tx_analogreset     => tx_analogreset,
		tx_digitalreset    => tx_digitalreset,
		tx_ready           => tx_ready,
		-- To adjust when number of channels is changed
		pll_locked(0)      => pll_locked(0),
		--pll_locked(0)      => (pll_locked(0) and pll_locked(1) and pll_locked(2) and pll_locked(3) and pll_locked(4) and pll_locked(5) and pll_locked(6) and pll_locked(7)),
		pll_select         => "0",
		tx_cal_busy        => tx_cal_busy,
		rx_analogreset     => rx_analogreset,
		rx_digitalreset    => rx_digitalreset,
		rx_ready           => rx_ready,
		rx_is_lockedtodata => rx_is_lockedtodata,
		rx_cal_busy        => rx_cal_busy
	);


-- -- -- reconfiguration -- -- --
	reconfiguration: reconf	port map(
		reconfig_busy             => reconfig_busy,
		mgmt_clk_clk              => CLK100,
		mgmt_rst_reset            => not RST,
		reconfig_mgmt_address     => (others=>'0'),
		reconfig_mgmt_read        => '0',
		--reconfig_mgmt_readdata    =>,
		--reconfig_mgmt_waitrequest =>,
		reconfig_mgmt_write       => '0',
		reconfig_mgmt_writedata   => (others=>'0'),
		reconfig_to_xcvr          => reconfig_to_xcvr,
		reconfig_from_xcvr        => reconfig_from_xcvr
	);

-- -- -- additional output information, status -- -- --
	-- ready if: 
		-- pll_locked, 
		-- tx_cal_busy and rx_cal_busy are low (no calibration is going on), 
		-- tx_ready, rx_ready
		-- rx_is_lockedtodata
		-- not reconfig_busy
	st_ready(0) <= pll_locked(0) and (not tx_cal_busy(0)) and (not rx_cal_busy(0)) and tx_ready(0) and rx_ready(0) and rx_is_lockedtodata(0) and (not reconfig_busy);
	
	-- To adjust when number of channels is changed
--	st_ready(1) <= pll_locked(1) and (not tx_cal_busy(1)) and (not rx_cal_busy(1)) and tx_ready(1) and rx_ready(1) and rx_is_lockedtodata(1) and (not reconfig_busy);
--	st_ready(2) <= pll_locked(2) and (not tx_cal_busy(2)) and (not rx_cal_busy(2)) and tx_ready(2) and rx_ready(2) and rx_is_lockedtodata(2) and (not reconfig_busy);
--	st_ready(3) <= pll_locked(3) and (not tx_cal_busy(3)) and (not rx_cal_busy(3)) and tx_ready(3) and rx_ready(3) and rx_is_lockedtodata(3) and (not reconfig_busy);
--	st_ready(4) <= pll_locked(4) and (not tx_cal_busy(4)) and (not rx_cal_busy(4)) and tx_ready(4) and rx_ready(4) and rx_is_lockedtodata(4) and (not reconfig_busy);
--	st_ready(5) <= pll_locked(5) and (not tx_cal_busy(5)) and (not rx_cal_busy(5)) and tx_ready(5) and rx_ready(5) and rx_is_lockedtodata(5) and (not reconfig_busy);
--	st_ready(6) <= pll_locked(6) and (not tx_cal_busy(6)) and (not rx_cal_busy(6)) and tx_ready(6) and rx_ready(6) and rx_is_lockedtodata(6) and (not reconfig_busy);
--	st_ready(7) <= pll_locked(7) and (not tx_cal_busy(7)) and (not rx_cal_busy(7)) and tx_ready(7) and rx_ready(7) and rx_is_lockedtodata(7) and (not reconfig_busy);
	
	ready_signal0 : process(clkrx(0)) begin
		if(rising_edge(clkrx(0))) then
			--READY(0) 		<= st_ready(0);
			if(RST = '0') then
				cnt_ready0 <= (others=>'0');						
			else
				if(ready_buf(0) = not st_ready(0) and ready_buf(0) = '1') then
					cnt_ready0 <= cnt_ready0 + 1;
				end if;
				if(ready_buf(1) = not st_ready(1) and ready_buf(1) = '1') then
					cnt_ready1 <= cnt_ready1 + 1;
				end if;
				if(ready_buf(2) = not st_ready(2) and ready_buf(2) = '1') then
					cnt_ready2 <= cnt_ready2 + 1;
				end if;
				if(ready_buf(3) = not st_ready(3) and ready_buf(3) = '1') then
					cnt_ready3 <= cnt_ready3 + 1;
				end if;
				if(ready_buf(4) = not st_ready(4) and ready_buf(4) = '1') then
					cnt_ready4 <= cnt_ready4 + 1;
				end if;
				if(ready_buf(5) = not st_ready(5) and ready_buf(5) = '1') then
					cnt_ready5 <= cnt_ready5 + 1;
				end if;
				if(ready_buf(6) = not st_ready(6) and ready_buf(6) = '1') then
					cnt_ready6 <= cnt_ready6 + 1;
				end if;
				if(ready_buf(7) = not st_ready(7) and ready_buf(7) = '1') then
					cnt_ready7 <= cnt_ready7 + 1;
				end if;
			end if;
			ready_buf(0)	<= st_ready(0);
		end if;
	end process ready_signal0;
	
	READY(chNo-1 downto 0)	<= st_ready(chNo-1 downto 0);
	
	-- clock signal
	CLK_RX <= clkrx;
	deb_cnt_ready <= cnt_ready0 + cnt_ready1 + cnt_ready2 + cnt_ready3 + cnt_ready4 + cnt_ready5 + cnt_ready6 + cnt_ready7; 
	
end architecture RTL;
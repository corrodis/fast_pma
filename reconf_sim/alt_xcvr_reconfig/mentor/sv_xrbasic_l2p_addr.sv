// Copyright (C) 1991-2013 Altera Corporation
// This simulation model contains highly confidential and
// proprietary information of Altera and is being provided
// in accordance with and subject to the protections of the
// applicable Altera Program License Subscription Agreement
// which governs its use and disclosure. Your use of Altera
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Altera Program License Subscription
// Agreement, Altera MegaCore Function License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs for
// use exclusively in logic devices manufactured by Altera and sold
// by Altera or its authorized distributors. Please refer to the
// applicable agreement for further details. Altera products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Altera assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 13.0sp1
// ALTERA_TIMESTAMP:Thu Jun 13 12:36:46 PDT 2013
// encrypted_file_type : mentor_tagged
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
P27hKylmlwq62mn5x+V7bNHR/RO0bszL2VKtnKDfYP6sdYA5fSWK2zO0mMPsFICn
ecun7STeudMVGa0zfRh77VnrUkVTZU4iyNf61tIrIHzb8vihxdf2+C5IvW5ngHag
rikWJrUYFbFnBA7AClm8naoAf+S+ae93elSHCTLeajA=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 2416)
fgcRhnn2iVKzHw3gjN/8Y8Du5R80P/yA/odEA43kwS3CCb4UvPB74QQmii9R/L4R
BQVmaOfCJvkgEYxh5/OOExb7O4yp9ulmyvzO27JXzDds1E23eJdBNol+lx6iQxTg
PBx4jXfSyjSXMt2s3ehNcJQ0UrF+Wy16AnZ1EA78zmS76CNO7jdxRert/bneo882
b1a+9dDrLiy/hjw9Qs9hWtP8eAi1BWaOo+szTdFwA9w6NRHNXJ+Rv2xGohSv9WN1
ZQ+snsz9ZuhQxJCeWWWpVRn0PhCag90hi+3dIGbIv+a5vZ3YNO16AA7S4HTQ0HmG
lbvZAJu4eXSZf8fgxIa/gL3GYaY07y9aROKSw5LvXikWBHy8f/ZEcK4JQn+3M9HQ
KtYgz0GYlTdQ1PfgZ0lzbBcW9b9DKSntM1vimNSzCghSvGaN2OJaPNCvpx9EAjCz
VsslyAW2m/S5C+wLZBc6uxb+gzqGdGog+7krWMZN+FzZw7oRetgnLZvDAPhzwQyu
NlCklFFouDGDUFr3ctyYx4H4TfVU6bxgO3WQwL3lQ6AQwJiLLBRkSqBvv0hxwvA2
xBFsxmf7bjH3Y0XHbJDV/u6r38V7BwXyEDipdGHS/CPyv4Rc3NC2MCMoNIIfxHrS
eNprzppX+RvJ5LlxKR9HypEWV6D86v5IipRXvziumstNiZY2F1PEBF50Zqj9Xaw5
RdGZ4sodpiEnpPgI88z+QEXO1hTHQFszC7GiX/OkzLfeoDWYLICxCUSoBgOqEGxo
vLTcEPwMXXiFOubgyQHOZ8XDUT8CHRWR2QwhcVM6RNQtShhvmm2xrO5zBo4HDqeS
56aOyS6MXOcQKRfkPQpMPKt0TWFNu6PFlVCTO7Snxd2hflWcJZfOaikKAA5TgoQK
HzO610mDe4PHYHgipkbUwNCcjWezb3YCgSS8wkDqUE/rdQFzfPDKkz/e4xDT+0Ab
u+K0Bv8ztOlnlVVvK49b1qfK/dCAjLUhnQuhffKESyvnQdpDezox5s+oEAvHS292
gv6ukwLR8yBlffMSMUG810mhhT+6p6fbf6aQBLJ22ouOkNXSwk5NzQ7mOrgaFSMA
yHfENrVf3rUzQ8VxYAFIrZ0nxCovhPRdCWrfLyAkwjIqtZdbWBBPdPvRlIbwtFfx
9kdJL4bOTERbJoHSVrV9X1t62NI34qc/1YaZjuk1bsLCkp7PsMDzdnEvZm3riGRz
mNOMGmItEt/nX7FXlHi1Bs44HRdKtsmo+jgMkD11bqmmoP7yqlVN/P0N8TkT5y2n
IrSv9tK1wOMSO9cAmT1nzn39kcdqQHhmc3akuMJYjGuCrvvUC6d3Iq8oUXuoEBgJ
F+97wAb3fS5tUDk7Eic6dzQQ2ReuH/aMMZX4O9Ce9R8TOVG37Q+qbLWbgrXtVMiW
eIOO72NdOx2MzXE5v/otugLtK18u/k7sjZsuVj3ln0NsY7tI81QbdeH+toyBwvMA
pfsjuNHJPk9WbUvPpaq+/tv9RzNdyY4LVNSxgdoNnXmLLRrHGv8m4Lpvm0A8CkQJ
F2dodK6QfWltHyjlwXX8Ns7g0Y7jsLHm3wQhb5cju7tTKjB94NQt5cEWOYYC9/gi
uE4JsVe9HIDOuGxzO5g0y9c3mXWV9mLUR+4kwtdi3ZuZPZ3J1kQNAE6993pNaExd
E88tUnA4y+kUVyQG8DlwdMdgzOgnz/wNQp0vN+8x+0v6FTOsaTfhfgj63VczBMhn
OY5wzIy3DFFNThav48g0IKiw8d/gOu7t421D84in2ZIKgx9fFd7/8CJA72Qi/txN
gjlLD5aeFCUKilk2zOtb0VB0++Z9RuAUoFHsK2TDzjLBXd63Ojg3guozI+3u27/I
DWnz7r2O7r1ePgZabqsrgK80thoHHTMQspYORcLIrYGsc5bn2FPsNyoeLcs7HFhF
Zl4vafhs7X0+ACVm65YbE3xWrICiblZr94cRwvC2GPq+KV9nXqNcDacjyIqrpdAc
xZrnWDwlpm0p6kOqIqHHojFmBIjFjFjZeQoq5ca7wjPvPZEZRIXk3BBgnpdr4tCA
0XfJfwGY8pCiH/qaZaLTO7JQ5Y1wyQu+GxO03YYUXBpvOzYwy8w27LTJ45k7sTt8
bn1em7Ycq+nMfv9ouzT0qd5PPDBPB7E/ZlxU9x/Z14IRJ++BXDW2h8I8eo2UHsJ9
Ip9WaNlhsRdzhKLM9tCdBP7xBUHGWdKicxI1M0qkMy8PdHCXjQ4uDReaJ3r+E95N
O/53XdJLJKXntnbsyo8wIpayYkd0Y2QG/StzeoB+twPr97RXRln3pX23CHgAh/57
FQ6RMsDpZ9GYailMot+ExX2EhSYZ3uAFcMQh7NAtTJrSMuxknrCH2BKcTzaXW5G2
uGyM1ta5hDBM8uf1UEE0C2Vo/B7FkQryd0T3Jl2kPK6lZudGoevRVYKBWyS8jYUn
2aR3PkTR/v4tsHTUdsrk0ul2w4pW0vRBQMP1L9GagtHSEQeCVCpQ7p7q59hdI2lX
BORhKUurG9iIB98KhPRanNunpQ2u3/crJhqNqGoDkamorQ6T1keS3+y/eg319ZsQ
BLh08MJpNGvpxOFOPWtZpkTDScxvQvshjAxWSxt7+pMdWnieO+ylatZWnh5aQ5pc
Jn6GQ9mZyGJL7NE4pShWd9vdnf5l40NiekAwgf1z9QJK7Zht0IcZcN5OLc+2mGaX
mk+QJQMu7mJ2d473juWnJKxfbQ0YG65WWs8HwJIiaxxYf5AKMd41bm4/XkbTEooL
jtqeFacUKMnZ2CyADXKNIWBjbcDy+h+5X/pu+8USrYMhmC9zDy55dIfIBGuzQvbh
swDSbxLj0WlcoY2hN4ui7qfRXMkOkTxRJUWSCn52M8odoo+tDfRwbJBIzTw5SS8+
3b0fo03gWZMwIBZ3+9rrmi2dJr74QAxNBvM+xVntitmtQny1m9HhNCakPR2vK7bK
VLy9rxax5pMb+dhbFI2yq5sV76iPqxFwwZyg/S7E7Qympk56CbkDK+8XKpobfmO8
90qLnN3nHPfhZQFuEGX+zvztg06UiB+avSdiup9ZTAooovHtSWC1a6tooaOLuQ5t
FNmfmYQ4yqN1HEEpVyyQEu97cb9tNOjpUOCof34oRenYGwkqYZhrED2joVVvGDVt
Kj06Z3k93vVDXQ+8prBatT8LL108bKN5++KzrFQhPnmpHgofa1zfqtj1hGkoBdk5
eTC7z4OVW8FLuOW5tZMQow==
`pragma protect end_protected

-- Generates date for different BETs
-- Simon Corrodi, corrodi@physi.uni-heidelberg.de, 23/10/2013

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use WORK.constants.all;

entity dataGenerator is
	Port ( CLK 			: in	std_logic;
			 RST			: in  std_logic;											-- active low
			 EN			: in	std_logic;
			 INJ_ERR		: in	std_logic											:= '0';
			 MODE			: in  std_logic_vector(3 downto 0);
			 DATA_IN		: in  std_logic_vector(pmaBitNo-1 downto 0);
			 DATA_OUT	: out std_logic_vector(pmaBitNo-1 downto 0);
			 SEED			: in  std_logic_vector(pmaBitNo-1 downto 0)     := WAP(pmaBitNo-1 downto 0);
			 SET_DATAK	: in    std_logic											:= '0'
	);
end dataGenerator;

architecture RTL of dataGenerator is
	
	constant pattern_alt : std_logic_vector(79 downto 0) 	:= x"AAAAAAAAAAAAAAAAAAAA"; 
	signal data : std_logic_vector(pmaBitNo-1 downto 0) 	:= (others=>'0');
	
	-- inject errors
	signal cnt_wait_error													: integer											:= 0;
	signal set_inj_err														: std_logic											:= '0';
	signal rn																	: std_logic_vector(6 downto 0)				:= "1001110";
	signal error_inj															: std_logic_vector(pmaBitNo-1 downto 0) 	:= (others=>'0');
	
begin

	dataGeneration: process(CLK) begin
		if (rising_edge(CLK)) then
			if(RST = '0') then
				-- synchronous clear, reset
				case MODE is
					when "0000" =>
						--data <= WAP(pmaBitNo-1 downto 0);
						data <= SEED;
					when "0001" =>
						data <=  pattern_alt(pmaBitNo-1 downto 0);
					when others =>
						data <=  pattern_alt(pmaBitNo-1 downto 0);
				end case;
			else
				-- rn for error injection
				rn <=	rn(5 downto 0) & (rn(3) xor rn(2));
				error_inj <= (others=>'0');
				if(set_inj_err = '1') then
					error_inj(to_integer(unsigned(rn))) <= '1';
				end if;
			
				if(EN = '1') then
					if(SET_DATAK = '1') then
						--data <= WAP(pmaBitNo-1 downto 0);
						data <= SEED;
					else
						case mode is
							--when "0000" =>		data 		<= WAP(pmaBitNo-1 downto 0);
							--when "0000" =>		data 		<= SEED;
							--when "0001" =>		data 		<= SEED;
							-- RN pettern
							--  8 bit					
							when "1000" => 	data(pmaBitNo-1 downto 8) 	<= pattern_alt(pmaBitNo-1 downto 8);
													data(7 downto 0) 				<= DATA_IN(6 downto 0) & (DATA_IN(3) xor DATA_IN(2));							
							-- 16 bit					
							when "1001" => 	data(pmaBitNo-1 downto 16)	<= pattern_alt(pmaBitNo-1 downto 16);
													data(15 downto 0) 				<= DATA_IN(14 downto 0) & (DATA_IN(3) xor DATA_IN(2));	
							-- 24 bit					
							when "1010" => 	data(pmaBitNo-1 downto 24) 	<= pattern_alt(pmaBitNo-1 downto 24);
													data(23 downto 0) 				<= DATA_IN(22 downto 0) & (DATA_IN(3) xor DATA_IN(2));			
							-- 32 bit						
							when "1011" => 	data(pmaBitNo-1 downto 32) 	<= pattern_alt(pmaBitNo-1 downto 32);
													data(31 downto 0) 				<= DATA_IN(30 downto 0) & (DATA_IN(3) xor DATA_IN(2));			
							-- 40 bit						
							when "1100" => 	data(pmaBitNo-1 downto 40) 	<= pattern_alt(pmaBitNo-1 downto 40);
													data(39 downto 0) 				<= DATA_IN(38 downto 0) & (DATA_IN(3) xor DATA_IN(2));
							-- 48 bit						
							when "1101" => 	data(pmaBitNo-1 downto 48) 	<= pattern_alt(pmaBitNo-1 downto 48);
													data(47 downto 0) 				<= DATA_IN(46 downto 0) & (DATA_IN(3) xor DATA_IN(2));					
							-- 56 bit						
							when "1110" => 	data(pmaBitNo-1 downto 56) 	<= pattern_alt(pmaBitNo-1 downto 56);
													data(55 downto 0) 				<= DATA_IN(54 downto 0) & (DATA_IN(3) xor DATA_IN(2));	
							-- 60 bit						
							when "1111" => 	data(pmaBitNo-1 downto 60) 	<= pattern_alt(pmaBitNo-1 downto 60);
													data(59 downto 0) 				<= DATA_IN(58 downto 0) & (DATA_IN(3) xor DATA_IN(2));	
							-- 62 bit						
							when "0100" => 	data(pmaBitNo-1 downto 62) 	<= pattern_alt(pmaBitNo-1 downto 62);
													data(61 downto 0) 				<= DATA_IN(60 downto 0) & (DATA_IN(3) xor DATA_IN(2));				
							-- 64 bit						
							when "0101" => 	data(pmaBitNo-1 downto 64) 	<= pattern_alt(pmaBitNo-1 downto 64);
													data(63 downto 0) 				<= DATA_IN(62 downto 0) & (DATA_IN(3) xor DATA_IN(2));
							-- 72 bit						
							when "0110" => 	data(pmaBitNo-1 downto 72) 	<= pattern_alt(pmaBitNo-1 downto 72);
													data(71 downto 0) 				<= DATA_IN(70 downto 0) & (DATA_IN(3) xor DATA_IN(2));
							-- 76 bit						
							when "0111" => 	data(pmaBitNo-1 downto 76) 	<= pattern_alt(pmaBitNo-1 downto 76);
													data(75 downto 0) 				<= DATA_IN(74 downto 0) & (DATA_IN(3) xor DATA_IN(2));
							-- 78 bit						
							--when "0011" => 	data(pmaBitNo-1 downto 78) 	<= pattern_alt(pmaBitNo-1 downto 78);
													data(77 downto 0) 				<= DATA_IN(76 downto 0) & (DATA_IN(3) xor DATA_IN(2));
							-- 80 bit	
								-- 7bit rn
	--							when "0000" => 	data(pmaBitNo-1 downto 0) 	<= DATA_IN(pmaBitNo-2 downto 0) & (DATA_IN(6) xor DATA_IN(5));
	--							-- 9bit rn
	--							when "0001" => 	data(pmaBitNo-1 downto 0) 	<= DATA_IN(pmaBitNo-2 downto 0) & (DATA_IN(8) xor DATA_IN(4));
	--							-- 11bit rn
	--							when "0010" => 	data(pmaBitNo-1 downto 0) 	<= DATA_IN(pmaBitNo-2 downto 0) & (DATA_IN(10) xor DATA_IN(8));
	--							-- 12bit rn
	--							when "0000" => 	data(pmaBitNo-1 downto 0) 	<= DATA_IN(pmaBitNo-2 downto 0) & (DATA_IN(11) xor DATA_IN(10) xor DATA_IN(9) xor DATA_IN(3));
	--							-- 13bit rn
	--							when "0001" => 	data(pmaBitNo-1 downto 0) 	<= DATA_IN(pmaBitNo-2 downto 0) & (DATA_IN(12) xor DATA_IN(11) xor DATA_IN(9) xor DATA_IN(7));
	--							-- 14bit rn
	--							when "0010" => 	data(pmaBitNo-1 downto 0) 	<= DATA_IN(pmaBitNo-2 downto 0) & (DATA_IN(13) xor DATA_IN(12) xor DATA_IN(11) xor DATA_IN(1));
	--							-- 15bit rn
	--							when "0011" => 	data(pmaBitNo-1 downto 0) 	<= DATA_IN(pmaBitNo-2 downto 0) & (DATA_IN(14) xor DATA_IN(13));
--								-- 16bit rn
								when "0000" => 	data(pmaBitNo/2-1 downto 0) 			<= DATA_IN(pmaBitNo/2-2 downto 0) & (DATA_IN(15) xor DATA_IN(14) xor DATA_IN(12) xor DATA_IN(3));
														data(pmaBitNo-1 downto pmaBitNo/2) 	<= DATA_IN(pmaBitNo-2 downto pmaBitNo/2) & (DATA_IN(15+pmaBitNo/2) xor DATA_IN(14+pmaBitNo/2) xor DATA_IN(12+pmaBitNo/2) xor DATA_IN(3+pmaBitNo/2));
--								-- 17bit rn
--								when "0001" => 	data(pmaBitNo-1 downto 0) 	<= DATA_IN(pmaBitNo-2 downto 0) & (DATA_IN(16) xor DATA_IN(13));
--								-- 18bit rn
								when "0010" => 	data(pmaBitNo/2-1 downto 0) 			<= DATA_IN(pmaBitNo/2-2 downto 0) & (DATA_IN(17) xor DATA_IN(10));
														data(pmaBitNo-1 downto pmaBitNo/2) 	<= DATA_IN(pmaBitNo-2 downto pmaBitNo/2) & (DATA_IN(17+pmaBitNo/2) xor DATA_IN(10+pmaBitNo/2));
--								-- 19bit rn
								--when "0000" => 	data(pmaBitNo-1 downto 0) 	<= DATA_IN(pmaBitNo-2 downto 0) & (DATA_IN(18) xor DATA_IN(5)  xor DATA_IN(1)  xor DATA_IN(0));

								-- 20bit rn
								when "0001" => 	data(pmaBitNo/2-1 downto 0) 			<= DATA_IN(pmaBitNo/2-2 downto 0) & (DATA_IN(19) xor DATA_IN(16));
														data(pmaBitNo-1 downto pmaBitNo/2) 	<= DATA_IN(pmaBitNo-2 downto pmaBitNo/2) & (DATA_IN(19+pmaBitNo/2) xor DATA_IN(16+pmaBitNo/2));
								-- 25bit rn
								--when "0001" => 	data(pmaBitNo-1 downto 0) 	<= DATA_IN(pmaBitNo-2 downto 0) & (DATA_IN(24) xor DATA_IN(21));
								-- 31bit rn -- -- -- PRB31 -- -- --
								when "0011" => 	data(pmaBitNo/2-1 downto 0) 			<= DATA_IN(pmaBitNo/2-2 downto 0) & (DATA_IN(30) xor DATA_IN(27));
														data(pmaBitNo-1 downto pmaBitNo/2) 	<= DATA_IN(pmaBitNo-2 downto pmaBitNo/2) & (DATA_IN(30+pmaBitNo/2) xor DATA_IN(27+pmaBitNo/2));
								-- 35bit rn
								--when "0011" => 	data(pmaBitNo-1 downto 0) 	<= DATA_IN(pmaBitNo-2 downto 0) & (DATA_IN(34) xor DATA_IN(32));

							when others =>		data 		<=  pattern_alt(pmaBitNo-1 downto 0);
						end case;
					end if;
					
					if(cnt_wait_error > 100000000*3) then
						if(INJ_ERR = '1') then
							set_inj_err <= '1';
							cnt_wait_error <= 1;
						else
							set_inj_err <= '0';
						end if;
					else
						cnt_wait_error <= cnt_wait_error + 1;
						set_inj_err <= '0';
					end if;
--				else 
--					data <= DATA_IN;
				end if;
			end if;
		end if;
	end process dataGeneration;
	
	DATA_OUT <= data xor error_inj;
	
--	with set_inj_err select
--		DATA_OUT <= data xor error_inj 	when '1',
--						data					  	when '0',
--						data						when others;
	
end RTL;			 
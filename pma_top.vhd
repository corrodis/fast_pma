-- Clean only PMA version for HSMA and HSMB cages
-- Made to test different transcieving modes
-- pma.vhd has to be adjusted, when ever the rate, interface width or number of channels is changed
-- 18/12/2013 Simon Corrodi and Cartsen

library IEEE;
package constants is
	use IEEE.std_logic_1164.all;
	constant chNo 				:integer :=8;
	constant pmaBitNo 		:integer :=80;
	constant WAP				: std_logic_vector(79 downto 0) := x"ABBA12578FCDEFDAB58A";
--	constant SEED0				: std_logic_vector(79 downto 0) := x"ABBA12578FCDEFDAB57A";
--	constant SEED1				: std_logic_vector(79 downto 0) := x"12578FCDEFDABAAAABBA";
--	constant SEED2				: std_logic_vector(79 downto 0) := x"AAAA8AADEFDAB58AABBA";
--	constant SEED3				: std_logic_vector(79 downto 0) := x"135AABBDEFDAB58AABBA";
--	constant SEED4				: std_logic_vector(79 downto 0) := x"145AAFCDEFDB58AAAAAA";
--	constant SEED5				: std_logic_vector(79 downto 0) := x"95578FCAEFDAB58AFB1A";
	constant SEED0				: std_logic_vector(79 downto 0) := x"123456789EFDB58AABBD";
	constant SEED1				: std_logic_vector(79 downto 0) := x"17578FCDEFD123456786";
	constant SEED2				: std_logic_vector(79 downto 0) := x"123456789EFDB58AABBC";
	constant SEED3				: std_logic_vector(79 downto 0) := x"17578FCDEFD123456787";
	constant SEED4				: std_logic_vector(79 downto 0) := x"123456789EFDB58AABBB";
	constant SEED5				: std_logic_vector(79 downto 0) := x"17578FCDEFD123456788";
	constant SEED6				: std_logic_vector(79 downto 0) := x"123456789EFDB58AABBA";
	constant SEED7				: std_logic_vector(79 downto 0) := x"17578FCDEFD123456789";
	
	constant en_autoDataK	: std_logic							  := '1';
	constant en_ParityCheck	: std_logic							  := '0';
	constant en_Scrambler	: std_logic							  := '1';
end constants;


library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use WORK.constants.all;

-- top_level
entity pma_top is
	Port (
		    -- GPLL_CLK 															-- 8 pins
			 clkin_50		: in    std_logic;							  	-- 1.8V    50 MHz: out also to EPM2210F256
--			 clkintop_p		: in    std_logic_vector(1 downto 0);	  	-- LVDS    100 MHz prog osc External Term.
--			 clkinbot_p		: in    std_logic_vector(1 downto 0);    	-- LVDS    100 MHz prog osc clkinbot_p[0]: out clkinbot_p[1] External Term.
--			 clk_125_p		: in    std_logic;                      	-- LVDS    125 MHz GPLL-req's OCT.

		    -- XCVR-REFCLK													   -- 16 pins req's ALTGXB instatiation
			 refclk0_qr0_p : in    std_logic;
--			 refclk1_ql0_p	: in    std_logic;								-- Default 100MHz
--			 refclk2_ql1_p	: in    std_logic;								-- Default 644.53125MHz
--			 refclk4_ql2_p	: in    std_logic;								-- Default 282.5MHz
--			 refclk5_ql2_p	: in    std_logic;								-- Default 148.5MHz
--			 refclk0_qr0_p	: in    std_logic;								-- Default 100MHz
--			 refclk1_qr0_p	: in    std_logic;								-- Default 156.25MHz
--			 refclk2_qr1_p	: in    std_logic;								-- Default 625MHz
--			 refclk4_qr2_p	: in    std_logic;								-- Default 100MHz
--			 refclk5_qr2_p	: in    std_logic);								-- Default 270MHz (DisplayPort)

			 -- Si571 VCXO															-- 2 pins
--			 sdi_clk148_up : out   std_logic;								
--			 sdi_clk148_dn : out   std_logic;

			 -- DDR3 																-- 125pins
--	       ddr3_a			: out   std_logic_vector(13 downto 0);		-- SSTL15    Address
--			 ddr3_ba			: out   std_logic_vector(2 downto 0);		-- SSTL15    Bank Address
--			 ddr3_casn		: out   std_logic;								-- SSTL15    Column Address Strobe
--			 ddr3_clk_n		: out   std_logic;								-- SSTL15    Diff Clock - Neg
--			 ddr3_clk_p		: out   std_logic;								-- SSTL15    Diff Clock - Pos
--			 ddr3_cke		: out   std_logic;								-- SSTL15    Clock Enable
--			 ddr3_csn		: out   std_logic;								-- SSTL15    Chip Select
--			 ddr3_dm			: out   std_logic_vector(8 downto 0);		-- SSTL15    Data Write Mask
--			 ddr3_dq			: in    std_logic_vector(71 downto 0);		-- SSTL15    Data Bus
--			 ddr3_dqs_n		: in    std_logic_vector(8 downto 0);		-- SSTL15    Diff Data Strobe - Neg
--			 ddr3_dqs_p		: in    std_logic_vector(8 downto 0);		-- SSTL15    Diff Data Strobe - Pos
--			 ddr3_odt		: out   std_logic;								-- SSTL15    On-Die Termination Enable
--			 ddr3_rasn		: out   std_logic;								-- SSTL15    Row Address Strobe
--			 ddr3_resetn	: out   std_logic;								-- SSTL15    Reset
--			 ddr3_wen		: out   std_logic;								-- SSTL15    Write Enable
--			 rzqin_1p5		: in    std_logic;								-- OCT Pin in Bank 4A

			 -- QDR2																	-- 66 pins
--	       qdrii_a			: out   std_logic_vector(19 downto 0);		-- HSTL15/18    Address
--			 qdrii_bwsn		: out   std_logic_vector(1 downto 0);		-- HSTL15/18    Byte Write Select
--			 qdrii_cq_n		: in    std_logic;								-- HSTL15/18    Read Data Clock - Neg
--			 qdrii_cq_p		: in    std_logic;								-- HSTL15/18    Read Data Clock - Pos
--			 qdrii_d			: out   std_logic_vector(17 downto 0);		-- HSTL15/18    Write Data
--			 qdrii_doffn	: out   std_logic;								-- HSTL15/18    PLL disable (TR=0)
--			 qdrii_k_n		: out	  std_logic;								-- HSTL15/18    Write Data Clock - Neg
--			 qdrii_k_p		: out	  std_logic;								-- HSTL15/18    Write Data Clock - Pos
--			 qdrii_q			: in    std_logic_vector(17 downto 0);		-- HSTL15/18    Read Data
			 -- qdrii_odt		: out   std_logic;								-- HSTL15/18    On-Die Termination Enable (QDRII Cn)
			 -- qdrii_c_p		: in    std_logic;								-- HSTL15/18    Read Data Valid	(QDRII Cp)
			 -- qdrii_qvld		: in    std_logic;								-- HSTL15/18    Read Data Valid	(QDRII Cp)
--			 qdrii_rpsn		: out   std_logic;								-- HSTL15/18    Read Port Select
--			 qdrii_wpsn		: out   std_logic;								-- HSTL15/18    Write Port Select
--			 rzqin_1p8		: in    std_logic;								-- OCT pin for QDRII/+ and RLDRAM II

			 -- RLDRAM2 															-- 58 pins
--	       rldc_a			: out   std_logic_vector(22 downto 0);		-- HSTL15/18    Address
--			 rldc_ba			: out   std_logic_vector(2 downto 0);		-- HSTL15/18    Bank Address
--			 rldc_ck_n		: out   std_logic;								-- HSTL15/18    Input Clock - Neg
--			 rldc_ck_p		: out   std_logic;								-- HSTL15/18    Input Clock - Pos
--			 rldc_dq			: inout std_logic_vector(17 downto 0);		-- HSTL15/18    Data
--			 rldc_dk_n		: out   std_logic;								-- HSTL15/18    Write (Input) Data Clock - Neg
--			 rldc_dk_p		: out   std_logic;								-- HSTL15/18    Write (Input) Data Clock - Pos
--			 rldc_qk_n		: in    std_logic_vector(1 downto 0);		-- HSTL15/18    Read (Output) Data Clock - Neg
--			 rldc_qk_p		: in 	  std_logic_vector(1 downto 0);		-- HSTL15/18    Read (Output) Data Clock - Pos
--			 rldc_dm			: out	  std_logic;								-- HSTL15/18    Input Data Mask
--			 rldc_qvld		: in 	  std_logic;								-- HSTL15/18    Read Data Valid
--			 rldc_csn		: out	  std_logic;								-- HSTL15/18    Chip Select
--			 rldc_wen		: out	  std_logic;								-- HSTL15/18    Write Enable
--			 rldc_refn		: out	  std_logic;								-- HSTL15/18    Ref Command

			 -- Ethernet   														-- 8 pins
--	       enet_intn		: in    std_logic;								-- 2.5V    MDIO Interrupt (TR=0)
--			 enet_mdc		: out   std_logic;								-- 2.5V    MDIO Clock (TR=0)
--			 enet_mdio		: inout std_logic;								-- 2.5V    MDIO Data (TR=0)
--			 enet_resetn	: out   std_logic;								-- 2.5V    Device Reset (TR=0)
--			 enet_rx_p		: in	  std_logic;								-- LVDS NEED EXTERNAL TERM   SGMII Receive-req's OCT
--			 enet_tx_p		: out   std_logic;								-- LVDS    SGMII Transmit

          -- FSM																	-- 74 pins
	       -- fm_a				: out   std_logic_vector(26 downto 0);		-- 1.8V    Address
--			 fm_d				: in    std_logic_vector(31 downto 0);		-- 1.8V    Data
--			 flash_advn		: out   std_logic;								-- 1.8V    Flash Address Valid
--			 flash_cen		: out   std_logic_vector(1 downto 0);		-- 1.8V    Flash Chip Enable
--			 flash_clk		: out   std_logic;								-- 1.8V    Flash Clock
--			 flash_oen		: out   std_logic;								-- 1.8V    Flash Output Enable
--			 flash_rdybsyn	: in    std_logic_vector(1 downto 0);		-- 1.8V    Flash Ready/Busy
--			 flash_resetn	: out   std_logic;								-- 1.8V    Flash Reset
--			 flash_wen		: out   std_logic;								-- 1.8V    Flash Write Enable
--			 max5_ben		: out   std_logic_vector(3 downto 0);		-- 1.5V    Max V Byte Enable Per Byte
--			 max5_clk		: in    std_logic;								-- 1.5V    Max V Clk
--			 max5_csn		: out   std_logic;								-- 1.5V    Max V Chip Select
--			 max5_oen		: out   std_logic;								-- 1.5V    Max V Output Enable
--			 max5_wen		: out   std_logic;								-- 1.5V    Max V Write Enable		
			 
			 -- Configuration														-- 32 pins
			 -- fpga_data		: out std_logic_vector(31 downto 0);		-- 2.5V    Configuration Data
			 
			 -- Character-LCD														-- 11 pins
			 lcd_csn       : out    std_logic;  							-- 2.5V    LCD Chip Select
			 lcd_d_cn      : out    std_logic; 								-- 2.5V LCD Data / Command Select
			 lcd_data      : inout  std_logic_vector(7 downto 0);	 	-- 2.5V    LCD Data
			 lcd_wen       : out    std_logic; 								-- 2.5V    LCD Write Enable
			 
			 -- User-IO																 -- 27 pins
			 user_dipsw		: in     std_logic_vector(7 downto 0);	    -- HSMB_VAR    User DIP Switches (TR=0)
			 user_led_g    : out    std_logic_vector(7 downto 0);	    -- 2.5V	       User LEDs
			 user_led_r    : out    std_logic_vector(7 downto 0);	    -- 2.5V/1.8V   User LEDs
			 user_pb       : in     std_logic_vector(2 downto 0);	    -- HSMB_VAR    User Pushbuttons (TR=0)
--			 cpu_resetn    : in     std_logic;	 							 -- 2.5V        CPU Reset Pushbutton (TR=0)
			 
			 -- PCI-Express														 -- 25 pins 
--			 pcie_rx_p     : in     std_logic_vector(7 downto 0);		 -- PCML14    PCIe Receive Data-req's OCT
--			 pcie_tx_p     : out    std_logic_vector(7 downto 0);		 -- PCML14    PCIe Transmit Data
--			 pcie_refclk_p : in     std_logic;	 							 -- HCSL      PCIe Clock- Terminate on MB
--			 pcie_led_g3   : out    std_logic;	 							 -- 2.5V      User LED - Labeled Gen3
--			 pcie_led_g2   : out    std_logic;	  							 -- 2.5V      User LED - Labeled Gen2
--			 pcie_led_x1   : out    std_logic;	 							 -- 2.5V      User LED - Labeled x1
--			 pcie_led_x4   : out    std_logic;	  							 -- 2.5V      User LED - Labeled x4
--			 pcie_led_x8   : out    std_logic;	  							 -- 2.5V      User LED - Labeled x8
--			 pcie_perstn   : in     std_logic;	 							 -- 2.5V      PCIe Reset 
--			 pcie_smbclk   : in     std_logic;	 							 -- 2.5V      SMBus Clock (TR=0)
--			 pcie_smbdat   : in     std_logic;	 							 -- 2.5V      SMBus Data (TR=0)
--			 pcie_waken    : out    std_logic;	  							 -- 2.5V      PCIe Wake-Up (TR=0) 
                                                 						 -- must install 0-ohm resistor

			 -- USB 2.0																 -- 19 pins
--			 usb_data		: in     std_logic_vector(7 downto 0);		 -- 1.5V from MAXV
--			 usb_addr		: in     std_logic_vector(1 downto 0);		 -- 1.5V from MAXV
--			 usb_clk			: in     std_logic;	 							 -- 3.3V from Cypress USB
--			 usb_full		: out    std_logic;	 							 -- 1.5V from MAXV
--			 usb_empty		: out    std_logic;	 							 -- 1.5V from MAXV
--			 usb_scl			: in     std_logic;	 							 -- 1.5V from MAXV
--			 usb_sda			: in     std_logic;	 							 -- 1.5V from MAXV
--			 usb_oen			: in     std_logic;	 							 -- 1.5V from MAXV
--			 usb_rdn			: in     std_logic;	 							 -- 1.5V from MAXV
--			 usb_wrn			: in     std_logic;	 							 -- 1.5V from MAXV
--			 usb_resetn		: in     std_logic;	 							 -- 1.5V from MAXV

			 -- QSFP 																 -- 23 pins
			 --qsfp_tx_p		: out    std_logic_vector(3 downto 0);
			 --qsfp_rx_p		: in     std_logic_vector(3 downto 0);
--			 qsfp_mod_seln	: out    std_logic;
--			 qsfp_rstn		: out    std_logic;
--			 qsfp_scl		: out    std_logic;
--			 qsfp_sda		: inout  std_logic;
--			 qsfp_interruptn : in   std_logic;
--			 qsfp_mod_prsn	: in     std_logic;
--			 qsfp_lp_mode	: out    std_logic;
	
			 -- DispayPort x4														 -- 12 pins
			 -- dp_ml_lane_p	: out	   std_logic_vector(3 downto 0);	 -- Transceiver Data
--			 dp_aux_p		: in 	   std_logic;								 -- LVDS (bi-directional) Auxillary Channel
--			 dp_aux_tx_p	: out    std_logic;				 				 -- LVDS (transmit side) Auxillary Channel
			 -- dp_aux_ch_p	: inout 	   std_logic;								 -- LVDS (bi-directional) Auxillary Channel
			 -- dp_aux_ch_n	: inout 	   std_logic;								 -- LVDS (bi-directional) Auxillary Channel
--			 dp_hot_plug	: in 	   std_logic;								 -- 2.5V  Hot Plug Detect
--			 dp_return		: out	   std_logic;								 -- 2.5V  Return for power
--			 dp_direction	: out	   std_logic;								 -- 2.5V  Direction Select on M-LVDS Transceiver

			 -- SDI-Video-Port													 -- 7 pins
			 -- sdi_rx_p		: in 	   std_logic;	          				 -- PCML14   -- SDI Video : in-req's OCT
			 -- sdi_tx_p		: out	   std_logic;	          				 -- PCML14   -- SDI Video : out
			 -- sdi_clk148_dn	: out	   std_logic;	     				 		 -- 2.5V     -- VCO Frequency Down
			 -- sdi_clk148_up	: out	   std_logic;	     						 -- 2.5V     -- VCO Frequency Up
--			 sdi_tx_sd_hdn	: out	   std_logic;	       					 -- 2.5V     -- HD Mode Enable
--			 sdi_tx_en		: out	   std_logic;								 -- 2.5V   -- Transmit Enable
--			 sdi_rx_en		: out	   std_logic;								 -- 2.5V   -- Receive Enable - Tri-state
--			 sdi_rx_bypass	: out	   std_logic;								 -- 2.5V   -- Receive Bypass

			 -- Transceiver-SMA-: 												 -- 2 pins
			 --sma_tx_p		: out     std_logic;   							 -- PCML14   -- SMA : out Pair

			 -- HSMC-Port-A													 	 -- 107 pins
			 --hsma_rx_p		: in     std_logic_vector(7 downto 7);	 	 -- PCML14   -- HSMA Receive Data-req's OCT
			 --hsma_tx_p0		: out	   std_logic;							    -- PCML14   -- HSMA Transmit Data
			 --hsma_tx_p2		: out	   std_logic;							    -- PCML14   -- HSMA Transmit Data
			 --hsma_tx_p_user : out 	std_logic_vector(4 downto 4);
																						 -- Enable below for CMOS HSMC        
			 -- hsma_d			: inout 	   std_logic_vector(79 downto 0);	 -- 2.5V     -- HSMA CMOS Data Bus
																						 -- Enable below for LVDS HSMC        
--			 hsma_clk_in0	: in 	   std_logic;	         				 -- 2.5V     -- Primary single-ended CLKIN
			 hsma_clk_in_p1 : out 		std_logic;	    					    -- LVDS     -- Secondary diff. CLKIN
			 hsma_clk_in_n1 : out 		std_logic;	    					    -- LVDS     -- Secondary diff. CLKIN
			 hsma_clk_in_p2 : out 		std_logic;	      					 -- LVDS     -- Primary Source-Sync CLKIN
			 hsma_clk_in_n2 : out 		std_logic;	      					 -- LVDS     -- Primary Source-Sync CLKIN
--			 hsma_clk_out0	: out	   std_logic;	     					    -- 2.5V     -- Primary single-ended CLKOUT
			 hsma_clk_out_p1 :out	std_logic;	      					 -- LVDS     -- Secondary diff. CLKOUT
			 hsma_clk_out_n1 :out	std_logic;	      					 -- LVDS     -- Secondary diff. CLKOUT
			 hsma_clk_out_p2 :out	std_logic;	     						 -- LVDS     -- Primary Source-Sync CLKOUT
			 hsma_clk_out_n2 :out	std_logic;	     						 -- LVDS     -- Primary Source-Sync CLKOUT
			 hsma_d			: out	std_logic_vector(3 downto 0); 	 	 -- 2.5V     -- Dedicated CMOS IO
--			 hsma_prsntn	: in	   std_logic;	         				 -- 2.5V     -- HSMC Presence Detect : in
			 hsma_rx_d_p	: out	   std_logic_vector(16 downto 0);	 -- LVDS     -- LVDS Sounce-Sync : in
			 hsma_rx_d_n	: out	   std_logic_vector(16 downto 0);	 -- LVDS     -- LVDS Sounce-Sync : in
			 hsma_tx_d_p	: out	   std_logic_vector(16 downto  0);	 -- LVDS     -- LVDS Sounce-Sync : out
			 hsma_tx_d_n	: out	   std_logic_vector(16 downto  0);
--			 hsma_rx_led	: out	   std_logic;	         				 -- 2.5V     -- User LED - Labeled RX
--			 hsma_scl		: out	   std_logic;	            			 -- 2.5V     -- SMBus Clock
--			 hsma_sda		: in	   std_logic;	            			 -- 2.5V     -- SMBus Data
--			 hsma_tx_led	: out	   std_logic;	         				 -- 2.5V     -- User LED - Labeled TX
	

			 -- HSMC-Port-B	 												 	 -- 107pins 
			 -- hsmb_rx_p		: in 	   std_logic_vector(0 downto 0);	    -- PCML14   -- HSMB Receive Data-req's OCT
			 --hsmb_tx_p		: out	   std_logic_vector(1 downto 1);     -- PCML14   -- HSMB Transmit Data
																						 -- Enable below for CMOS HSMC        
			 -- hsmb_d			: in 	   std_logic_vector(79 downto 0);	 -- 2.5V     -- HSMB CMOS Data Bus
																						 -- Enable below for LVDS HSMC        
--			 hsmb_clk_in0	: in 	   std_logic;	        				    -- 2.5V     -- Primary single-ended CLKIN
--			 hsmb_clk_in_p1 : in  	std_logic;	     						 -- LVDS     -- Secondary diff. CLKIN
			 hsmb_clk_in_p2 : out 	std_logic;	      					 -- LVDS     -- Primary Source-Sync CLKIN
			 hsmb_clk_in_n2 : out 	std_logic;	      					 -- LVDS     -- Primary Source-Sync CLKIN
--			 hsmb_clk_out0	: out	   std_logic;	       					 -- 2.5V     -- Primary single-ended CLKOUT
--			 hsmb_clk_out_p1 : out	std_logic;	     						 -- LVDS     -- Secondary diff. CLKOUT
--			 hsmb_clk_out_p2 : out	std_logic;	    						 -- LVDS     -- Primary Source-Sync CLKOUT
--			 hsmb_d: inout	    		std_logic_vector(3 downto 0)      -- 2.5V     -- Dedicated CMOS IO
																						 -- DQS Standard - 1.5V/1.8V/2.5V standards  --  -- /
			 hsmb_a			: inout	std_logic_vector(15 downto 0);	 -- Address 
--			 hsmb_addr_cmd	: inout	std_logic_vector(0 downto 0);		 -- Additional Addres/Command pins
			 hsmb_ba			: inout	std_logic_vector(3 downto 0);		 -- Bank Address
			 hsmb_casn		: inout	std_logic;					  
			 hsmb_rasn		: inout	std_logic;								  
--			 hsmb_wen		: inout	std_logic;								  
			 hsmb_cke		: inout	std_logic;					 			 -- Clock Enable
			 hsmb_csn		: inout	std_logic;								 -- Chip Select
			 -- hsmb_c_p		: out	   std_logic;								 -- c_p = QVLD; c_n = ODT
--			 hsmb_odt		: inout	std_logic;								 -- ODT
--			 hsmb_qvld		: inout	std_logic;								 -- QVLD
			 hsmb_dm			: inout	std_logic_vector(3 downto 0);		 -- Data Mask
--			 hsmb_dq			: inout	std_logic_vector(31 downto 0);	 -- Data
			 hsmb_dqs_p		: inout	std_logic_vector(3 downto 0);		 -- Data Strobe positive
			 hsmb_dqs_n		: inout	std_logic_vector(3 downto 0);		 -- Data Strobe negative
	
--			 hsmb_prsntn	: in 	   std_logic;	         				 -- 2.5V     -- HSMC Presence Detect : in
--			 hsmb_rx_led	: out	   std_logic;	         				 -- 2.5V     -- User LED - Labeled RX
--			 hsmb_scl		: out	   std_logic;	            			 -- 2.5V     -- SMBus Clock
--			 hsmb_sda		: in	   std_logic;	            			 -- 2.5V     -- SMBus Data
--			 hsmb_tx_led	: out 	std_logic;	         			    -- 2.5V     -- User LED - Labeled TX
	
--			 rzqin_hsmb_var : in 	std_logic

			 --hsma_rx_p		: in     std_logic_vector(7 downto 7);	 	 -- PCML14   -- HSMA Receive Data-req's OCT
			 --hsma_tx_p		: out     std_logic_vector(0 downto 0)	 	 -- PCML14   -- HSMA Receive Data-req's OCT
			 --hsmb_rx_p		: in 	   std_logic_vector(0 downto 0)	    -- PCML14   -- HSMB Receive Data-req's OCT
			 hsma_in 	: in 	   	std_logic_vector(chNo-1 downto 0);
			 hsma_out	: out 	   std_logic_vector(chNo-1 downto 0)
	);	
end pma_top;

architecture RTL of pma_top is

	component pma
		port(		
			CLK100								: in  std_logic													:= '0';						-- clk used for transceivers, for 8Gbps a CLK125 could be used
			CLK50									: in  std_logic													:= '0';
			CLK_TX								: out  std_logic_vector(chNo-1 downto 0);													-- clk which should drive input data
			CLK_RX								: out std_logic_vector(chNo-1 downto 0);													-- clk which drives output data
			RST									: in  std_logic													:= '0';						-- active low
			DATA_TX								: in  std_logic_vector (chNo*pmaBitNo-1 downto 0)		:= (others=>'0');			-- chNo*pmaBitNo
			DATA_RX								: out std_logic_vector (chNo*pmaBitNo-1 downto 0);										-- chNo*pmaBitNo
			SERIAL_TX							: out std_logic_vector(chNo-1 downto 0);
			SERIAL_RX							: in  std_logic_vector(chNo-1 downto 0);
			BITSLIP								: in  std_logic_vector(chNo-1 downto 0)					:= (others=>'0');
			READY									: out std_logic_vector(chNo-1 downto 0)					:= (others=>'0');
			LOOPBACK								: in  std_logic													:= '0';
			deb_cnt_ready						: out unsigned(63 downto 0)
		);
	end component pma;
	
	component dataGenerator 
		Port ( 
			CLK 									: in	std_logic;
			RST									: in  std_logic;																					-- active low
			EN										: in	std_logic;
			INJ_ERR								: in	std_logic											:= '0';
			MODE									: in  std_logic_vector(3 downto 0);
			DATA_IN								: in  std_logic_vector(pmaBitNo-1 downto 0);
			DATA_OUT								: out std_logic_vector(pmaBitNo-1 downto 0);
			SEED									: in  std_logic_vector(pmaBitNo-1 downto 0)     := WAP(pmaBitNo-1 downto 0);
			SET_DATAK							: in  std_logic											:= '0'
		);
	end component dataGenerator;
	
	component ParityControler is
		Port ( 
			CLK 				: in	std_logic;
			RST				: in  std_logic;											-- active low
			EN				: in  std_logic										:= '1';
			DATA_IN			: in	std_logic_vector(pmaBitNo-1 downto 0);
			DATA_OUT		: out std_logic_vector(pmaBitNo-1 downto 0) := (others=>'0')
		);
	end component ParityControler;
	
	component dataChecker_v2
		Port (
			CLK								: in    std_logic;
			RST								: in    std_logic													:= '0';		-- active low 
			RST_CNT							: in    std_logic													:= '0';		-- active high	
			DATA								: in    std_logic_vector(pmaBitNo-1 downto 0);
			SEED								: in    std_logic_vector(pmaBitNo-1 downto 0);
			MODE								: in 	  std_logic_vector(3 downto 0);
			ERROR_NO							: out   unsigned(63 downto 0);
			TESTED_NO						: out   unsigned(63 downto 0);
			SYNC								: out   std_logic;
			REC								: out   std_logic;
			BITSLIP							: inout std_logic													:= '0';
			LOSTSYNC_NO						: out	  unsigned(63 downto 0);
			DEB_SETRST						: out   std_logic													:= '1';
			DEB_SETDATAK					: out   std_logic													:= '0';
			deb_pattern1					: out   std_logic_vector(pmaBitNo-1 downto 0)			:= (others=>'0');
			deb_pattern2					: out   std_logic_vector(pmaBitNo-1 downto 0)			:= (others=>'0');
			deb_patternas					: in    std_logic													:= '0'
			);
	end component dataChecker_v2;
	
	component controllerLine
		port (
			line1 						: in std_logic_vector(63 downto 0);											-- 64bit pattern that is shown in hex on the LCD, 64/4=16 chars possible
			line2 						: in std_logic_vector(63 downto 0);
			clock							: in std_logic;																	-- Reference Clock
			reset							: in std_logic;																	-- Manual reset_n
			enable_in					: in std_logic;																	-- external LCD enable
			pause_lcd					: in std_logic;																	-- '1': lcd does not update data, '0': continous update of incoming data
			lcd_enable					: out std_logic;																	-- On/Off Switch, connect to lcd_csn, 1 is on, 0 is off
			lcd_rs						: out std_logic;																	-- Data/Setup Switch, connect to lcd_d_cn, 1 is data, 0 is setup
			lcd_rw						: out std_logic;																	-- Read/Write Switch, connect to lcd_wen, 1 is read, 0 is write
			busy							: out std_logic;																	-- Busy Feedback
			lcd_data						: out std_logic_vector (7 downto 0);										-- LCD Data
			ready_out					: out std_logic																	-- ready signal
		);
	end component;

	-- -- -- -- General -- -- -- --
	signal user_rst, user_rst_cnt																					: std_logic;
	signal mode																											: std_logic_vector(3 downto 0)						:= (others=>'0');	
	signal WAP0, WAP1, WAP2, WAP3, WAP4, WAP5, WAP6, WAP7													: std_logic_vector(pmaBitNo-1 downto 0)			:= WAP(pmaBitNo-1 downto 0);
	-- Counter
	signal cnt_delay_mode, cnt_delay_datak, cnt_powerUp													: integer 													:= 0;
	
	-- -- -- -- Transceiver -- -- -- -- 
	signal clk_gen, clk_check																						: std_logic_vector(chNo-1 downto 0);
	signal set_bitslip				 																				: std_logic_vector(chNo-1 downto 0)					:= (others=>'0');	
	signal stat_ready																									: std_logic_vector(chNo-1 downto 0);
	signal set_loopback																								: std_logic;
	
	
	-- -- -- -- DataChecker -- -- -- --
	-- Counter
	signal bet_err_no, bet_cek_no																					: unsigned(63 downto 0);
	signal bet_err_no0, bet_err_no1, bet_err_no2, bet_err_no3											: unsigned(63 downto 0);
	signal bet_err_no4, bet_err_no5, bet_err_no6, bet_err_no7											: unsigned(63 downto 0);
	signal bet_cek_no0, bet_cek_no1, bet_cek_no2, bet_cek_no3											: unsigned(63 downto 0);
	signal bet_cek_no4, bet_cek_no5, bet_cek_no6, bet_cek_no7											: unsigned(63 downto 0);
	signal bet_sync_no0,	bet_sync_no1,	bet_sync_no2,	bet_sync_no3									: unsigned(63 downto 0);
	signal bet_sync_no4,	bet_sync_no5,	bet_sync_no6,	bet_sync_no7									: unsigned(63 downto 0);
	signal bet_sync_no																								: unsigned(63 downto 0);
	
	-- Status
	signal stat_sync, stat_rec																						: std_logic_vector(7 downto 0);
	
	-- -- -- -- Data -- -- -- --
	-- ch0: pmaBitNo-1 downto 0
	-- chi: (i+1)*pmaBitNo-1 downto pmaBitNo*i);	
	signal data_tx, data_txP, data_rx 																			: std_logic_vector (chNo*pmaBitNo-1 downto 0)   := (others=>'0');	
	signal set_inj_err, set_datak																					: std_logic;
	signal deb_datak																									: std_logic_vector(chNo-1 downto 0) := (others=>'0');
	signal deb_setrst																									: std_logic_vector(chNo-1 downto 0) := (others=>'0');
	
	-- -- -- -- Display -- -- -- -- 
	signal lcd_line1, lcd_line2																					: std_logic_vector(63 downto 0);
	signal set_lcdmode																								: std_logic_vector(3 downto 0);
	signal lcd_paus																									: std_logic;
	signal cnt_lcd 																									: integer;
	
	-- -- -- -- Debug -- -- -- --
	signal deb_pattern1, deb_pattern2																			: std_logic_vector(pmaBitNo-1 downto 0);
	signal user_rst_deb_pattern																					: std_logic 												:= '0';
	signal cnt_ready0																									: unsigned(63 downto 0);
	
begin
	-- chose sued WAPs
	with set_loopback select
		WAP0	<= SEED0 when '0',
					WAP	when '1',
					WAP	when others;
	with set_loopback select
		WAP1	<= SEED1 when '0',
					WAP	when '1',
					WAP	when others;
	with set_loopback select
		WAP2	<= SEED2 when '0',
					WAP	when '1',
					WAP	when others;
	with set_loopback select
		WAP3	<= SEED3 when '0',
					WAP	when '1',
					WAP	when others;
	with set_loopback select
		WAP4	<= SEED4 when '0',
					WAP	when '1',
					WAP	when others;
	with set_loopback select
		WAP5	<= SEED5 when '0',
					WAP	when '1',
					WAP	when others;
	with set_loopback select
		WAP6	<= SEED6 when '0',
					WAP	when '1',
					WAP	when others;
	with set_loopback select
		WAP7	<= SEED7 when '0',
					WAP	when '1',
					WAP	when others;


	-- -- -- -- TRANSCIEVER -- -- -- -- 
	Transceiver : pma port map (		
		CLK100						=> refclk0_qr0_p,
		CLK50							=> clkin_50,
		CLK_TX						=> clk_gen,
		CLK_RX						=> clk_check,
		RST							=> user_rst,
		DATA_TX						=> data_txP,
		DATA_RX						=> data_rx,
		SERIAL_TX					=> hsma_out,
		SERIAL_RX					=> hsma_in,
		BITSLIP						=> set_bitslip,
		READY							=> stat_ready,
		LOOPBACK						=> set_loopback,
		deb_cnt_ready				=> cnt_ready0
	);
	
	-- -- -- -- DATA GENERATOR -- -- -- -- 
	dataGenerator0 : dataGenerator port map ( 
		CLK 						=> clk_gen(0),
		--RST						=> user_rst and deb_setrst(0),
		RST						=> user_rst,
		EN							=> '1',
		INJ_ERR					=> set_inj_err,
		MODE						=> mode,
		DATA_IN					=> data_tx(((0+1)*pmaBitNo)-1 downto pmaBitNo*0),
		DATA_OUT					=> data_tx(((0+1)*pmaBitNo)-1 downto pmaBitNo*0),
		SEED						=> WAP0,
		SET_DATAK				=> set_datak or (deb_datak(0) and en_autoDataK)
	);
	dataGenerator1 : dataGenerator port map ( 
		CLK 						=> clk_gen(1),
		--RST						=> user_rst and deb_setrst(1),
		RST						=> user_rst,
		EN							=> '1',
		INJ_ERR					=> set_inj_err,
		MODE						=> mode,
		DATA_IN					=> data_tx(((1+1)*pmaBitNo)-1 downto pmaBitNo*1),
		DATA_OUT					=> data_tx(((1+1)*pmaBitNo)-1 downto pmaBitNo*1),
		SEED						=> WAP1,
		SET_DATAK				=> set_datak or (deb_datak(1) and en_autoDataK)
	);
	dataGenerator2 : dataGenerator port map ( 
		CLK 						=> clk_gen(2),
		--RST						=> user_rst and deb_setrst(2),
		RST						=> user_rst,
		EN							=> '1',
		INJ_ERR					=> set_inj_err,
		MODE						=> mode,
		DATA_IN					=> data_tx(((2+1)*pmaBitNo)-1 downto pmaBitNo*2),
		DATA_OUT					=> data_tx(((2+1)*pmaBitNo)-1 downto pmaBitNo*2),
		SEED						=> WAP2,
		SET_DATAK				=> set_datak or (deb_datak(2) and en_autoDataK)
	);
	dataGenerator3 : dataGenerator port map ( 
		CLK 						=> clk_gen(3),
		--RST						=> user_rst and deb_setrst(3),
		RST						=> user_rst,
		EN							=> '1',
		INJ_ERR					=> set_inj_err,
		MODE						=> mode,
		DATA_IN					=> data_tx(((3+1)*pmaBitNo)-1 downto pmaBitNo*3),
		DATA_OUT					=> data_tx(((3+1)*pmaBitNo)-1 downto pmaBitNo*3),
		SEED						=> WAP3,
		SET_DATAK				=> set_datak or (deb_datak(3) and en_autoDataK)
	);
	dataGenerator4 : dataGenerator port map ( 
		CLK 						=> clk_gen(4),
		--RST						=> user_rst and deb_setrst(4),
		RST						=> user_rst,
		EN							=> '1',
		INJ_ERR					=> set_inj_err,
		MODE						=> mode,
		DATA_IN					=> data_tx(((4+1)*pmaBitNo)-1 downto pmaBitNo*4),
		DATA_OUT					=> data_tx(((4+1)*pmaBitNo)-1 downto pmaBitNo*4),
		SEED						=> WAP4,
		SET_DATAK				=> set_datak or (deb_datak(4) and en_autoDataK)
	);
	dataGenerator5 : dataGenerator port map ( 
		CLK 						=> clk_gen(5),
		--RST						=> user_rst and deb_setrst(5),
		RST						=> user_rst,
		EN							=> '1',
		INJ_ERR					=> set_inj_err,
		MODE						=> mode,
		DATA_IN					=> data_tx(((5+1)*pmaBitNo)-1 downto pmaBitNo*5),
		DATA_OUT					=> data_tx(((5+1)*pmaBitNo)-1 downto pmaBitNo*5),
		SEED						=> WAP5,
		SET_DATAK				=> set_datak or (deb_datak(5) and en_autoDataK)
	);
	dataGenerator6 : dataGenerator port map ( 
		CLK 						=> clk_gen(6),
		--RST						=> user_rst and deb_setrst(6),
		RST						=> user_rst,
		EN							=> '1',
		INJ_ERR					=> set_inj_err,
		MODE						=> mode,
		DATA_IN					=> data_tx(((6+1)*pmaBitNo)-1 downto pmaBitNo*6),
		DATA_OUT					=> data_tx(((6+1)*pmaBitNo)-1 downto pmaBitNo*6),
		SEED						=> WAP6,
		SET_DATAK				=> set_datak or (deb_datak(6) and en_autoDataK)
	);
	dataGenerator7 : dataGenerator port map ( 
		CLK 						=> clk_gen(7),
		--RST						=> user_rst and deb_setrst(7),
		RST						=> user_rst,
		EN							=> '1',
		INJ_ERR					=> set_inj_err,
		MODE						=> mode,
		DATA_IN					=> data_tx(((7+1)*pmaBitNo)-1 downto pmaBitNo*7),
		DATA_OUT					=> data_tx(((7+1)*pmaBitNo)-1 downto pmaBitNo*7),
		SEED						=> WAP7,
		SET_DATAK				=> set_datak or (deb_datak(7) and en_autoDataK)
	);
	-- -- -- -- Parity Controler -- -- -- --
	ParityControler0 : ParityControler port map ( 
		CLK 						=> clk_gen(0),
		RST						=> user_rst,
		EN							=> not (set_datak or (deb_datak(0) and en_autoDataK)) and en_ParityCheck,
		DATA_IN					=> data_tx(((0+1)*pmaBitNo)-1 downto pmaBitNo*0),
		DATA_OUT					=> data_txP(((0+1)*pmaBitNo)-1 downto pmaBitNo*0)
	);
	ParityControler1 : ParityControler port map ( 
		CLK 						=> clk_gen(1),
		RST						=> user_rst,
		EN							=> not (set_datak or (deb_datak(1) and en_autoDataK)) and en_ParityCheck,
		DATA_IN					=> data_tx(((1+1)*pmaBitNo)-1 downto pmaBitNo*1),
		DATA_OUT					=> data_txP(((1+1)*pmaBitNo)-1 downto pmaBitNo*1)
	);
	ParityControler2 : ParityControler port map ( 
		CLK 						=> clk_gen(2),
		RST						=> user_rst,
		EN							=> not (set_datak or (deb_datak(2) and en_autoDataK)) and en_ParityCheck,
		DATA_IN					=> data_tx(((2+1)*pmaBitNo)-1 downto pmaBitNo*2),
		DATA_OUT					=> data_txP(((2+1)*pmaBitNo)-1 downto pmaBitNo*2)
	);
	ParityControler3 : ParityControler port map ( 
		CLK 						=> clk_gen(3),
		RST						=> user_rst,
		EN							=> not (set_datak or (deb_datak(3) and en_autoDataK)) and en_ParityCheck,
		DATA_IN					=> data_tx(((3+1)*pmaBitNo)-1 downto pmaBitNo*3),
		DATA_OUT					=> data_txP(((3+1)*pmaBitNo)-1 downto pmaBitNo*3)
	);
	ParityControler4 : ParityControler port map ( 
		CLK 						=> clk_gen(4),
		RST						=> user_rst,
		EN							=> not (set_datak or (deb_datak(4) and en_autoDataK)) and en_ParityCheck,
		DATA_IN					=> data_tx(((4+1)*pmaBitNo)-1 downto pmaBitNo*4),
		DATA_OUT					=> data_txP(((4+1)*pmaBitNo)-1 downto pmaBitNo*4)
	);
	ParityControler5 : ParityControler port map ( 
		CLK 						=> clk_gen(5),
		RST						=> user_rst,
		EN							=> not (set_datak or (deb_datak(5) and en_autoDataK)) and en_ParityCheck,
		DATA_IN					=> data_tx(((5+1)*pmaBitNo)-1 downto pmaBitNo*5),
		DATA_OUT					=> data_txP(((5+1)*pmaBitNo)-1 downto pmaBitNo*5)
		
	);
	ParityControler6 : ParityControler port map ( 
		CLK 						=> clk_gen(6),
		RST						=> user_rst,
		EN							=> not (set_datak or (deb_datak(6) and en_autoDataK)) and en_ParityCheck,
		DATA_IN					=> data_tx(((6+1)*pmaBitNo)-1 downto pmaBitNo*6),
		DATA_OUT					=> data_txP(((6+1)*pmaBitNo)-1 downto pmaBitNo*6)
	);
	ParityControler7 : ParityControler port map ( 
		CLK 						=> clk_gen(7),
		RST						=> user_rst,
		EN							=> not (set_datak or (deb_datak(7) and en_autoDataK)) and en_ParityCheck,
		DATA_IN					=> data_tx(((7+1)*pmaBitNo)-1 downto pmaBitNo*7),
		DATA_OUT					=> data_txP(((7+1)*pmaBitNo)-1 downto pmaBitNo*7)
	);

	
	-- -- -- -- DATA CHECKER -- -- -- -- 
	dataChecker0 : dataChecker_v2 port map (
		CLK						=> clk_check(0),
		--RST						=> user_rst and stat_ready(0),
		RST						=> user_rst,
		RST_CNT					=> user_rst_cnt,
		DATA						=> data_rx(((0+1)*pmaBitNo)-1 downto pmaBitNo*0),
		MODE						=> mode,
		SEED						=> WAP0,
		ERROR_NO					=> bet_err_no0,
		TESTED_NO				=> bet_cek_no0,
		SYNC						=> stat_sync(0),
		REC						=> stat_rec(0),
		BITSLIP					=> set_bitslip(0),
		LOSTSYNC_NO				=> bet_sync_no0,
		DEB_setrst				=> deb_setrst(0),
		DEB_SETDATAK			=> deb_datak(0),
		deb_pattern1			=> deb_pattern1,
		deb_pattern2			=> deb_pattern2,
		deb_patternas			=> user_rst_deb_pattern
	);
	dataChecker1 : dataChecker_v2 port map (
		CLK						=> clk_check(1),
		RST						=> user_rst,
		RST_CNT					=> user_rst_cnt,
		DATA						=> data_rx(((1+1)*pmaBitNo)-1 downto pmaBitNo*1),
		SEED						=> WAP1,
		MODE						=> mode,
		ERROR_NO					=> bet_err_no1,
		TESTED_NO				=> bet_cek_no1,
		SYNC						=> stat_sync(1),
		REC						=> stat_rec(1),
		BITSLIP					=> set_bitslip(1),
		LOSTSYNC_NO				=> bet_sync_no1,
		DEB_SETDATAK			=> deb_datak(1),
		DEB_setrst				=> deb_setrst(1)
	);
	dataChecker2 : dataChecker_v2 port map (
		CLK						=> clk_check(2),
		RST						=> user_rst,
		RST_CNT					=> user_rst_cnt,
		DATA						=> data_rx(((2+1)*pmaBitNo)-1 downto pmaBitNo*2),
		SEED						=> WAP2,
		MODE						=> mode,
		ERROR_NO					=> bet_err_no2,
		TESTED_NO				=> bet_cek_no2,
		SYNC						=> stat_sync(2),
		REC						=> stat_rec(2),
		BITSLIP					=> set_bitslip(2),
		LOSTSYNC_NO				=> bet_sync_no2,
		DEB_SETDATAK			=> deb_datak(2),
		DEB_setrst				=> deb_setrst(2)
	);
	dataChecker3 : dataChecker_v2 port map (
		CLK						=> clk_check(3),
		RST						=> user_rst,
		RST_CNT					=> user_rst_cnt,
		DATA						=> data_rx(((3+1)*pmaBitNo)-1 downto pmaBitNo*3),
		SEED						=> WAP3,
		MODE						=> mode,
		ERROR_NO					=> bet_err_no3,
		TESTED_NO				=> bet_cek_no3,
		SYNC						=> stat_sync(3),
		REC						=> stat_rec(3),
		BITSLIP					=> set_bitslip(3),
		LOSTSYNC_NO				=> bet_sync_no3,
		DEB_SETDATAK			=> deb_datak(3),
		DEB_setrst				=> deb_setrst(3)
	);
	dataChecker4 : dataChecker_v2 port map (
		CLK						=> clk_check(4),
		RST						=> user_rst,
		RST_CNT					=> user_rst_cnt,
		DATA						=> data_rx(((4+1)*pmaBitNo)-1 downto pmaBitNo*4),
		SEED						=> WAP4,
		MODE						=> mode,
		ERROR_NO					=> bet_err_no4,
		TESTED_NO				=> bet_cek_no4,
		SYNC						=> stat_sync(4),
		REC						=> stat_rec(4),
		BITSLIP					=> set_bitslip(4),
		LOSTSYNC_NO				=> bet_sync_no4,
		DEB_SETDATAK			=> deb_datak(4),
		DEB_setrst				=> deb_setrst(4)
	);
	dataChecker5 : dataChecker_v2 port map (
		CLK						=> clk_check(5),
		RST						=> user_rst,
		RST_CNT					=> user_rst_cnt,
		DATA						=> data_rx(((5+1)*pmaBitNo)-1 downto pmaBitNo*5),
		SEED						=> WAP5,
		MODE						=> mode,
		ERROR_NO					=> bet_err_no5,
		TESTED_NO				=> bet_cek_no5,
		SYNC						=> stat_sync(5),
		REC						=> stat_rec(5),
		BITSLIP					=> set_bitslip(5),
		LOSTSYNC_NO				=> bet_sync_no5,
		DEB_SETDATAK			=> deb_datak(5),
		DEB_setrst				=> deb_setrst(5)
	);
	dataChecker6 : dataChecker_v2 port map (
		CLK						=> clk_check(6),
		RST						=> user_rst,
		RST_CNT					=> user_rst_cnt,
		DATA						=> data_rx(((6+1)*pmaBitNo)-1 downto pmaBitNo*6),
		SEED						=> WAP6,
		MODE						=> mode,
		ERROR_NO					=> bet_err_no6,
		TESTED_NO				=> bet_cek_no6,
		SYNC						=> stat_sync(6),
		REC						=> stat_rec(6),
		BITSLIP					=> set_bitslip(6),
		LOSTSYNC_NO				=> bet_sync_no6,
		DEB_SETDATAK			=> deb_datak(6),
		DEB_setrst				=> deb_setrst(6)
	);
	dataChecker7 : dataChecker_v2 port map (
		CLK						=> clk_check(7),
		RST						=> user_rst,
		RST_CNT					=> user_rst_cnt,
		DATA						=> data_rx(((7+1)*pmaBitNo)-1 downto pmaBitNo*7),
		SEED						=> WAP7,
		MODE						=> mode,
		ERROR_NO					=> bet_err_no7,
		TESTED_NO				=> bet_cek_no7,
		SYNC						=> stat_sync(7),
		REC						=> stat_rec(7),
		BITSLIP					=> set_bitslip(7),
		LOSTSYNC_NO				=> bet_sync_no7,
		DEB_SETDATAK			=> deb_datak(7),
		DEB_setrst				=> deb_setrst(7)
	);

	-- -- -- -- DISPLAY -- -- -- --
	lcd : controllerLine port map (
		clock => clkin_50,
		reset => not user_rst,
		enable_in => '1',			
		pause_lcd => lcd_paus,
		lcd_data(7 downto 0) => lcd_data(7 downto 0),				-- internal needed, not so nice :-( TODO
		lcd_enable => lcd_csn,
		lcd_rs => lcd_d_cn,
		lcd_rw => lcd_wen,
		--line1 => std_logic_vector(cnt_locktodata) & std_logic_vector(errorNo(64-16-1 downto 0)),
		line1 => lcd_line1,
		line2 => lcd_line2
	);
	-- sum up all channels
	bet_err_no <= bet_err_no0 + bet_err_no1 + bet_err_no2 + bet_err_no3 + bet_err_no4 + bet_err_no5 + bet_err_no6 + bet_err_no7;
	bet_cek_no <= bet_cek_no0 + bet_cek_no1 + bet_cek_no2 + bet_cek_no3 + bet_cek_no4 + bet_cek_no5 + bet_cek_no6 + bet_cek_no7;
	bet_sync_no <= bet_sync_no0 + bet_sync_no1 + bet_sync_no2 + bet_sync_no3 + bet_sync_no4 + bet_sync_no5 + bet_sync_no6 + bet_sync_no7;
	
	with set_lcdmode select
		lcd_line1 <= std_logic_vector(bet_sync_no(11 downto 0))  & std_logic_vector(bet_err_no(63-12 downto 0)) 								when "0000",
						 std_logic_vector(bet_sync_no0(11 downto 0)) & std_logic_vector(bet_err_no0(63-12 downto 0)) 							when "1000",
						 std_logic_vector(bet_sync_no1(11 downto 0)) & std_logic_vector(bet_err_no1(63-12 downto 0)) 							when "1001",
						 std_logic_vector(bet_sync_no2(11 downto 0)) & std_logic_vector(bet_err_no2(63-12 downto 0)) 							when "1010",
						 std_logic_vector(bet_sync_no3(11 downto 0)) & std_logic_vector(bet_err_no3(63-12 downto 0)) 							when "1011",
						 std_logic_vector(bet_sync_no4(11 downto 0)) & std_logic_vector(bet_err_no4(63-12 downto 0)) 							when "1100",
						 std_logic_vector(bet_sync_no5(11 downto 0)) & std_logic_vector(bet_err_no5(63-12 downto 0)) 							when "1101",
						 std_logic_vector(bet_sync_no6(11 downto 0)) & std_logic_vector(bet_err_no6(63-12 downto 0)) 							when "1110",
						 std_logic_vector(bet_sync_no7(11 downto 0)) & std_logic_vector(bet_err_no7(63-12 downto 0)) 							when "1111",
						 std_logic_vector(deb_pattern1(63 downto 0)) 																							when "0001",
						 std_logic_vector(data_tx(63 downto 0)) 																									when "0010",
						 std_logic_vector(bet_sync_no(11 downto 0)) & std_logic_vector(bet_err_no(63-12 downto 0))								when others;
	with set_lcdmode select				
		lcd_line2 <=  std_logic_vector(cnt_ready0(11 downto 0)) & std_logic_vector(bet_cek_no(63-12 downto 0)) 								when "0000",
						 std_logic_vector(cnt_ready0(11 downto 0))  & std_logic_vector(bet_cek_no0(63-12 downto 0)) 								when "1000",
						 std_logic_vector(cnt_ready0(11 downto 0))  & std_logic_vector(bet_cek_no1(63-12 downto 0)) 								when "1001",
						 std_logic_vector(cnt_ready0(11 downto 0))  & std_logic_vector(bet_cek_no2(63-12 downto 0)) 								when "1010",
						 std_logic_vector(cnt_ready0(11 downto 0))  & std_logic_vector(bet_cek_no3(63-12 downto 0)) 								when "1011",
						 std_logic_vector(cnt_ready0(11 downto 0))  & std_logic_vector(bet_cek_no4(63-12 downto 0)) 								when "1100",
						 std_logic_vector(cnt_ready0(11 downto 0))  & std_logic_vector(bet_cek_no5(63-12 downto 0)) 								when "1101",
						 std_logic_vector(cnt_ready0(11 downto 0))  & std_logic_vector(bet_cek_no6(63-12 downto 0)) 								when "1110",
						 std_logic_vector(cnt_ready0(11 downto 0))  & std_logic_vector(bet_cek_no7(63-12 downto 0)) 								when "1111",
						 std_logic_vector(deb_pattern2(63 downto 0)) 																							when "0001",
						 std_logic_vector(data_rx(63 downto 0)) 																									when "0010",
						 std_logic_vector(cnt_ready0(11 downto 0)) & std_logic_vector(bet_cek_no(63-12 downto 0))									when others;

	-- -- -- -- User interface -- -- -- --
	-- LEDs
	user_led_g(7 downto 0)			<= not stat_sync;
	user_led_r(7 downto 0)			<= not stat_rec;
	
	-- Dip-Switch
	set_loopback						<= user_dipsw(7);
	debouncer : process(clkin_50) begin
		if(rising_edge(clkin_50)) then
			-- set mode
			if(cnt_delay_mode > 50000000/10) then
				if(mode(1 downto 0) /= user_dipsw(6 downto 5)) then
					mode <= "00" & user_dipsw(6 downto 5);
					cnt_delay_mode <= 0;
				end if;
			else
				cnt_delay_mode <= cnt_delay_mode + 1;
			end if;
			--mode <= "0010";
			
			-- set datak
			if(cnt_delay_datak > 50000000/10) then
				if(set_datak /= user_dipsw(0)) then
					set_datak <= user_dipsw(0);
					cnt_delay_datak <= 0;
				end if;
			else
				cnt_delay_datak <= cnt_delay_datak + 1;
			end if;
		end if;
	end process debouncer;
	lcd_paus								<= '0';
	set_lcdmode							<= user_dipsw(4 downto 1);
	
	
	-- Press Buttons
	powerUp : process(clkin_50) begin
		if(rising_edge(clkin_50)) then
			if(cnt_powerUp > 50000000) then
				user_rst		<= user_pb(0);
			else
				user_rst		<= '0';
				cnt_powerUp <= cnt_powerUp + 1;
			end if;
		end if;
	end process powerUp;
	user_rst_cnt						<= not user_pb(1);
	user_rst_deb_pattern				<= user_pb(2);
	set_inj_err							<= not user_pb(2);

	
	-- Settings to enable optical links on HSMA and HSMB.
	-- HSMA
	hsma_clk_in_p1 					<= '0';
	hsma_clk_in_n1 					<= '0';
	hsma_clk_in_p2 					<= '0';
	hsma_clk_in_n2 					<= '0';
	hsma_clk_out_p1 					<= '0';
	hsma_clk_out_n1 					<= '0';
	hsma_clk_out_p2 					<= '0';
	hsma_clk_out_n2 					<= '0';
	hsma_d								<= (others=>'0');
	hsma_rx_d_p							<= (others=>'0');
	hsma_rx_d_n							<= (others=>'0');
	hsma_tx_d_p							<= (others=>'0');
	hsma_tx_d_n							<= (others=>'0');
	
	-- HSMB
	hsmb_clk_in_p2 					<= '0';
	hsmb_clk_in_n2 					<= '0';
	hsmb_a								<= (others=>'0');
	hsmb_ba								<= (others=>'0');
	hsmb_casn 							<= '0';
	hsmb_rasn 							<= '0';		  								  
	hsmb_cke 							<= '0';
	hsmb_csn 							<= '0';
	hsmb_dm 								<= (others=>'0');
	hsmb_dqs_p							<= (others=>'0');
	hsmb_dqs_n							<= (others=>'0');
end architecture RTL;
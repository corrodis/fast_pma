// Copyright (C) 1991-2013 Altera Corporation
// This simulation model contains highly confidential and
// proprietary information of Altera and is being provided
// in accordance with and subject to the protections of the
// applicable Altera Program License Subscription Agreement
// which governs its use and disclosure. Your use of Altera
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Altera Program License Subscription
// Agreement, Altera MegaCore Function License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs for
// use exclusively in logic devices manufactured by Altera and sold
// by Altera or its authorized distributors. Please refer to the
// applicable agreement for further details. Altera products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Altera assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 13.0sp1
// ALTERA_TIMESTAMP:Thu Jun 13 12:28:40 PDT 2013
// encrypted_file_type : mentor_tagged
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
OvIITQOkLeFlWRtyFzykJbDf7FX/9U/HlKJKa1siB1jtvBoLWEwPhPDcgHCrm76a
I6ZrkdAwyoRLANngUgmJefR6yeFrh/nQbgETZfUPN6EVGCzPyYaJ5O0Gc3YPBSw2
61zpp7bnRHNalRZyyGFMmqLQvAGTPftKp+AQozXNOfE=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 2384)
ynG46C9/4HbYtjDqxcvDS0JSNWk7AEtqoAxX/KFYPBPrbWc/+48/sZ2u/vRjDTF/
cwt8nRX0dcNlgz3yvyBIWUTrd5Mt8WmqmUiNLtsU+4VdeIz2RG+zs1izzlZ2BO6/
XeJ6OnOlTV9ZYKsQapwuMwdC1eHzF9ehrGU5kZ0J46RTEWoBwmKRnpshIZzZMSmz
oc2YCdyyX06En0lap071Q1aWwHEKKPb/Rn/vgrdGBW0bhgcbkgBNIc0j/vABQXaP
qy2PqdQMSaMAKJSrqeVvbp693tzyxlSykf6Z8GnIfWxETeqlHd8RUkSFR1tcXNFV
OFA7PJflIL2ENCrFtHHVPoLWQUCHbDwdzb38ppf0KVGZl2+puOsFFAHVfoKU/JPO
SFbweMKelKReALb7i/foDNeFrVmov7RGJ4E+KTaCjYbzACT+g4useqX3d65v1KIU
GnH1R2jVIXp0PlQ3eJm3Ejhvi+5IyGFgmlXDSrHe30jECf+Fy2oqEGo2ZfOK5mbW
JehA8vPM5YZCIVvErUMxS0WGmBsaGrEKachaLOcURNRwuGVX5jHzsB5x8cy5Z/8n
LbA8MXeUTgFRYVFmqVx5A8uPo9sE+t39vlJ6p1+mBbCbDm50fMW2bzsRfv5xBxfp
1ODfKxw5BD2F/niAxc8whVc+QzJZcq6uRDMzqcnSBWmxa/64OTAo9DF0QuGlCtAz
RfpMBCtX70mFJQxUFqyPlJWYDVox6L9bBAWWcKl2jUrogWoiAZbGng3gQ/etRocd
uRCQXGD67faIxNmOkdHL2AcTMlaK+E8Pwi80lSrYqJOGAqJFEqD7ck5eZqEoOYse
bYTmMa8OFIRB2Pv4ZcXU2D2y74VOeCOBHvFT8cYthaE/e/lyH17ZRaF3+/CKBHiK
Cm9F53ZD6nq34s4kULRi0md2xgaT2WFB+VD54hwLGO4fSiqp0zx+WGHr5Yyr+MDg
lLz/gQcFi87P8I1eyVGfMJkkBxMn9jT+ZzQJ6IwMhXHeGjSmIDUryohWCbVAafWO
Kcmur8XkoAk8wWI2GijolGH/uuDW55B+ywGnCYVkyA6q/XiQ3z88NxpdYx3GuyjV
hhE6BvOIGg3kO+vx5l2uet5hyhgy+/Q8t4ziVY02Wn+0PYGa7mJ6CIFKysBnYm80
L+PSBQWvZKOM4GG144LphRmRkW8P0fX/I3nAliokhK9KJQDSflAVKgpujgDFlQOE
W2HwZCEP8dXVx0pyD8j20u/nFdPp0QbHqy24HC932cJ8U7I2/58jxY+pC5dEvHzx
p6rVYnTxJn0PQavfFGyWJGtRuXIJ9GDQCmezopFUnPYxvqHa2lnpcvVG4ANjnoBb
27ETUcKzYgiWXGjL3rmAwUr/00fI/nR4TUq3wdsz1Fa7shVXZ/4YyB4qgHRxWpL/
u1ofhLFal/q3rMUJBFj7OvJPLo/IZQXvMN7GO8rz2MPoDFUpHqx980fgccUUox7f
oL7PcyfTdc05l2zSFkoPwOBGJo329DKBEGhbx8AI6qUgkYo1JpxmN3+GAxjDu1IS
pCeR9jf5q9Je4oUt4kspuZxF9EbQRpC5s6F4XKc8XnqoCYwsg9Kuw4WxTgylldGW
n1bp2r6ZVCfVY1Tn/nJbeRB/SvlZJkhdRv26JxHcmYdHY/rIDcnuJ3OGZFDVh8iR
xhYBkcYli4DA6nxnlkfb7yVOq5/ReDn2ZJQUHMt0y9EC2tGFLmr+0XfMfc8LCklR
6iE1DjlEoPTPloFEP+peQL78R+3TpyR63OHrGn802eT5uRKObb7DGWKq5WUuwDsK
YbjUZc7ONr85rbqD1cORmCJauSM+lvtYz/0KyAY0sCGx5AgBwTyX8BbuFc+gh1E5
Cx7+hf+Uz19GSjS6ldIE+TlKTB8SaxgzpXhhGvcntpn0YMC9igBbuZITMreQJoum
mZvw+G4LseKpa7ykOBratb2MLWgA3eNEz0HATdlwY+6OmfuwtOX1QLtKbMOK2E80
YOXOHjwy4Nf1tXqUAQp0fMpw0Pz0d1Tp+2XhcmSKPw1e7cqcLt4pHlr+clxK8hWC
0mgsKQtzlVnPEIUXJEiozWwEtQlQGAB2HhC4UZA8F1AJsPkLwiCFVhhsPn5TwEy2
zZNklNWYzYKCECNcicjaEeBAjGBgsMBAU0pS6FD1LMS6poDlrFYuOcLrIU6XinTv
VbLiqCE2vjJBLwALayMLo1d960iMv9h87bnUTDWWc9kqVtcNhnpoSK/2RLv97Wsm
QUHVeOnQVIAisWXwSXtPENdZeKEz/cD7PyvCMHK9tjaNI090RWcqDUoIwagAkJzm
Oo2iZuVXza4yn4FH3brppObf3VWkzScvcPOrV7XKZdkT7TEcY3ulUOXLaOxpVqxM
kUpgY9SYDF3zclXVtjBrPbRixTXmvdmZ77aySa2zIoTHicm1IjCmCgEMWtO4jEdm
dEz4jUrCNVcgSrhyBvO89bnGHKEVMXX2vp/qXknPnQsFNSWpTsVT0eSkSy1ciKQB
wVA6vNxY0K5Tzd1OknixniHNDvtEaRpWiQ2TiBKhYwlJSM295YUeRqCpqNb2pkUu
xi/ZfzmWY9hru9DW6e4KZWGAmN0Hal/nqGLjjyo3Sipe6CW+u3157kev221gCHUC
D5W3BcmostCIlsIpzHQ4Nv2UG2qtAnhYJN0IvoKWskldGWcd17k/YGjlkasZ9+Iq
QKjbsnfpxEBGYkv/saZtTc7VmAe16/hIrs7G8Ce2hItp+zNW9PUb9FrEfEkiS2QM
Za4jo4flAnfypFdy0XAa5+FU8/w1YEtzWyJKXvPV2wVtov8sXTk42ulurauC8WLk
aR1a3r+AuugRx1V1d/S1wRyQtQ3nLgSK6s9A3dvvdI65f+CihCL/SMBUdqrtQ7nl
iG30NkFLI5P+94a9G6LL15TL6tMDapci+2Vakit6OD5eLd7W+51KCCt+K3A+/nyA
9o1mGdAa2e9CVg4CdQ3Wsp0uWN5ekIu+aPN5O8eE4Q3kVGmnHarawCAwg8za6/HK
WyYEjPLBquXKU35wN9i5GGzAu/Ky2LmYHVmPz8QPLnzBhu6E8Vm5QkwEeg2JUv1A
9MmnJJ4nRHshJIFbXg4hV/z4YMeCyCfTNPGw/M6910NvJmsNvyzwt4yMwRJtLZdw
hDWhokaJLGXU1WyfUt0tBNZPNRCiX2YY65Y2n3sZ5Lc=
`pragma protect end_protected

-- Gear Box
-- currently maximum PMA width of 80 bits

library IEEE;
package settings is
	use IEEE.std_logic_1164.all;
	constant WidthData 		:integer := 64;
	constant WidthPMA 		:integer := 64;
end settings;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use WORK.settings.all;

entity GearBox is
	Port ( CLK_IN 		: in	std_logic;
			 RST			: in  std_logic;											-- active low, sync to clk_in
			 CLK_OUT		: in  std_logic;
			 DATA_IN		: in	std_logic_vector(WidthData+2 downto 0);	-- sync to clk_in
			 DATA_OUT	: out std_logic_vector(WidthPMA -1 downto 0);	-- sync to clk_out
			 OVERFLOW	: out std_logic;
			 EMPTY		: out std_logic
	);
end GearBox;

architecture RTL of GearBox is

	component fifo is
		Port (
			data		: in  std_logic_vector(79 downto 0);
			rdclk		: in  std_logic;
			rdreq		: in  std_logic;
			wrclk		: in  std_logic;
			wrreq		: in  std_logic;
			q			: out std_logic_vector(79 downto 0);
			rdempty	: out std_logic;
			wrfull	: out std_logic 
		);
	end component fifo;

begin

	fifoInst : fifo port map (
		data(WidthData+2 downto ) =>	DATA_IN,
		rdclk							  =>	CLK_OUT,
		rdreq 						  =>	'1',				-- always output data in fifo
		wrclk 						  =>	CLK_IN,
		wrreq 						  =>	'1',				-- always write data into fifo
		q(WidthPMA -1 downto 0)	  =>	DATA_OUT,
		rdempty						  => EMPTY,
		wrfull						  => OVERFLOW);

end architecture RTL;
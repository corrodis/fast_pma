// Copyright (C) 1991-2013 Altera Corporation
// This simulation model contains highly confidential and
// proprietary information of Altera and is being provided
// in accordance with and subject to the protections of the
// applicable Altera Program License Subscription Agreement
// which governs its use and disclosure. Your use of Altera
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Altera Program License Subscription
// Agreement, Altera MegaCore Function License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs for
// use exclusively in logic devices manufactured by Altera and sold
// by Altera or its authorized distributors. Please refer to the
// applicable agreement for further details. Altera products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Altera assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 13.0sp1
// ALTERA_TIMESTAMP:Thu Jun 13 12:36:50 PDT 2013
// encrypted_file_type : mentor_tagged
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
LlBzlWFGnSa4fOs5Bz/PFVqieVAYQUAu+9Mbdfz5RtC69gSpsXSfieLW93t1SN6d
4La2DqZzSlisHHQH4NcPQMCCUUpSrqhy9UTuoWWVV+XQJNmPq5m8wqY4QXj/hrM6
lOeCO2hSj1v+8WQtfVqlylGTpSIFaXT9dCb2Pw58f7s=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 5392)
KIhU5+SCSHFn2SLx/2Mbt78Wx9WfCruHUHHSVdLIE+dc7QTXfYDL5rw62O7eH1sk
VX0LRlVgWrdSJAWGzN+9c2giVpcjgBEDFK1FCCGkH6TmgXzFecdVffKIdT2XgXNj
fp1sc/GwZJltavlLwBaKwz5m6jzX0y5eLc75tDdC6dAd6as4kVb7O+CPHTZ5j5HI
URHM4+QJOL5XaBI2ZHijPb69OSsgC7EzbBryQ9DF6XvMhxqiqtqoXbQuiUfNHE0p
jGgiYM9aU0MmKlYNBeawhhU+DNJq105ZoHWhcB7RXdbSWV9HFCMD6cW0r5NfYixW
gN5UKgyq3/GvVcWRyVbMciZjikch8COachgTqiegVYlmJ+td96uxkfjB52c4MivF
kyE0TD9zYtVzFOn2YcDKrN8pj8cZnDmnljDPjYLZ6RJTkhwdFUCxij6ZajooJmKK
clx59lO2o6EqmUPjqZ8X1n51aldfe8CY7lZuYxSKRczIkS50qMMOmWXWGa3QRA1A
vnFkNBs5tLDYDOO6Xu3X/9Bcr90L+g8dUQjwJNDj1ZkzQs9RsdWHFprEJ6c4AAOI
fX1P9X34+4qAiAEALpCHwzh9PjBL8ZnohIAYVbSvXRSYl74fRvG5a35NIEJZd9wC
a5LK1CC9/OPYNC1on778DQI/dBCu1S4Xr2jwWY6d6oR3AQ//CdJmJckMcX+gXNUI
h1exFOoLeLw/U4jWLhqNpXCbsqTMkcOKAzVb+Qtc27jugVxnlrOrYlKxgcbJ7D1p
MgFWhbl9boaxTZ8Nm3H+95+pSEV3+SX7uOmSRPh/Ar7X4TMm1YSW/49oPWQHqJDY
wvKKLFnOPBh060XmkhoovD55XnXwBrfdOMj76Ddni1F7DBKxnwQ/cGQRNla94EsI
BGFMA02jt1TFfkE+b/a3+V3hPmtgb9FpTw2RqAhlSWZ127047O455El3GaPi06q7
KgFR1R7jmXCj57k7kK+mB2AFPo/0Oa/4hTsLoiaQGCrJ27PqKnTZF5025l4wewy4
AUkePwDd5Q3l77P1ehXRytnEkbSh7wex5ULMsJMxC5paDrK5UU8OhTd4+XcoUH7i
0pX9X+6qOfXpFWfNj3hhzPtuG+ytibP9xCtHR9Cb04bOP5l8K5HDtnajOGuYqPVl
qW/O4zLA9Qgqcjgu2byjzlYsuCeuYlUVXipsfNQCcrhbFW7d+sjoMrs7QatyucER
KjsFMbw5dmHVYIqMkZeUfM8neZOtYFlC0FGcs65iDBMbm4XaJDd7EJmpxbJVbs+8
A7KhWYCwCe8ZxbryeaHtVrgD8ew9oV25nuL6LQfa7HpvA6tOh+Yo+PdJJOHen4I6
n3/vL8I/hGUDyl8puDgen+JKBxeMvA8kDFWpURWNI4gKFeNyFRmKeCgdu9Qw3V96
a1YhSFoxNnpT5w5GJPBTKqb7T64LunD9S+Axnd+O848BmLm+bwSBkwBqZ3yV/XyN
+3qVXwwL+IG44DVaQCfHGmLDPuv1zsbe64w2KMXvpcF4ydnnH8LL7bPYyDQ/bdQB
8uq+mjQz7YZ2HOmUQ2oB4TVvWwkGiJ7q4yCcsBHH8HfUZjQl3QNSMLwYoIUZbluT
Z9hmbhsXBG5rbi7ZERVYq79IO/gUbTGlL03rKYNSzZgnlh+MtGZJNg7dIU9m+lSa
LKKRz91fTxrwW75Hec+2naEjepr4Ia49HeHYXnJybKjikkh8s3GpfLzfe6ir0L67
vIWoNfE7aiOKhdKsTsMiYmkg8Xtvk6JTJeOiPb0nn8vPId9mlFSBPsAMmBjkiVgY
4FsG6RngGwrfmK77dqHVZcZJvvwgWF55KfPulhNme07m4JP/keY9S2OX76cHoA59
gEXHkZOwv9fpKTR5EEg4bPyRpSjYZN2F3Esxsym7rvY4mzL8bKHc8h57o1Nvxv04
Pdd/1vIpQLgStv1e2ayttrv+dCjUCV8PiSEdgRQuKKAz+WfXjHvccqnCuag/UBT/
VNQK/EiXXmcxbnHTcMGEX/gAbbJDQw/eKiFcE6UmiBLSK+XsQ9J1xQtcHIb9aj3e
fEuaMfbOE2SKWuedMhpmU2e8zyDKtZHeZjX72oXVveljbT8eSxgUjMzncq7+EvTm
Yvz+HKmG+QBFbp7rhc+aCwavlF2Bz0VeCbey64425tZ3kFW3xdB3pGGMhnTD3k5n
pCrcRaWyIrlSWep2Avbej0INj1Wk4Vst2W1ZzXskiQ/FJcu+SQwOGAYXZjL8GB0G
wAG05DQ/tCvx8Y3IMH778XB0Jrqinr15wnCwpk1gCs5iTD99e053y/Dbf1gSZo/+
M1R4o3won/1dgOB/IASYe+t8I/8gjln5qErHDQ/naMSYT6LfbgVjmU4gubdFqZAF
DKZDIOT3+oBNSjA5Ih/wVHSZUWhvebATztSVdBKxCDAsa7xBKMFhTNDqXz65CbwX
+RL4fOZ4rcgGKNiZQdkZdhlx1+Y+Ar88h4PQNuR91dPA6ZGBVSZlgykXQxBA30Lp
PPZ00EfOMgfd5wy+jZGRKu9GJVkdze0ICmRxkPVv1ryK7j/yQaom6BmbbRDY8Qrd
lRky54frJe/CPEkFGiuDXxqvIukuzLta7d9nXzjMhpW32IpNDZdJM9utfsZ3paL5
C1DwKbLPhTDv/boteJlwkYoc3oiJZoL5gGfG59ZUBDo5Qsmf5imKNNXnTFdAs1rC
I1yQCzS4fLSfii8mYgr8tP6SCQuYIN68h2wJdepG/k2EIWkguYPc3RAHyZlmeCYN
lz6y85zvyXFxBsaZLi3ETzeKxQjqXhXach+qpcmwKXOzX2+UmJM54gmcF21bmFRw
Qtx7gy7yRygbtfS+ufYQ11gRuln3Uyi/JhhgQErXJt+IcR1KxsT5Dci757UUQ0ut
8/qzNnVVPytCCvVyEwv4DuQ/nW3QQ85sUgdTjhUHK2WGzsNxvLRn34EylQJpa0Z0
2flsQmAYK50ibtBLU+QYRMcdTREF1UUExMiBZ3HNB8bDkUs6Ttp9uiMUFeRnWOHc
GLULF2m/W4C8ResO/+YaIdqDICJOa6E6/pG4zPQG4QAUKOh63WxTOoWaMJy+1E90
mwBc6lKfeIr6SAOwuRmQbKr/4uaQ2gJqt8qSNZPWRP0VFJ5UhBbXAh/+S+rKpYpr
LJ0HwrF1YBV4Dc09Ie7ex+MejCbjXgR7KC5hOue3eNGKC1Pjqrtb6Sihl5aJ/46H
q0tlLeffibCMsxSZ0OtVjspx30021DXITybYI7GsoaypC0Q8rwzra5LIPYZXJtJD
pozzSiempjYagDKkcdSqSYySPvcDiHY1I+A21re58JOsQcqVLd/DME5wLQgnsHo0
CVWaTCfKVzyLxgtiOhb+sb5j48KKRsZfCRPkYhpcz9jrsiHeiID6duv5HmVU2H88
Pq+ZWlZGHHqsHwJCAcGnGQ5BNnGTFoJHIz7olcethLmSsnMggkz95wQCY0SUe766
8Nm1ENQ8CNTdOChZ+XIpAQ0jvBqR3dGn7iGcpZyRQVBYBIEIAZH68jhNaLg1J6/4
p0kqPgQMj/xeHXbHWdoWD8nDp7xNfG7hWgA6OMImJHBMkvRmbq67q8pTyBedDVq/
K81WrGtn6MF4vceyZru+7abh6cscr6n8FklUEWAl7xNie8IIsF80bzJSUabgP2gQ
6gdMf8iqkZ+PJZ6Vx5P+MTR+AnVDNV2qdZEMCE8UUpmBumQzCox0q7lHMGf++Ebx
6MpbEpn2op2YXlOPqLGluQdmUoFkG3YMpZR8X3WgMbALxrGiKoWb9zbLFQk4iaE9
jEs7t9XjyPeOUO3xA/mVNrcvuGVubMSfvRoUqa/ceBXvn8YZsErgzL6+Nr5SdxHo
MODWkTbGBjZTbcuhEsIOb5GXVuLK4WChE/t5Eb64F0+MGcoIOevdS+MP7Jd7NOxY
N1e6CYkz4dh5xmwd8LAxLfVMF4NNGKCp3SjnRCTs/H8igQoN2xfTKeXdtTV6R3wL
/GYq0iV5DD10kRd3ep0JByD16EbLFKzs/e7kxUDI8Ag9mUsF/cP7Bam+LTKIrP2s
hHOaIPKzCjy/Ax0udpmRpbp1nFGYgdIrzOvIPeivjGeYjRPnbrjQ+e1B2CySz9rX
9vQXMndZfwTGYy6Xhaui1XQKoc9TV6XE+qt9a+4eB49xR5wy6//Zm1Gyf3ejMe3h
ZMDihncJbQDKN432rrlqwtlOERASUlHQBBXDjI6cBKmB5gUeHwpM5xxulrndA1Pp
Jz2QVIHr0YQIB8JYopT6ucLDw+FrJKRL7U6K8fX+TRysB1w/3OfDJhm/IznEW5oH
evF+VLZnPF9N9FAoXFRLgixwnPnL8DPfStWQNRKvmXUyDsewhMr5peSsL67cDYq1
hZFheVD2Nrcsx7QUkZEFvh48mMkyVdbFRgtMCkTg8B+nputeCf2MENbAFL2fc331
X57coLaNLO4+PAq/WP8quyx+GjijEVawgiwSd1WScLbmHYiYfAqW9gbk0QiIJjNM
UKHW/+yNC5RrzrGsUKGOie6oOsyiLEZDKiI1I6otAq+7K2+mLRDprwvnMxrhRADg
vji5nLkS/9oxPoAJCL+NRht06FOUKXHgs7qyXb7C5hiIm54tOCyIrFEPBCgSC0gG
L6si140VKBIyEpDtuVF6cwQufwWBHSzRfgZ5Olf2pUUffRmfg1P4vEz6+wizLh3b
ENToV6LKgvp/+h8meCXezFJgAp+xbZ0GLxtlym1VESaUzBAg3eI+Lt9OXfDwwxa8
H22CKjfIpiMSr+nJPNgs9Vst+R9R9kL/ULXDkoXoDn7ZtGNS/YPyvxlnsEX2dfZc
92G8hZ42GhFIbcj0mAtrPngyVd9Wyg+8xxHgSHtrKvEjHyLYGGt/jsYmHDwZ1Mz5
pk6ISa6uenclcI6MfJ1Ng+X/+btK2Np/gJQ3nOb1OsaE3GhCzM7kgoFIEPMIDXYY
3j8+nC/DxbL+uELKM74ClpeiW8jFpMdeg33p9jM4vbHO1GvmwLEdzb69vfBYHoQu
YY1YL6N8Tdp6fgGJZpGpniD7SlMJ/SfkrJO9OuGmmyLMQrZIX6hFxDigfV2G9l97
kQsaQHj+d+rL02K4MlELRk27i+WEPTfV+Pj2Ps3gUH3VEmjcUC9vBMxeuihBHM6r
XWl1CCqVFkKQFesXc0P1R9etPy5N5eTVMzB0dNgqII9fYmg0lQrIf7x8Yvzav0qD
mCKfh1hjaAUXudUgJ2CHHgxLYH3L/L+yhktYF9xKyA/x7x3eYk3LA22poxy8tKD2
ZZvIXJATvT64CibnLV/hRZlskxMPd8Ih7GuAt39yrtdaHS0Q4AiZUkt3Xa49yn/1
vlm58+3OT5cePZz/c2M9+0BBa6k9E7Qswd10IVihA0aVacTDMvyKsaONZwa6Zu0E
G3hLsQLIP+4eg5pmzJxPBqdZFpLFpIn9OMb1rvWstiIgAhrhB7pn/JgB7n13HgCy
V74xfITwrW2MKdTT4Zblj76cSdDSEoHihitmIJQTVDt40aTXoZh9MSBtxoBPRHbj
QfZvH6ZYi3p5n0u2W725HWczlktYRMwAxbdcA/17ke1QpU+Hyoo3bP87RgBLx6pC
Mxba1Xbq7EaFVExXzltbZw65H+tR+0vppvJ0I6PpwCbL49fB5KVDZwxfDEvCjo+V
Kua9LgxyQzcOOO3JUAEqZ6KsbjdODSZ934AHLU45UNTgOuDS2JSea+1fcO0T5oGm
fdnmQ2hPBhmNiL+COvk4qezHcp2Bg9sar0bgn/YcBSWcGvNXPXRnqZ69TRdtrCXx
bQ40yyxbVhpXSVU2Mcn7EeC9c6zvayohFYONkINCrgKAPvRGZ6U406OIrc44XUUU
h8T04AVy6ffaBuPzLnnyH+CAz/yv0ckdHuFHNi29I7XY/g7loAz5u+1ehgPrERTq
1YucT50reR0VDcPvLqzrPLkdqBGttd17HocbGBYb7DuPpI96CsdscBJSOVrRnpFb
HtRDXkJSw8z/0LrqsDMoK+UyxM1MYYjU1gfRiClcL9hlVutN6s8ffpOwU+q8Z/HP
gyeWFOJQ0soJpANH6lSKt//F6OVygqYsxfOQZby9QOZbwLig+YlMB3Y5NzG8zf6a
ss0g6ESHm1Kis6zL9IspKSLYb2TNsCL9fEj4ZsSY51awcylYw5Rsv2S78WlotYxx
C7WuS6zBff966WVRsHaI+OWYBqYMP9TpWDD9p3Nd5b/zQ1d5YCMHEKD1AJrbr37j
Vuif1VA622z+4u4tVRiMEM/LzMw6SkL2Q4nbzwrY3h7sN2SgVg4TOF+Whdap2NID
Y5xDONpvvonm3udYfUm8Knrcdp3q1CMDtchOsJG6jWEkkKPSIBBur6T8ljThlJkF
y1kPvuovb5OUKTQ5KoCtZMFVlpzYOXqKF+10FIPJIlT5dNC9UjKLgKWsYFcIpRVO
qPc13oD4TBzaSAUZh9aQHMq5SoJioUOhoHX5m9924PHiHAHNuqHYndiIXPYrV5qy
rczR6PLw5CBzDAQAyaidq6oTBGQT6OrpX3bVqcOkA45lv1NFXeuNcV6SG3EI2Mqe
ntvo+WdWukSoDzHAob3Occ96P8KWsOcAMcx0s2ZH+I5fOdERujbUZLFfE+7T8v3A
l2Q4m0q+enQmRgESaUnzA1gTv/ZS7adGdJxWKNADLPtzOGsAyIWIlqObFU6626+I
RmWn0zmHMn7nhULnaP4+t7sBflSc9xYT/Smz8q1kZz0gsKbrE0pS4jTjcPxevMn2
bmp0STskNMZBBilnVsM7e8dFzmltvxtfgHTBal63sWn++xbhhhdiVfXQflkr1brD
qeZHmx3kc5U6K/HuJ6GpPrWKYpJdhwiMa/EcvZKLhGFckWfgEkPsCj8v2qCsbn1J
tAN7/qbltVCBafifJ4kT3S+Xui6X9f0gVTFJb9CAp5b4gI1cyAUZCVOQOsvoG9zO
oJe5A08grn5++bzkDXKJGgn3zY6iAYiPUGPqR5BqQo3taLD8ttUfbhwvbGS3N/Au
MCAN5tdSWhkV+KYju6UbUc2ZA5yODXW7rCK9UHDmlQ0McdtFjPr6vSsGUj3YqzVh
W1pg/Cyv+i0T8inbws4cPd9tsyHK2c2F5N2fpMRLG6dBY8euAFTF1MvCyi0qK+P2
Zz0OxtdIX7ff6gr+C6j9EiKQ51bHYP9CmIUvkFPrvcGlLLvyGbZmTJXGKy1Sui2+
it8YZrTHUv/eCUX8z+4L+w==
`pragma protect end_protected

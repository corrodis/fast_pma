// Copyright (C) 1991-2013 Altera Corporation
// This simulation model contains highly confidential and
// proprietary information of Altera and is being provided
// in accordance with and subject to the protections of the
// applicable Altera Program License Subscription Agreement
// which governs its use and disclosure. Your use of Altera
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Altera Program License Subscription
// Agreement, Altera MegaCore Function License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs for
// use exclusively in logic devices manufactured by Altera and sold
// by Altera or its authorized distributors. Please refer to the
// applicable agreement for further details. Altera products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Altera assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 13.0sp1
// ALTERA_TIMESTAMP:Thu Jun 13 12:36:29 PDT 2013
// encrypted_file_type : mentor_tagged
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
tcc1kwHL5fXNqtEUb68+wBynGQSYJKBSpe8pKBBwg7eipuaOWTAmeRgTG8BGsgC6
vOEo63IMAu8XRMnmherfGxDHzpKogSso4wTP35NEXOT6+2P7MiBrHHKWX3sIwnvF
PmOg38Qr7wvX1y9gsZGIX2wcG93pcDmJogpcxU+Hx0M=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 3584)
umwlH2Ks8nqAj7nrLKkmu6un4KgHYKID1TLBLJGUKVRc+Bpxk27rYG6cEp9KVe4F
EYIi7hqea9P8x051LfkREz4L7OQ7I1/COoVAhDF/dFwf+NiZDuDJ9bGf5h+nKvXU
PDk6SBQlEVbK2lzgRque7l7/A3N5T37zRwtzvBw1+U1MEBPBtTCZDV4sq2BnyX0O
FspbMsAGVF4gP+vOx1KVK481IvkN6BAChNSjw7PMFndXUvD5ZYowF8XmjkTBTLhK
tSdWtwDZH1A1BvRwXzEDTgvdsT4X/xv15Se68SwDXvA8T4knY8mZNcKI83CoG7wK
vqIkfRF4GBFglZhnc1IC4/7PwvKhARACEYWx3bkIclmTj2bKrZTNk9T65qKrVgTi
ZG+QUkVV5+Il+8fu8C8NUJlLQLfwr2qy66K/rLPJQECcWpZu2tXM0L+X7Q20ZCNV
7pcxDQJXFfvMCSDOobxK9+EKMnziEX1Zkht9PMjQRQ8bNEqDX5L1HkQQfxdxgrDf
WBdTDM3GvbwtyAw9BzX3RW4ghbrW8f6sHUKxmKDTtAUuzaxPonFCb8PR8RCxQ1Mj
VEkcz29PQ7UCC9wupDdCB99bj+pCu94H1fZE7emJtf5YikW58xkT/Dd1lBhXYB8I
+VgIlhlH7J0uANaKpjDB8B9nNO5vuYi31DLOI0/RyZ1v6pR597PvLByb7SSPLmUn
d91f+M4LdF5y1BeM8ljouliwEbMLNBUSuAQCcil2NczkY1jr8kuLWSEXM2obN3ZN
HVbJvLWSC2d4jBsvz4DQOjml+Ok9pXLMUsTgWOD3Wm9ZUF+8NL2IWnJ1xkuoj+Ci
sMYl7FRbYcGvpTh+jlHK1WSKecVS7YlMVYpdQSyCv7rtowhgL5P3qCSjsowpTfDb
izRb3ACE/c/oApgZ6Op+AceuQA+S5edoDElTkg00m5cWrSSbyLZ5hsSO1uS76jri
elEVxAiN5DIx42gfrfTzX83Bs3WUcg8UGJvtY/6c3ZfEuNoyzZnYOlicR+nvHNdN
GKE1WeDmvTSI+APcF/qKZXv8z8JAH/LZf7b7b73FGJNySf39zBr7If8LjIucwVTB
AS8Vki/AJKy+JESOZxCChw4USVKktpuj/NeWNyfUWOpEpZuLGthxSg4HPOsvCkUL
4ql7JW/3Ygazi6+5eULZL+MOwNDZSAhGgbZAiDIIsIqEoF9ijN+mVS0DxDjI42Qn
czXI7Tyf/IcqrIbDR90gTslIAVEgITpDR/iysvD0AQKgso+lp1FR1SXDfyPw9Pc6
CqRa7uFiXHvEFkjh/FiZ54uRkgnt5HQzvj4zedZm3VZNoFl4Q9YjW/fLb5ejCLA+
wDj2iJlb/jfdYNwvS2fSk8q5X4itavbgrVaPtz7KxUkHc+TavEqzPfKUc2XhWWwC
BteUzy0i2Ws2KjKET/Ff5Ue5II8JHDWQgHGzGnkz8AcSFeZF51oZJJVem/pAy5lL
zL1cw0RWbU2eu9H9samLBNbV3IbMEyzTt3BfSe4HjSglN4PxN/28yfpz7wn0r8Mn
+BdJnF1ztZmnN5lKd21Y2UH3Mz+1AV0Q/bna4RrUmklW4HLXLMobjtNO5gCeznve
xIMZQ9LCpA3U7TaVAcvJGOskHY81bUA58+qMXPacbhLgBXvWFhKTd0CVEdg+d2Mn
ZnDzlN9SB4lCZOnfcefz9c+UQ92vbiD3wO9wehDrKIav96UEcO4ZWm+zDAkXk7IC
2H2fX9n3Jf8gRwbZeliQfKYcRlZ9QtNRtquafa9pl5X+PJg6R7fV4CE7I+uBzaKD
h/aVYIlyB5QIh5FbnMLWVCderEa0JONyAKpjf3LrvAJRaLWm7j7MZ54PkZzRU5kJ
D2ue8Y+tIseDiPRJsnTWbUhP5L08tomEIN8nS6J9jLwZcu6bkCWXKweSJZm2x/P4
VAJFxfFBVMB6vCBbky0Xqn+/No7QaUHAzcj1MUi3+b6EPQ/ttfZs3eKNMue/EpWy
rTM3PXR+0C+tro41YL2dE/H7zrAd1aKwkI6+YG0/W+joewDFSiPaSLE2RkzgJL1S
X4XB791SIuqscgcxxejLWMq9EVA2oB/A/tDITq9mdc0SBl1yaFRBj44FJmKEAumL
nH2xSpB2muq2fMfxP8oz9BP80LgTw7srTBk62NxC1uX1cOgTShnPKWw8aQRjxU6h
o1t+iB6fUBpJr2argRw2irz2EnpFpxtVcm/RDVq9eLivYyWsdrmUo/fAsGa9D8Cm
Jutb+oWFDMRD/OU0Xw7eKJMPIh/kLONUz97PKRL82r26iuL22l8XMh3svUMw7GFR
gXkeEHRMzFBas3DBkyr8/w3xfJzntByYBgQ6KXmukuelRVtZL28OGKU5pAinNDxL
KMlo68wd1HheePHQADZY/hb9F4BZyHA1Kmaldiw1QzJ4zBH3Mzr8mMQjcUpuqdGp
qlIqeeM7n3mQJs1yl/PGkKRFhcwGGPs53AByG1BoIRTAL4EYAj+dbqIWHTExPaG4
JiddNk2mRjrXE/JZf0s5ZhHXOv2FYzx9/B/7Kna0rk0r9W1PLr7GowOOE6K2i9wT
eEZR0Bu2E+wCZSpJIRyMTe+e4iRUJLqt4qnHbf9+ZsInUpX7qX6uJ8v5uD9uX2xT
NqWFmOdiwzmJN0LHY8qjKKCWLaZ12vcqOY5+1G7xnWCR0h3ctNye/oXBUcGwR0EJ
JEXsTHw7kgqOODm0q1UthFAxAwbfjSVSHqkBIDbBToKmSRW1ufxe+4KSny5I1hci
VXqI4KXLkjgdrS8c16r/+J6n48qYmB7HozTQSIUK42T3fU9ROqNNq4oKh3rKUlLy
RKSDAevcNeJsWv6+HvZ3CL+xF4KwRvrzAW8eZiVsEgJ1sodOoRyCYldlEEuA0dZV
cm0yotMcd8ZjEk1CVr3nLAlLVhyrHuT8mfFuk6T3GSOEP5NdAZccBU6TIlGMHeMf
jIYJeGncQNvwZOrEa2pMVswukPdvu9LXozLh+4xLSPhwWuRkyyRJODVhyu66QFEL
VUceL/Egjp5BTZm9ptVSDje7kroBz7EB6ZxDXpsOr9ICKBwaLdmZ+QARlZnn1Hkq
1MoASg+c3ASJaEoQA0eBf3yrW3eEsyU+AC2ZiPbJCuN4F+AKSmWFU2UR2GFL9Ek+
nLDlNVK0CRmWFgD+pIZ0uolwJx6Qng5V6D/LLy6feb7tLVBN+R9ZqCdaKu4hWDm3
KKf+/rpquah23LBPoSx2cUFQ1hULnOfQyzWui1G0gZsqHfUw/plPT0AWftlpY1sb
Yr3Canux9atjAa/0X7iKuBDKJddsK8mO71lyItaZfIfD6lHrbjTwg2lmzczXtbWp
8Phq4iCnEJ1Ng0vd/jrLuqfabwjLkZbQe3g8R+hlz8fXgmDe3PYZYHgY/L0f1Dpb
65p6EXDphRPg8AWDSxdlPBofORGE2xDVVMcmb9U6VullDPFaoDftqkcdadjJfILY
2rjIAuYa4Vj0fXaLNY9zxSiG1/pAfUDkjtsnBiLSikOn089VrgLSxlKr3CKEQ8Tj
Xq0LfaWUuOmJLS+vbt0rkATKRNUFEffRI793EIs8FtwXtkuP+mYgRMroN/M8ewmd
8x592J0nMVRftemnRgvTYbWyoc67bVN2IuaWX8VlJLwevQgc8JmsilJaXHPnRtkc
tOgSR7B5rbmBm5PVabJA3FTECvoLQttTWpEYovrBv0SYDLqddDtPhw6618dTv8p8
IVgH2zR1nswIH7jLhGIEd27Y3IGzV7Odou6CeC5IjZi2NowTRGVYax0/uEEpKpZp
EmNYHNNsN6d56NUTjh5+KkW+h2hqyyGI4D260WXM+xbIcy7MQn+NHqLiu5RCNVMj
U4bFi65IzXW6/pk0uRX+mmaRoxuASLY619cGiKVqw8Ew14/zMSqnLxb1zugdXK+v
67fBzle9DpqQSm9nXB1qrG6+oRW4KfOeb8lfEqVKSOi1t27DX8gaMdUhK9nDgZ3E
5NQ3W4634aV9fsS5QogW+7Or94sQEw0zRnj/pT1DxhIDyG4sZ7i0wKdxCfc+g8k7
8mkTbR4XJFSakvjmK8zQ6HJPfohuFd4ygFNwu+FCZTdUGphVEhkZ1QkxBMTvdpvT
pPm+DpJGKL1/xqrPlfxHqs0tLCZZZopRsW03sKn+L2ahScM39cXuaxolJbyNBaY2
fl4x3auGLQbViSpSzPZcXKwZ7IFt3dn/3TfKjVkpCxBiWywHBN5+84W8KKx1c+aV
BDIHC8/i7uwNAEsRX2cDFJdYMM1sOhjX4lbzHPvDqm+WnWOmnFPaAgg+aUgm2SOz
AUW54i0JEU8txs3hI/+Z8oMp7G3THpIzc6x/Q54an+WTPwZSnjy+CAziVAsQpkK0
K0fSZm9chd19lQGBjt6c17lfLHvtVkXxMuVmMqdKVJUVKZR/SJj/ott5a38NNC/8
KcOtT8RZqPNyd4hi1IJ48drD1LA5zZtTGh+/eMGtccQnsipQTAOVD68ISS6CTDhM
8AG6hjQqVVwZxpkXY81b94s4lqxhFFGPFtzYqMHHbTy45XVYT+ptE3wQQYyAmzFL
l/kPzo2BCBxlfh3KpeBMCU0MOkioaQD2f/qX2fnso2npanzvGKwYSQuU9pj1bork
qJeyJeBK8vgAAPKZ1EpncbeEnHPhqQ0/NZFH+ZfV95c+neTRqguOBHAakErC4nfb
uR2oNEDf8GJ6wq4H5uOBeCSieo3pSZtDPCDbPbXzVaDTMeekdOqWFwz5bPH1DBsC
vcL5+jS47sTMGe2tVzwhTy9RITepMm1KKHFW0hgfOy8=
`pragma protect end_protected

// Copyright (C) 1991-2013 Altera Corporation
// This simulation model contains highly confidential and
// proprietary information of Altera and is being provided
// in accordance with and subject to the protections of the
// applicable Altera Program License Subscription Agreement
// which governs its use and disclosure. Your use of Altera
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Altera Program License Subscription
// Agreement, Altera MegaCore Function License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs for
// use exclusively in logic devices manufactured by Altera and sold
// by Altera or its authorized distributors. Please refer to the
// applicable agreement for further details. Altera products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Altera assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 13.0sp1
// ALTERA_TIMESTAMP:Thu Jun 13 12:36:55 PDT 2013
// encrypted_file_type : mentor_tagged
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
T8n14T8kfzxjfm3ntPB6lk2rUNb7QaLXBuv+OttcufbaULpJpGBGFpFYZM4fllqJ
PUonk5Mlqc+8edqZrSvGcYyovVkbBAiOHHco2gZspBwdSutbXwCDIIjrGO/zfpgJ
1AP0vxDgJ59J1rcW0EJ5g9MXVBjvgd3zohdRKC+AiCY=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 6032)
YBWnElThvsEFjR/4HXNxpzl78ONgcmR+cOpJCxA8FOvJ6CB/ajWuNuHNITHNiQS/
IWVNiyztXgr1pBYuovyEhUlNQtJ1KIcZzNiuQHpQH8nUPzDKXWhVZ8tabGJXReij
1atkmEjdcx/s2B8QEVr+h0Vago9JjLwqqjUa8T731pczmUbQbaPbHtfrbIHX5MXE
YwN4QAuDXIuIKHmC8JhFj0obac43WVC+OYOsI1bx+Nrhzg+lyXQdfqXimOHNROBB
61b8Xw8LUPAxquySG7q9kFwsGG2DX34O8JBRs/hEPZPPBOMnU4O8FYOA/I19tT5w
A2dyABzy8tUDrXTy9N0/W930wRWDLJGWDA5uKGdyvRngkzId1MPDL1RFvA+wM03y
W4J77qKHTAJ+/icuRhBK+y9rAAUlmNwJRPa+nnvepqJzMQiQf43FSlnlPOX6zgF0
YB1FcjSDg37RI4905Ji0sDpa4UqyYXNm2hWHw4ZDTv5SOtx1KlVT13lcOYhqjpQu
A5c26lfV1pZhAugpVm8giSCPkHnluUrv2BSUTDiQPuWTqjbL8FEWBE+74uqutFuw
4Ha54btswiqtS3Mr/uts7SVxtZEMfo1UfYo8Vuml55q7B82uk5He0P17Gdkr2ZaJ
EEBEo4V19Gd2JogX54TjwZSof1WurO6uSBx3egALePrv4l0nxMx+c1RNS6kGvitJ
+VGdxkwkIpXfTxBPwxfV4eP0V8KSeP2AZjwhluPCb3spbzN4rYL83ux1B5N5Pn94
0tm1Xnbv/ihYwNhpZIG6j+6rsti8MU6UTGiW84g9TE/LYT1qLmQEFeS0yhHfpBHX
rYA6KIDpita/vmSl4t39m716xoAE8Q0J8yQE1K3FjJYa0aHylFibERnaqvlPFzRF
VKeyOzZfZQZ76hSqNWxxY6HMvvwk5U3OZwKYQawfQISofTjpZl8FqrAv955/aBcz
+au6bhf6/Si4LqbKmc+p6gG3GaEBiKjmXV9NCiJMLwO2klfWINWuX3IlD1D6JE8a
ZO8jX63st5fWNQIFamhHE7xzwtlgo4ZxbuzHE/3bO1UKCi0oZLU6PD8gK0gIB+/z
tDByqtwE5PUCH4tFN0NljaYjiOJE+0+T1tj/m5QMWx5qJZkOxrTGo3BY8W4MDlg9
6mw52ZkMSMBNLllM67OEUDoTJQNdvoANypWzPHiHqcXE4lb/n4g2uEmDeNn9C4P0
zxBRPT75en4ohwgDyHswywDD7jXZdS39gVA57V3eg5L+FecWeWK+1n9HlEQ1MOiw
DaD8yAkNLO574F3UZyH3hzDJ5syyknzmP6tSGVGoUP3ObydFX5/yfxiXREzP0guo
0vOhG50sy+XrsA7UedlUstUXJmsaTqRGFadCX6CNtIgcgSxmhgucTbHSJ+9vh3l8
8MqKJrNlyRwS9JiSnY44OrEHaqeU2bCUKr5qGGnYmwXMthjBf3VEuIY49cJtF07O
utUEDJuAmQaWe/E7LvTtAlJ3/uRemEJ4BKiITmma+je+NBqfwJEfVkJCTYnTRKKT
JO2wGmq+YfrmkbIBvQOWjm6nugsMho5M13QPvWOkafFlINbwnAX9skH3muIwyG8l
Lgz3tnagJ9DNcLCfcKKE6xNSBZVLKxfwyzut5JezUxBhFKw5NdlYelvULbmjuD/0
UXoJ1zcSWHz3HnjXKK/Z1zQV6d0ES9Mr/wjMl8ztWN6j7qsEydllPZXm9BnngkqQ
enPFtbuRBg2/sALvrjNGmaYEJbkDwuBGA3l646yxGDHb6crE0cFCuZqzQjsF5EC/
8F5VYtsp9CeAJWAToK3H5/hJlKqf+kiCKPgNcYAbZn43yIc5dIGsCKaNE/wLRARl
uXhuD9YGopMZy7MWtColBskcLgMQKjXhif1kGWY/eIoGKVSZRiqjrgbXkyE0eF0G
eT0VMYsgWz1I9vttNYYulW8xg73s6Zyalv/wAHQRTpiQkjCdkHxGRPanEuQZbFaW
hW1RCRgB/4dcak0EcI3LhYBenItsWugfwSLLxLy5pLr4pwuwS8+PgXVggQz5tAjC
u4Wcq3JoQnt3ywcINuApl/ov03DMs+JeRjuQyhRSrbBmHp38ep9gmqO1EwGEVNay
6dMIqWW8DE+5r1IyoLVQXFmbgH2dDeNjhNhWQ6DqTk02TYg9EbH/gVSnkP/oSAhC
WYTmgk9aPzp3bdUGLzZAMv/lwTzKdWdrx+3/galQR7QwyXiIp8KgBTmXZxBDEt9y
E80VpwlFyeO23VQdv+vZDwofVRKLw3I2rGWYJG4m6whIojnw8d7L5JTuxp4wb1T0
ZBFdW0JAgHvKsktdAKs4YOvOtCw+Wa3GVZONMbUzbDUtMK4MbtYZ9k5zac18RiBZ
ELp6Zc22cZpKYHWmHagkVpOP0TV3Ur3xvVKQISU2Cx+v99thMwTOo4FU8DTamhYL
oXQr39Y5hy5ifiorL/V5gJ+eucTBMwwEnjB5PYNnK1l7Sow0NKeAROdKdcC5QS4l
esjYGkCRU/O2zQy/wlLnO325041hfctcj1/C1HlZ+DV3FjdjzRVZ5yxSCO8a9ILZ
CJNkTAMcO86e3DOh8dbWtcnN76nYku0wEsH2vFlQirNgYj60KkErAY2RSVWiHD0w
mGXS5+HWGimwG1EN+gCb/UfRn0hWZ+xyOGpmQndFYqzY3qAvpcQ/W6R0bvN1M82L
BBa+Lc05BQehGkRLAeIJq1OLs8KfaMeQxcgIvFXxlHScWBY3W1fDPictRY/gFst9
01xb78mto4eTYYvljXfH2xTGM6/ZKo1ObZsB8xv37aTLWFuQ2f76vStO3VrYGitn
wcGoB5iV762Gvm7+HKjO+VhRLEt3tAF/RH2dp1vd4kBNhFpbUvscpY66BO6YEVAu
OMx1r2rLxb/xYXqmW1Bw014W19k8vgSwgUemfZIpBpuMC93BEcZalkdoK13SV/KZ
pmTcA9yqMKLpIi6VKfO7mDMiZS1r/9LjwR3Ap824LSe7onVueHjsrMQ3DDHiDMqv
/gWVTwCyOET+jqnumKE9JtaOB6+6sYAroZZXqIaD+iGEYuF+jo9YE+FwdeCOI0mS
bPIW6tCnAgTpVwPdvVVhsAI2dctrGRfYMPfZTZ9AvngL6m9NxJBg7cW2hCv/VBF2
nJHccRQ8cpDuaH1hx6Y9G40qdjrlBroyCyM/b/4JVsXVbJqJPz6UNGcpP06OUmaJ
lyT4Rjgc66hoZxJCO9u0LlhvjXgCbQLc3IPk57lTN+SeF11R8pnREUB+w2fFXrK/
LDm+i2EdwpxCE4gY5ea1A3McZ4F4ZSviZjoMoFznaw0JWQ+8apbHDZpBnZ3cCYQo
JlPmSyxetcFcdwsb+aocPcQMxQTZ4pTBggDK1s9CsZLTi7Qhy0irUJ5cMDvYnQlf
MRX0KbeMaqEQCW7mVnS1tey8scPgzmBkDkWqUodCo76OjnkCUkynSaSi4eDV9Cwr
PoE6xx6EyXDS2uwzorsYX8PhBrbkS1erqh8+KUn5T4oNMARokVbuoID/7FMmIJ2N
VNxWAXJmfMrfmTtuvsFrA8EFDqZiXe1spDV/ii0jRHoEQ8CjF3m1qHiUlNFbw02s
HGHZ7ruzhjDVqu+vcMFlYiV7Z40T/kKYgKZXrzXcytEy4DrGtNfUrNZw9IVRmhb7
eYHCYM8SHOxAbvwD5svpNaopMp4dImSIbUm5yJ6V7EtdqXXE+fjgKPZv3ODhoPz8
r5NCXdo69un++eTHoHGmIhcDDgDXnVN4/5N1MOZFdJF1kX1lnJsYaupFAiDvcI3W
KIGu2CrcBpHa/NQMU1opXkm2T3oGdkcip9354e/P1BmW0kQ9eMqRoXsau92JWGJA
eXIpoRIGf0UhxSatFoxJeC2Ng3+2hlg8t3Ewjr/BNTxQ2Onrzvbesq6wyoL0/acB
3AZFanWSqfPNpE1ZJC/KbqH2ZS8Wz7Ao8laVGbK4Kjxl6V9LZG4CMR9t66HgO/mT
neKQlQT8C2DunKx96Em+BZw+YFI4/1aqzGM7+as0E18GYx5/dWHbpD1BtKJ9k98d
7OcrD9xlnCL1QF5IuLYbjOsnLWLeUd0i54cprRzRxsRQbfoHjI6D3cb+rOP4JxGL
HpWadam86vx4ZU07HC+M/ZQWYaceGGHZE3+bGTvUvPBOrk6kRuojnd7x0Q90mx3J
tjNb0XAnIZ6H+2X3kiG+tcT1Z+aFkmq974gbQvaBJWpz3wPOJgF3eKCOUF/BxYNt
5c400AZUsOGfewCofyPRKZzQk154afV8u2nRx9reJsFT6hgekQjOCo4Z3S1w+iDB
cnQww1DSWPnd7dzMBBhQqIaLf3XnwwucMyUwB6NYvQ9iNcAZzDpJ4XpdCQAzMgJ7
7BusrUOzOmUHvpK7RwvkUP8muYN4X/0Afy3EKH7oxyqFe5GGulUWDIECtupY+epZ
sOQSmtnpgLvLsGeLAcLYEY5mdku9YRgxhNwY5eyPw4ylhxdlNPf9DxanrlaRYBw6
ZZWMlxf6NPoF22kj0uQbdrie6wKI/Bm9NbqVbbi7DuBoXudsehjazuboyjN6GyT9
kkevqA5O3RQQA8IDiSEiUaJgAJ90tAWMCtbt5ostDxkHtoLzkaWJRKd8dRJo0MqA
wkTwgTFsUgIOjSv2InpOqaPO7divQQDRIbUUO98mry3jLZa/DJXyw3mz7ZIX9tLV
bF5pvP/Ls2AfKpQ2ZVBECf/mQ6zdN/wBzI6dwms0pdMncHA6kHIyd+mI8i4XkPVp
EWunHbq0ER/EB1hUlLI/OiuWooINDGu2P1Uo9az9IigEqZuHr9AeL0j45tP45mRC
gPDfWJr/fzQFg9jZN/0tqK7xQAR/37ierhVDKIyk4emq+GZV2pHwu1wZ2uHSyhT5
c2P5OQsw7VY7673M/A6K6vZ8ol8B1T2JYVo220yy2r3YyQrUEqMigOWKylq7l9YM
xmSj4mqPuyMCzHz6Sg7E/JF8KmwoDzw5uBU5t7xEf0Y2/rJPvUBN91WcnJlvRJAe
1jZ2ZQW5VUiex+leHI96YKysrXGprNTtFPUszIze7G1dmhn7sf/j/swAKodBomiX
r1ne5/DRJonIE2vMQSzxlVeOdGpXZWHr6N7D032MItG3RWnjkGmgeYCZF3oulK+7
8Et5vqpQxj/4kHWESIw8QkZYc5xrA4/VtW3cqVfleTHgIM0wXKVrX7X3kmCoZc9G
AzCjwVlcEU34viaz1EHUZhgS+9sKslv9m+ZctzyW9j3U2wPCHem0+tne4D9VYEB3
XYrNi8DeKwS6A6rGrhPTNurYL2IX9ywbtBvNX38MBkji9NRSRr9KakkP9rpSOpOG
CzEsbHfDRCNbGcOWtk6L6jd5mY+xQUer/T0I7N9+PqACp/rbyWm//mzlPpYmGiyp
sSYQjYoJWnvOzDsabFCZIL9WINWlp77NR+a78bY5s9Q2xWNSYrLbTaIkowR1sq9R
2SFiNogNSEf9/9ogjl0Gqvk5ZferhhyDmGs+zN0m83ojKAbj3VQvkkx7AaBPaDmZ
Q+u5ucfjiTyhO9mUywzheWGP78QEXnhtPRMxgY5P7GmbuchdoKM6ghorMXGJ8Ir8
cCYeMHFUzCgPAcnReaVu4Jty6t+W/2s41aVLSYPBECtQooV+tRRqkR0OLH4H7qKU
F5xoohESBDPmpsoQbGAJt8lR/VohwUXRSypxwBYXzzipFCdv8U7W4q5HuV8yX94b
YJuqDMNsBSQnsqQe5nOvP82xYwd4FFtORKNZhXXh5kOc44vT9VUX9MaDWg+QHFk1
F8DaOSWVXg07W+FDvi2mxq5llGqZH7Y7xab1uA4sNBW/9fEHCN+4KMl2hZHM/9VW
7c/YbuU+kl8UJNUFAZrEWgjvDn7Rr6mrtKwHTfdy27t5Pkg/Q6vbj536QTOxZgCl
vrU93Su0d1bMIzzBRy+RLFE4tiFtnD/yUXF7X5I57C8O0u79n9wCBw/sewTgzT94
vNEXZMjcfn6XgPz8n91PLxBRMcgQDzToysFUIOay6i4xlvzpLT25Rpq4BgXSHrSx
+Kp1fxNgP0eJUBxqgzOaYHbUlOFJKrBgT6YkpVDRaW98X3jMoCEzsfvHBQtmBatC
xVEvhHhIMpG34RaveFYiSmeLCQ932wBjrOQUHVouDSHVYlw5cDdLZ8tC12GGFeSJ
7oWgOhP1zsaETVFppt/V5wpcxerhxfkJoESb8g40/C4Dvj9IMD96cGuXwydBSEjo
VZ81oGcjhTA5mgLYtbMqMHxbT2Zu0Ib0akxcClQg/L/k7musKMd3FjrxMJfuwf4P
jpjtTZAv5AtbSpg9pkOrPNQbqAHU9+hKfRAPALMAaxiZcRVjXRrpUlR6X3tHLqWe
3Ik4U/WDjBkVxr62T1NlzwTv/OC48tzKG0rKBtFLgHr5FLInyTe9hAMSmYsIz/4b
6Dzq1pbDJTwl5JmkhcSVw4c2O/x9QUd2dJi4Pd3NXIuyZX+XR1FYzi0hVf8uy2BD
a8UjoPYxUTy9ddQCXkS2BD1tuflf1Gl8kbE55rj52BwoHhwJJJ4rWO9A1Uc11YR1
yVISxR8JddBkGoD7JmeaJgYxoBqxeOgi7GfjPODbmaLkmeiZfLGdzuGRRxOO4HaQ
dSGS9ZvrCcoXYG7PZwcHOzMKKy2nbhbjoBf2o1QSwuej7yX4paPti0b30Ds5Fz/v
4qoeudqPQdffdz0ppOFzJ2JRmy2Yjxsh9jDWbJM2Hogneenu6xVUWccLiViVTpZx
cjrBzu/dVqgJdGfketOmNZMbFS3dawUux8GVdFdQo8DzgbBDiBaQOd2bviX2j1nY
v+AlePtiJ9c5OP+1YgFjyBY3D8TOtuq7AFO6FCl3F6+GZHpNiFWtOVx/Zd2Yntus
ewstKHw9fJEXfYJ48DJJENgP9FoOvb80YfLwfnYS09DyQCcrE+DesdLngUA6E/Ei
nqkLtD/Y3UqyVqICemCr8VRV58CiN9VO5Ut2DIV6dclPonFT5eflWY/FB4BzJDuo
Qe/n4OLu5sLpAWEdrCbe/lOpAIHsaYVptxefEt0APn3xXSfII3bSgOXx3QdLpKEg
AW0+cheJpXf+Mq3DM8/5bp0TdqRifA+LcsJXjPGOok6IzYTCp1QM3NmXSSKM0WEv
4FLbwEq6RY5pX5xep6EuMsE5KlZ/UXY4mfZ1BTumCj8A2IL9goA7bj7RXIsQt9LI
XYJQVp8Dqx8JyMEzgw8Ur1g2/9fQTtj18MJnCuClBVv8qp7r6YOJSBkSbeKXs07M
1wuDxjySetQaHbqqsOZLxBx0INc1wAI/MDSem2ZLX4jG+QW+BBkwqis4B50FmM7c
nOs4BBTSmcR7SwAOS3/ypcWB2eRLPN4IUSFcxtStSNuVMWGFyVH8qILx8lGtpbUs
KKdFHN4BoKsI6tnr33ZdZU2/90A4IpU0rMOnynAhkDVWJFfFeMnu8A6aLH81AolG
4+VwyKjMq9j+m6HOznWSujaj+n+3s4lqlsyJkqUKRKBbxK5m8oaaq3I9Ey9rYwTB
HcXCAOwxvS1Uhsl+B1XO2hW+5d9avtGlvSiLVf66UchF0Fa0z7E2YikQrRK28dJQ
rSoAvWZGyew0z4sxfCDrS+h+btUVNMLuCIyg0XifpPFSXXt1phld2nfIxvZsW7EY
DROF/8gpzgcWplDZ8McHdoeWplErUNIRkSKqTar4ub5jK60F8kJvKoYMaiRNCt1a
KdVdfYFwfFAEmJba2iwU6YCvoCXXoW43JHG6Wq4eOOHFzTvH9LhRrrrezx9X4ThV
yRlz4pcwTHGUapK3My2EfDk4fuuPCu3g3+LZkKMxKRTPQqSNk0l2qyvkCqw+aMdW
rLJul8SibdIGNFI1uLh2J8tuQ+zZ5cjHW6hJZ81oBayyyqo6NTUSWVt9TjBJqYob
9w0clgCVLVgu7C6tLwTMM43NulcXqtZ74DF47Sn56RJ1XpIvZuvG1v2eDcW3FuTi
9ngUa6+CZO2fVrnsIX7TahTK5kXnqEvqqu2wHqg98AWCYYf75mpomLkJ+PE19ZWQ
Jh0WlTFGiqSJV5wDewO2YH4aQU41G5Nak/x0Z+ExQcM=
`pragma protect end_protected

------------------------------------------------
-- Simon Corrodi										 --
-- corrodi@physi.uni-heidelberg.de			 	 --
------------------------------------------------
-- LCD Controller							 		    --
-- based on version by Sebastian Dittmeier    --
------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use ieee.numeric_std.all;

entity controllerLine is
	port(
		line1 : in std_logic_vector(63 downto 0) := (others=>'0');
		line2 : in std_logic_vector(63 downto 0) := (others=>'0');
		-- for external data -- one line ----------------
--		char1: in std_logic_vector(7 downto 0);
--		char2: in std_logic_vector(7 downto 0);
--		char3: in std_logic_vector(7 downto 0);
--		char4: in std_logic_vector(7 downto 0);
--		char5: in std_logic_vector(7 downto 0);
--		char6: in std_logic_vector(7 downto 0);
--		char7: in std_logic_vector(7 downto 0);
--		char8: in std_logic_vector(7 downto 0);
--		char9: in std_logic_vector(7 downto 0);
--		char_10: in std_logic_vector(7 downto 0);
--		char_11: in std_logic_vector(7 downto 0);
--		char_12: in std_logic_vector(7 downto 0);
--		char_13: in std_logic_vector(7 downto 0);
--		char_14: in std_logic_vector(7 downto 0);
--		char_15: in std_logic_vector(7 downto 0);
--		char_16: in std_logic_vector(7 downto 0);
--		char_17: in std_logic_vector(7 downto 0);
--		char_18: in std_logic_vector(7 downto 0);
--		char_19: in std_logic_vector(7 downto 0);
--		char_20: in std_logic_vector(7 downto 0);
--		char_21: in std_logic_vector(7 downto 0);
--		char_22: in std_logic_vector(7 downto 0);
--		char_23: in std_logic_vector(7 downto 0);
--		char_24: in std_logic_vector(7 downto 0);
--		char_25: in std_logic_vector(7 downto 0);
--		char_26: in std_logic_vector(7 downto 0);
--		char_27: in std_logic_vector(7 downto 0);
--		char_28: in std_logic_vector(7 downto 0);
--		char_29: in std_logic_vector(7 downto 0);
--		char_30: in std_logic_vector(7 downto 0);
--		char_31: in std_logic_vector(7 downto 0);
--		char_32: in std_logic_vector(7 downto 0);		
		-------------------------------------------------
		
		clock: in std_logic;											-- Reference Clock
		reset: in std_logic;											-- Manual reset_n
		enable_in: in std_logic;									-- external LCD enable
		pause_lcd: in std_logic;									-- '1': lcd does not update data, '0': continous update of incoming data
		
		lcd_enable: out std_logic;									-- On/Off Switch, connect to lcd_csn, 1 is on, 0 is off
		lcd_rs: out std_logic;										-- Data/Setup Switch, connect to lcd_d_cn, 1 is data, 0 is setup
		lcd_rw: out std_logic;										-- Read/Write Switch, connect to lcd_wen, 1 is read, 0 is write
		busy: out std_logic;											-- Busy Feedback
		lcd_data: out std_logic_vector (7 downto 0);			-- LCD Data
		ready_out: out std_logic									-- ready signal
		
		);		
end entity controllerLine;



architecture lcd_controlLine of controllerLine is	
	
	type control is (
		power_up,
		init,
		selector,
		switch_lines,
		ready,
		send,
		idle);
	signal 		state: 	control;
	constant 	freq:	integer := 240;									-- set to clock MHz value
	
	subtype char_vector is std_logic_vector(7 downto 0);
	type char_array is array(0 to 31) of char_vector;
	signal char: char_array;	

begin

--	char(0) <= char1;
--	char(1) <= char2;
--	char(2) <= char3;
--	char(3) <= char4;
--	char(4) <= char5;
--	char(5) <= char6;
--	char(6) <= char7;
--	char(7) <= char8;
--	char(8) <= char9;
--	char(9) <= char_10;
--	char(10) <= char_11;
--	char(11) <= char_12;
--	char(12) <= char_13;
--	char(13) <= char_14;
--	char(14) <= char_15;
--	char(15) <= char_16;
--	char(16) <= char_17;
--	char(17) <= char_18;
--	char(18) <= char_19;
--	char(19) <= char_20;
--	char(20) <= char_21;
--	char(21) <= char_22;
--	char(22) <= char_23;
--	char(23) <= char_24;
--	char(24) <= char_25;
--	char(25) <= char_26;
--	char(26) <= char_27;
--	char(27) <= char_28;
--	char(28) <= char_29;
--	char(29) <= char_30;
--	char(30) <= char_31;
--	char(31) <= char_32;	
	
	control_sequence: process(clock, reset) is
		
		variable clock_count: integer := 0;
		variable send_count: integer := 0;
		variable second_line: integer range 0 to 1;
		
																		-- start data list
		
--		variable a: std_logic_vector (7 downto 0) := "01100001";
--		variable b: std_logic_vector (7 downto 0) := "01100010";
--		variable c: std_logic_vector (7 downto 0) := "01100011";
--		variable d: std_logic_vector (7 downto 0) := "01100100";
--		variable e: std_logic_vector (7 downto 0) := "01100101";
--		variable f: std_logic_vector (7 downto 0) := "01100110";
--		variable g: std_logic_vector (7 downto 0) := "01100111";
--		variable h: std_logic_vector (7 downto 0) := "01101000";
--		variable i: std_logic_vector (7 downto 0) := "01101001";
--		variable j: std_logic_vector (7 downto 0) := "01101010";
--		variable k: std_logic_vector (7 downto 0) := "01101011";
--		variable l: std_logic_vector (7 downto 0) := "01101100";
--		variable m: std_logic_vector (7 downto 0) := "01101101";
--		variable n: std_logic_vector (7 downto 0) := "01101110";
--		variable o: std_logic_vector (7 downto 0) := "01101111";
--		variable p: std_logic_vector (7 downto 0) := "01110000";
--		variable q: std_logic_vector (7 downto 0) := "01110001";
--		variable r: std_logic_vector (7 downto 0) := "01110010";
--		variable s: std_logic_vector (7 downto 0) := "01110011";
--		variable t: std_logic_vector (7 downto 0) := "01110100";
--		variable u: std_logic_vector (7 downto 0) := "01110101";
--		variable v: std_logic_vector (7 downto 0) := "01110110";
--		variable w: std_logic_vector (7 downto 0) := "01110111";
--		variable x: std_logic_vector (7 downto 0) := "01111000";
--		variable y: std_logic_vector (7 downto 0) := "01111001";
--		variable z: std_logic_vector (7 downto 0) := "01111010";
--		
--		variable acap: std_logic_vector (7 downto 0) := "01000001";
--		variable bcap: std_logic_vector (7 downto 0) := "01000010";
--		variable ccap: std_logic_vector (7 downto 0) := "01000011";
--		variable dcap: std_logic_vector (7 downto 0) := "01000100";
--		variable ecap: std_logic_vector (7 downto 0) := "01000101";
--		variable fcap: std_logic_vector (7 downto 0) := "01000110";
--		variable gcap: std_logic_vector (7 downto 0) := "01000111";
--		variable hcap: std_logic_vector (7 downto 0) := "01001000";
--		variable icap: std_logic_vector (7 downto 0) := "01001001";
--		variable jcap: std_logic_vector (7 downto 0) := "01001010";
--		variable kcap: std_logic_vector (7 downto 0) := "01001011";
--		variable lcap: std_logic_vector (7 downto 0) := "01001100";
--		variable mcap: std_logic_vector (7 downto 0) := "01001101";
--		variable ncap: std_logic_vector (7 downto 0) := "01001110";
--		variable ocap: std_logic_vector (7 downto 0) := "01001111";
--		variable pcap: std_logic_vector (7 downto 0) := "01010000";
--		variable qcap: std_logic_vector (7 downto 0) := "01010001";
--		variable rcap: std_logic_vector (7 downto 0) := "01010010";
--		variable scap: std_logic_vector (7 downto 0) := "01010011";
--		variable tcap: std_logic_vector (7 downto 0) := "01010100";
--		variable ucap: std_logic_vector (7 downto 0) := "01010101";
--		variable vcap: std_logic_vector (7 downto 0) := "01010110";
--		variable wcap: std_logic_vector (7 downto 0) := "01010111";
--		variable xcap: std_logic_vector (7 downto 0) := "01011000";
--		variable ycap: std_logic_vector (7 downto 0) := "01011001";
--		variable zcap: std_logic_vector (7 downto 0) := "01011010";
--		
--		variable num0: std_logic_vector (7 downto 0) := "00110000";
--		variable num1: std_logic_vector (7 downto 0) := "00110001";
--		variable num2: std_logic_vector (7 downto 0) := "00110010";
--		variable num3: std_logic_vector (7 downto 0) := "00110011";
--		variable num4: std_logic_vector (7 downto 0) := "00110100";
--		variable num5: std_logic_vector (7 downto 0) := "00110101";
--		variable num6: std_logic_vector (7 downto 0) := "00110110";
--		variable num7: std_logic_vector (7 downto 0) := "00110111";
--		variable num8: std_logic_vector (7 downto 0) := "00111000";
--		variable num9: std_logic_vector (7 downto 0) := "00111001";
--		
--		variable at: std_logic_vector (7 downto 0) := "01000000";
--		variable space: std_logic_vector (7 downto 0) := "00100000";
--		variable exclam: std_logic_vector (7 downto 0) := "00100001";
--		variable quote: std_logic_vector (7 downto 0) := "00100010";
--		variable hash: std_logic_vector (7 downto 0) := "00100011";
--		variable dollar: std_logic_vector (7 downto 0) := "00100100";
--		variable percent: std_logic_vector (7 downto 0) := "00100101";
--		variable andsign: std_logic_vector (7 downto 0) := "00100110";
--		variable apostrophe: std_logic_vector (7 downto 0) := "00100111";
--		variable leftbracket: std_logic_vector (7 downto 0) := "00101000";
--		variable rightbracket: std_logic_vector (7 downto 0) := "00101001";
--		variable star: std_logic_vector (7 downto 0) := "00101010";
--		variable plus: std_logic_vector (7 downto 0) := "00101011";
--		variable comma: std_logic_vector (7 downto 0) := "00101100";
--		variable bar: std_logic_vector (7 downto 0) := "00101101";
--		variable dot: std_logic_vector (7 downto 0) := "00101110";
--		variable slash: std_logic_vector (7 downto 0) := "00101111";
--		variable colon: std_logic_vector (7 downto 0) := "00111010";
--		variable semicolon: std_logic_vector (7 downto 0) := "00111011";
--		variable less: std_logic_vector (7 downto 0) := "00111100";
--		variable equal: std_logic_vector (7 downto 0) := "00111011";
--		variable more: std_logic_vector (7 downto 0) := "00111110";
--		variable question: std_logic_vector (7 downto 0) := "00111111";
--		variable leftsquarebracket: std_logic_vector (7 downto 0) := "01011011";
--		variable rightsquarebracket: std_logic_vector (7 downto 0) := "01011101";
--		variable circumflex: std_logic_vector (7 downto 0) := "01011110";
--		variable bottombar: std_logic_vector (7 downto 0) := "01011111";
--		variable rightarrow: std_logic_vector (7 downto 0) := "01111110";
--		variable leftarrow: std_logic_vector (7 downto 0) := "01111111";
		
																		-- end data list
		
	begin
	if (reset = '1') then
	
		state <= power_up;
		
	elsif rising_edge(clock) then
					
		case state is
					
	
			when power_up =>											-- 50ms wait to ensure supply voltage has risen
				ready_out <= '0';
				busy <= '1';
				if (clock_count < (50000 * freq)) then
					clock_count := clock_count + 1;
					state <= power_up;
				else
					clock_count := 0;
					send_count := 0;
					lcd_rs <= '0';
					lcd_rw <= '0';
					lcd_data <= "00110000";
					state <= init;
					
					-- specify characters if necessary ---------
--					char(16) <= space;
--					char(17) <= space;
--					char(18) <= space;
--					char(19) <= space;
--					char(20) <= space;
--					char(21) <= space;
--					char(22) <= space;
--					char(23) <= space;
--					char(24) <= space;
--					char(25) <= space;
--					char(26) <= space;
--					char(27) <= space;
--					char(28) <= space;
--					char(29) <= space;
--					char(30) <= space;
--					char(31) <= space;
					--------------------------------------------
				end if;	
							
				
			when init =>
				busy <= '1';
				clock_count := clock_count + 1;
				if (clock_count < (10 * freq)) then						-- set function
					lcd_data <= "00111100";								-- 2-line mode, display on
					--lcd_data <= "00111000";								-- 2-line mode, display off
					--lcd_data <= "00110100";								-- 1-line mode, display on
					--lcd_data <= "00110000";								-- 1-line mode, display off
					lcd_enable <= '1';
					state <= init;
				elsif (clock_count < (60 * freq)) then					-- 50us wait
					lcd_data <= "00000000";
					lcd_enable <= '0';
					state <= init;
				elsif (clock_count < (70 * freq)) then					-- display, cursor, blink
					lcd_data <= "00001100";      						--display on, cursor off, blink off
					--lcd_data <= "00001101";    							--display on, cursor off, blink on
					--lcd_data <= "00001110";    							--display on, cursor on, blink off
					--lcd_data <= "00001111";    							--display on, cursor on, blink on
					--lcd_data <= "00001000";    							--display off, cursor off, blink off
					--lcd_data <= "00001001";    							--display off, cursor off, blink on
					--lcd_data <= "00001010";    							--display off, cursor on, blink off
					--lcd_data <= "00001011";    							--display off, cursor on, blink on
					lcd_enable <= '1';
					state <= init;
				elsif (clock_count < (120 * freq)) then					-- 50us wait
					lcd_data <= "00000000";
					lcd_enable <= '0';
					state <= init;
				elsif (clock_count < (130 * freq) and second_line = 0) then					-- clear display
					lcd_data <= "00000001";
					lcd_enable <= '1';
					state <= init;
				elsif (clock_count < (2130 * freq)) then				-- 2ms wait
					lcd_data <= "00000000";
					lcd_enable <= '0';
					state <= init;
				elsif (clock_count < (2140 * freq)) then				-- set entry mode
					lcd_data <= "00000110";      						--increment mode, entire shift off
					--lcd_data <= "00000111";    							--increment mode, entire shift on
					--lcd_data <= "00000100";    							--decrement mode, entire shift off
					--lcd_data <= "00000101";    							--decrement mode, entire shift on
					lcd_enable <= '1';
					state <= init;
				elsif (clock_count < (2200 * freq)) then				-- 60us wait
					lcd_data <= "00000000";
					lcd_enable <= '0';
					state <= init;
				elsif (clock_count < (2400 * freq)) then				-- set DDRAM address
					lcd_data <= "10000000";
					lcd_enable <= '1';
				elsif (clock_count < (2460 * freq)) then				-- 60us wait
					lcd_data <= "00000000";
					lcd_enable <= '0';
					state <= init;
				else													-- initialization complete
					clock_count := 0;
					busy <= '0';
					state <= selector;
						if(line1(63 downto 60) < "1010") then
							char(0) <= x"3" & line1(63 downto 60);
						else
							char(0) <= x"4" &  (line1(63 downto 60)-9);
						end if;
						if(line1(59 downto 56) < "1010") then
							char(1) <= x"3" & line1(59 downto 56);
						else
						char(1) <= x"4" & (line1(59 downto 56)-9);
						end if;
						if(line1(55 downto 52) < "1010") then
							char(2) <= x"3" & line1(55 downto 52);
						else
							char(2) <= x"4" & (line1(55 downto 52)-9);
						end if;
						if(line1(51 downto 48) < "1010") then
							char(3) <= x"3" & line1(51 downto 48);
						else
							char(3) <= x"4" & (line1(51 downto 48)-9);
						end if;
						if(line1(47 downto 44) < "1010") then
							char(4) <= x"3" & line1(47 downto 44);
						else
							char(4) <= x"4" & (line1(47 downto 44)-9);
						end if;
						if(line1(43 downto 40) < "1010") then
							char(5) <= x"3" & line1(43 downto 40);
						else
							char(5) <= x"4" & (line1(43 downto 40)-9);
						end if;
						if(line1(39 downto 36) < "1010") then
							char(6) <= x"3" & line1(39 downto 36);
						else
							char(6) <= x"4" & (line1(39 downto 36)-9);
						end if;
						if(line1(35 downto 32) < "1010") then
							char(7) <= x"3" & line1(35 downto 32);
						else
							char(7) <= x"4" & (line1(35 downto 32)-9);
						end if;
						if(line1(31 downto 28) < "1010") then
							char(8) <= x"3" & line1(31 downto 28);
						else
							char(8) <= x"4" & (line1(31 downto 28)-9);
						end if;
						if(line1(27 downto 24) < "1010") then
							char(9) <= x"3" & line1(27 downto 24);
						else
							char(9) <= x"4" & (line1(27 downto 24)-9);
						end if;
						if(line1(23 downto 20) < "1010") then
							char(10) <= x"3" & line1(23 downto 20);
						else
							char(10) <= x"4" & (line1(23 downto 20)-9);
						end if;
						if(line1(19 downto 16) < "1010") then
							char(11) <= x"3" & line1(19 downto 16);
						else
							char(11) <= x"4" & (line1(19 downto 16)-9);
						end if;
						if(line1(15 downto 12) < "1010") then
							char(12) <= x"3" & line1(15 downto 12);
						else
							char(12) <= x"4" & (line1(15 downto 12)-9);
						end if;
						if(line1(11 downto 8) < "1010") then
							char(13) <= x"3" & line1(11 downto 8);
						else
							char(13) <= x"4" & (line1(11 downto 8)-9);
						end if;
						if(line1(7 downto 4) < "1010") then
							char(14) <= x"3" & line1(7 downto 4);
						else
							char(14) <= x"4" & (line1(7 downto 4)-9);
						end if;
						if(line1(3 downto 0) < "1010") then
							char(15) <= x"3" & line1(3 downto 0);
						else
							char(15) <= x"4" & (line1(3 downto 0)-9);
						end if;
						
						-- new line
						if(line2(63 downto 60) < "1010") then
							char(16) <= x"3" & line2(63 downto 60);
						else
							char(16) <= x"4" & (line2(63 downto 60)-9);
						end if;
						if(line2(59 downto 56) < "1010") then
							char(17) <= x"3" & line2(59 downto 56);
						else
							char(17) <= x"4" & (line2(59 downto 56)-9);
						end if;
						if(line2(55 downto 52) < "1010") then
							char(18) <= x"3" & line2(55 downto 52);
						else
							char(18) <= x"4" & (line2(55 downto 52)-9);
						end if;
						if(line2(51 downto 48) < "1010") then
							char(19) <= x"3" & line2(51 downto 48);
						else
							char(19) <= x"4" & (line2(51 downto 48)-9);
						end if;
						if(line2(47 downto 44) < "1010") then
							char(20) <= x"3" & line2(47 downto 44);
						else
							char(20) <= x"4" & (line2(47 downto 44)-9);
						end if;
						if(line2(43 downto 40) < "1010") then
							char(21) <= x"3" & line2(43 downto 40);
						else
							char(21) <= x"4" & (line2(43 downto 40)-9);
						end if;
						if(line2(39 downto 36) < "1010") then
							char(22) <= x"3" & line2(39 downto 36);
						else
							char(22) <= x"4" & (line2(39 downto 36)-9);
						end if;
						if(line2(35 downto 32) < "1010") then
							char(23) <= x"3" & line2(35 downto 32);
						else
							char(23) <= x"4" & (line2(35 downto 32)-9);
						end if;
						if(line2(31 downto 28) < "1010") then
							char(24) <= x"3" & line2(31 downto 28);
						else
							char(24) <= x"4" & (line2(31 downto 28)-9);
						end if;
						if(line2(27 downto 24) < "1010") then
							char(25) <= x"3" & line2(27 downto 24);
						else
							char(25) <= x"4" & (line2(27 downto 24)-9);
						end if;
						if(line2(23 downto 20) < "1010") then
							char(26) <= x"3" & line2(23 downto 20);
						else
							char(26) <= x"4" & (line2(23 downto 20)-9);
						end if;
						if(line2(19 downto 16) < "1010") then
							char(27) <= x"3" & line2(19 downto 16);
						else
							char(27) <= x"4" & (line2(19 downto 16)-9);
						end if;
						if(line2(15 downto 12) < "1010") then
							char(28) <= x"3" & line2(15 downto 12);
						else
							char(28) <= x"4" & (line2(15 downto 12)-9);
						end if;
						if(line2(11 downto 8) < "1010") then
							char(29) <= x"3" & line2(11 downto 8);
						else
							char(29) <= x"4" & (line2(11 downto 8)-9);
						end if;
						if(line2(7 downto 4) < "1010") then
							char(30) <= x"3" & line2(7 downto 4);
						else
							char(30) <= x"4" & (line2(7 downto 4)-9);
						end if;
						if(line2(3 downto 0) < "1010") then
							char(31) <= x"3" & line2(3 downto 0);
						else
							char(31) <= x"4" & (line2(3 downto 0)-9);
						end if;					
				end if;
				
				
			when selector =>
				if (send_count = 16 and second_line = 0) then			-- switch to second line
					state <= switch_lines;
				elsif (send_count > 31) then
					send_count := 0;
					second_line := 0;
					if(pause_lcd = '1') then
						state <= idle;									   -- for fixed display data
					else
						state <= power_up;										-- for updatable display data
					end if;
				else
					state <= ready;
				end if;
				
				
			when switch_lines =>										-- second line initialization
				busy <= '1';
				if (clock_count < (50000 * freq)) then					-- 50ms wait
					clock_count := clock_count + 1;
					state <= switch_lines;
				elsif (clock_count < (50050 * freq)) then				-- set DDRAM address
					lcd_rs <= '0';
					lcd_rw <= '0';
					lcd_data <= "11000000";
					lcd_enable <= '1';
					clock_count := clock_count + 1;
					state <= switch_lines;
				elsif (clock_count < (50100 * freq)) then				-- 50us wait
					lcd_enable <= '0';
					lcd_data <= "00000000";
					clock_count := clock_count + 1;
					state <= switch_lines;
				else
					clock_count := 0;
					second_line := second_line + 1;
					state <= selector;
				end if;
				
				
			when ready =>
				if (enable_in = '1') then
					busy <= '1';
					lcd_rs <= '1';
					lcd_rw <= '0';
					lcd_data <= char(send_count);
					clock_count := 0;
					state <= send;
				else
					busy <= '0';
					lcd_rs <= '0';
					lcd_rw <= '0';
					clock_count := 0;
					lcd_data <= "00000000";
					state <= selector;
				end if;
						
				
			when send =>												-- send data
				busy <= '1';
				if (clock_count < (50 * freq)) then
					busy <= '1';
					if (clock_count < freq) then
						lcd_enable <= '0';
					elsif (clock_count < (14 * freq)) then
						lcd_enable <= '1';
					elsif (clock_count < (27 * freq)) then
						lcd_enable <= '0';
					end if;
					clock_count := clock_count + 1;
					state <= send;
				else
					send_count := send_count + 1;
					clock_count := 0;
					ready_out <= '1';
					state <= selector;
				end if;
				
				
			when idle =>
				if (reset = '1') then
					ready_out <= '1';
					state <= init;
				else
					ready_out <= '1';
					busy <= '0';
					if(pause_lcd = '1') then
						state <= idle;
					else
						state <= power_up;
					end if;
				end if;
						
				
		end case;
		
	end if;
	end process control_sequence;
	
end architecture lcd_controlLine;
				
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
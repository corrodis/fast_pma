// Copyright (C) 1991-2013 Altera Corporation
// This simulation model contains highly confidential and
// proprietary information of Altera and is being provided
// in accordance with and subject to the protections of the
// applicable Altera Program License Subscription Agreement
// which governs its use and disclosure. Your use of Altera
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Altera Program License Subscription
// Agreement, Altera MegaCore Function License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs for
// use exclusively in logic devices manufactured by Altera and sold
// by Altera or its authorized distributors. Please refer to the
// applicable agreement for further details. Altera products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Altera assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 13.0sp1
// ALTERA_TIMESTAMP:Thu Jun 13 12:28:47 PDT 2013
// encrypted_file_type : mentor_tagged
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
dq1hxBEo8hm25fbiytirR2N3E9i46yvQT51pyMZOHiswlNTVsh/VxkM+nAAuAI/E
8X+3+RpTWIXGtX8I5fT3SZoMB6ap/40rsfnh0HHQTpYsa2HMSi2zhUzFCSpozCtW
bOoQb2Thd2GES45FS6cg5knMdJDyTrkCl++aaYRzuuE=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 2704)
Fq7fZHcBgRw66bCL+65rl4U/0qsIs+bSoAuTMBFlBbd97w1QpF3S/uC255x4YhjN
rBo7hwNaMEfKQEtevIKEz3J5RG94FH83jYYuE50TcJ31FGqLXu+ZO+6lPSIDY2/B
10I383GwDYo1lTIEsYbYkQmQ9guCE1XEnoV69AG24tS+csfDsqLNQLaXU+B4QTZW
G+z/A2zkoD7FcuT1HqcDaPTMEAFnCpKi3cUTRj1dx0eMb24xEnRuOQG98rhX6QK/
tg4JeBrfMOaEGjdbKFQTxjm6IS7WUA6BbkSWIGb2R8FUjGdrMUGJXGXQEXnKpJoy
zMoEv3lOI3YH4I41SX9XeB/mZvdLqeaKuZUTMrx1g59m+NenOqaIpqqiTqhYl9wa
KArGUBKDPyJhJUtDflwdXSNLp1sZxCyq1JWQVdxrIfXrVvGwT2y4kfKlrfVQ91af
4t3BSHZuC10aS1fhGi/zHaWDyfwD17Bfn1erD9OqkhlqkSIMOtdvUJ1jQgN4n8/p
Xkn8Kh5E8PfYy560Ryu78S5A8+4hmhXJWnxdYsp1vzqLWIXmWlvDyRAtXmZBETKa
y6Faa+oIQ757gWWVg1O5KzbVUv4695pZ26vUpW4OxrsTjQRfnEwwtfudvWpphmJY
RL2YN41tlSCOhkfJLaaNPzOYqjUFJ/RacBsIdVjAKQ49dsc1/EX6CgnsBSbgkbha
BYjJEFsUYwbQZw5JjH7w7Ph50IQUdpigoohfwnVe8Yneaw+A2Y9auS/KV4/YwkPR
PAHr4CvKup5MzNdAERMMGHUtFI9ZzaI6pd6eDndqKNASQTv7KUJYyOh3gtelR9P6
cihOgyf+apbxkkyHLWKDF4ViW0a3FvNqY3qXZQlbxIHVIIYIk56eiyYKIxYxKC79
w+gQ5LWws/xGyROjTnKEc7MGzHIQsBZy1Ony21ZQDiKgBYhbRyYJHF9c4LmtzZJk
zftB9J/HY0S5kdIbUZRR4RLPTKGcAFRoiR8YVwdoF07WVkuE6XquSvn2xA9GS9LI
/84x6lXaqkiwOgOXZp4uIcprntcB5PWwrWpVUPf9cEoryQlryBFF3zyhiwF4lGBS
F7WlYZrah6JEaRhJya/MhQLbYbaQUtaWcAIy+2NebWea/FVnt6kgqrbz116iwrjo
cFWaU+JVuDueFBTSOUz12A/nTXiHpNk3k6DtDKHOwU0IAp/dRYFyUn81uGQLH16U
pivptHQ5jm6eG8zf8+4xnCs/bZ62l8vlRsFI+QLJVGHkP5ZxOxdXE5YHZdr+1cV3
ZQuBsZzCTc9BGAn4EYDceEPeKhakpWo77kt9YiDdxj2OI91Uycl4uKqxcnB2gtM8
NKeoqJ9galEs2VLamS6X5UeBUR0E1cQHvPoStOrySe8cALVA5CaNk+b2TyVV5sLW
nT+2DI76Ttuwl/owoLMYHsqMKPJ8hZcViQSu2+Z+ox0+0J++B3LSDZqAWSgX7UJF
o0sJOky2eMpqr/9IXLU6Vsze3gnlM0HZQQ9Oez1DusaZ2oWzOfwcmqgc9Fv27aXu
wVcVII/X5Xfswp3vG9JeyO5E37qD6EQ3oTm2EL3tcPsWsseJMK6DYghnH29HpH/Q
poVqPHCNelhAo4EZlSy6Dt6G2TmnKtwgP+OIndM2ir7fzs/dt2ZIAf6KSP4z/OUi
HAre9zwUH1yz8iKABwk9C3fbd/B92hohRUrgli0nCBQ/wOvWtTdXmP8AHOHsmXbS
WJ9sBtOk7r3TyR77JW5SBTokEWxyPAo6kFN44LuxQkM94YhUe8VLILwvSi+sNhqa
iHUkHWbmKS49j6hBIRyNniXNKn9Ch799vYr1IIzCoxX0nLPwz0DXzrnJr15craxX
VxqegXKXUPGeLCPwqmBGhhdGu5swpW9QNjFpoeusDuUIzbMShDQKY5W/GZJtTEjH
/JoZOsUaWK9oI759RzDNRjTp1UmtrUZ3+0Z42GkSlgi/ZgnV4ev9K0+6DmM2J6J8
VqjaezfByoZVzUpFK4XZ2xEJDibSuTF5XHzo3aEKwtd2209sd727QIzHw68fQU61
X66NvbSy0Yr23TETZc4tPKX4ZgEGfXeTEeALEJ81V2xxQL9RvPhdWoohr2BNPzTr
CaNBlPb7Qf+nOZ+4WwWi9hysIE9aIPVqFlK7bAaa1VYV0JS4vTGTIMYJEI6KMhQm
Uh0AsRThyyQkEQi/rdfXZFUB7Gv0Wesf1q3DNZN1bhmFHzeQjnkIRJvJDjHgwPsD
GmTGYGajjFpZHJfYXUgBlLE+BJGhZrksG2UmHPotCqbXL+SGsHoTqKLZY7+8PnhT
kKGo8X83NPRvZDk1cfLDhl9Rfb2ZGyqbMsq6QVDxMZ5zojVwGJijnuMpxYv6Bdpn
nU6gA7sB7ENJ87NMbRCw0YzgVuZqUIyraJp+UXaDfWwzJi1IK4cwfPL5DNnaSP9n
slWSv8wyAVJDI3+f+wxfZ3xymdYLydk7O+57ZNLVBHDTOEItfHHbY0sZKE+f5dTn
pqqxg+ntxBNV4dgIfsQjrhWGxO9OGmcSPdsCh3LojHG69bCT7kh309TRs4ooSnAR
m/ZolStiJxiS3VOJXqaZwNCUxdkGfWlBM6WnMSTGUOk+ODT5j7OsXpqyheul+TSu
8XzIOQTUil8lRnOlEvRZK1IkdAt83lwldU6r9DAF9wmmpGQkcv9WLGULALfgN8j6
djkzl4YbcgkNcV3bczO6IB+YYHRAlwpToGvsQQ9b+C0YkGnEc2mLfz2hP+CeioJl
BWud5LDi4yhUons/1lYFIqTfZHXKX071LkQ8Y4L2xr9flVBdcIiadfsDomiVXaBg
4GbwjsufnPN9FVLJzgYDGb1zxM21OpBNeis4FcO5n2PhG0xElQi+Q8db9rvwKAie
EkrUUgzAx0JoAV4X+Ov7EDhx4h7kG1OmRilRL1r4n9246nTdzvy2Y0cKp3V9IGhd
lKvFmppLldociCpnKqPP6n2JGCzMELiL50eUkViMWLy7or3d8IlQfAcCpzKDZPnS
FTNKCjgCCRi/DuF41HjEVvENIkaECMvbShGhxG4z6x6OAe6jgqW4wK0fvRfruvvP
8q55wX5HCbu32/YU+dujyeb2OvLzqAQEMWFfbBEzz4Mxc01yo2dzRw4PwgLcosz3
1XpKCu8EJkcgtOo3UT6uh4xv9UuA2U597Tv0ZYv9dZFlonbMLlNSC1dhUzciFq96
6YH5su1AtlQr1gtJZfap9kgaQk0CjoE/84Py6ctGhSJZPCfrEWdk1ZBosO8ZTkTe
1WtMhcG8pKKwnyOqjwGwWj91hd0Hh0gKU3XNXy/YtCJfXUY+iySvsNUXeoLWBqfW
pUl2D6r4BQ+uhkKbOZrvoyA2HAa7K22ljZOu2wa0k9X0p+Ny/0yAYmSi/hD2/lcs
cymb1hyhfTnTu6Dnz1nf/EqQCqkG5M9ioJeU9RmBPp1hQlF3Y8FsuPXrBpfOn0JD
fNPOH6quWeV0XwEq98NT6ihzIuCdhSZd3orCUlis4cvoR+JZlDAz7RkKUXJ6gPQM
loaNTtpVbv2wcV8R1vTxa7lx55UFC4tpY2KQonTye618UNW2EhoJqpzRw8MQwgCr
U5V+eF4AFbxEaHyvDGgBsQ==
`pragma protect end_protected

-- CRC Calculation
-- Simon Corrodi, corrodi@physi.uni-heidelberg.de, 08/01/2014

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity CRC32_serie is
	Port ( CLK 			: in	std_logic;
			 RST			: in  std_logic;											-- active low
			 DATA			: in	std_logic;
			 CRC			: out	std_logic_vector(31 downto 0)					:= (others=>'0')
	);
end CRC32_serie;

architecture RTL of CRC32_serie is

	signal 	crc_reg 	: std_logic_vector(31 downto 0)					:= (others=>'0');
	constant crc_gen 	: std_logic_vector(31 downto 0)					:= "10000010011000001000111011011011";

begin
	CRC_calc : process(CLK) begin
		if(rising_edge(CLK)) then
			if(RST = '0') then
				-- reset CRC
				crc_reg		<= (others=>'0');
			else
				if(crc_reg(31) /= DATA) then
					crc_reg <= (crc_reg(30 downto 0) & '0') xor crc_gen;
				else 
					crc_reg <= (crc_reg(30 downto 0) & '0');
				end if;
			end if;
		end if;
	end process CRC_calc;

	CRC <= crc_reg;
end RTL;